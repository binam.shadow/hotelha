<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', ['as'	=> 'login', function(){
    return response()->view('errors.404', [], 404);
}]);

Route::get('media/{filename?}/{size?}', [
	'as'	=> 'show_img',
	function ($filename, $size = 'lg') {
		return App\Medium::where('file_name', $filename)->first()->show($size);
	}
]);

Route::post('remove_media', [
	'as'	=> 'remove_media',
	function (Illuminate\Http\Request $request) {
        $medium = App\Medium::whereFileName($request->key);
        try {
            $medium->first()->remove();
        } catch(\Exception $exception){
            dd($exception);
        }
        return ['success' => '1'];
	}
]);
