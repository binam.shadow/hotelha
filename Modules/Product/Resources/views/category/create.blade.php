@extends('adminbsb.layouts.master')

@section('title')
@lang('product::default.category_create')
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('product::default.category_create')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="categoryCreate" method="post" action="{{ route('category.create') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
							<label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="description" value="{{ old('description') }}" aria-required="true" type="text">
							<label class="form-label">@lang('product::default.description')</label>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب آیکن</p>
							@include('partials._single_fileinput', [
								'field_name' => 'icon'
							])
						</div>
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب تصویر</p>
							@include('partials._single_fileinput', [
								'field_name' => 'image'
							])
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-6">
							<label class="form-label">زیردسته</label>
							<select class="col-md-8" name="parent_id">
								<option value="0" selected>مادر</option>
								@foreach($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-6 parent-parent">
							<label class="form-label">زیردسته</label>
							<select class="col-md-8" name="parent_parent_id" disabled>
								<option value="0" selected>مادر</option>
							</select>
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
function responsive_filemanager_callback(field_id){
	$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

}
function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
function deleteImage(field_id) {
	$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
	$('#'+field_id).val('');
}
$('select').select2({
	dir: 'rtl'
});
$('select[name=parent_id]').on('change', function () {
	var p_id = $(this).val();
	if(p_id != 0) {
		showWating();
		$.ajax({
			url: "{{ route('category.getChild') }}",
			data: 'parent_id='+p_id+'&parent_parent_id=0',
			type: 'GET',
			success: function (result) {
				$('select[name=parent_parent_id]').html('<option value="0">مادر</option>');
				if(result != '') {
					$.each(result, function (key, value) {
						$('select[name=parent_parent_id]').append('<option value="'+value.id+'">'+value.name+'</option>');
					});
					$('select[name=parent_parent_id]').removeAttr('disabled');
				}
			},
			error: function () {
				alert('مشکلی به وجودآمده است، لطفا دوباره امتحان کنید');
			}
		});
	} else {
		$('.hidden-child').slideUp()
	}
});
</script>
@endsection
