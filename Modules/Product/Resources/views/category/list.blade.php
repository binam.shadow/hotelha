@extends('adminbsb.layouts.master')

@section('title')
@lang('product::default.category_list')
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
<link href="{{ asset('backend/plugins/nestable/jquery-nestable.css') }}" rel="stylesheet"/>

<style>
	.modal-open {
		padding-right: 0px !important;
	}
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('product::default.category_list')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover dataTable js-exportable category-list" width="100%">
						<thead>
							<tr>
								<th>@lang('product::default.id')</th>
								<th>@lang('product::default.icon')</th>
								<th>@lang('product::default.image')</th>
								<th>@lang('product::default.name')</th>
								<th>@lang('product::default.description')</th>
								<th>@lang('product::default.setting')</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
	<div id="sortModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">مرتب سازی این سطح</h4>
	      </div>
	      <div class="modal-body row">
			  	<div class="dd">
				    <ol class="dd-list">

				    </ol>
				</div>

	      </div>
	      <div class="modal-footer">
			  	<button type="button" class="btn btn-primary btn-lg" onclick="submitSort()">تایید</button>
	        	<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">بستن</button>
	      </div>
	    </div>

	  </div>
	</div>
@endsection


@section('scripts')
@include('adminbsb.layouts.datatable_files')
<script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/nestable/jquery.nestable.js') }}"></script>
<script type="text/javascript">
	var dataTable = $('.category-list').DataTable({
		"order": [[ 0, "desc" ]],
		ajax: {
			url: '{{ route('category.dataList') }}'
		},
		responsive: true,
		serverSide: true,
		columns: [
			{data: 'id', name:'id'},
			{
				data: 'icon',
				"bSortable": false,
				render: function (data, type, row) {
					if(data != null && data != '')
						return '<a href="{{ route('show_img') }}/'+ data.file_name +'" target="_blank"><img src="{{ route('show_img') }}/'+ data.file_name +'" width="60px"></a>';
					else
						return '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default.png') }}" style="width: 60px;">';
				}
			},
			{
				data: 'image',
				"bSortable": false,
				render: function (data, type, row) {
					if(data != null && data != '')
						return '<a href="{{ route('show_img') }}/'+ data.file_name +'" target="_blank"><img src="{{ route('show_img') }}/'+ data.file_name +'" width="60px"></a>';
					else
						return '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default.png') }}" style="width: 60px;">';
				}
			},
			{data: 'name'},
			{data: 'description'}
		],
		columnDefs: [
			{
				targets: 5,
				render: function(data, type, row) {
					var btns = '';
					@permission('category.edit')
					btns += '<a href="{{ route('category.edit') }}/'+row.id+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
					btns += '<button onclick="sortBox('+row.id+','+row.parent_id+','+row.parent_parent_id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="مرتب سازی این سطح" type="button" class="btn btn-xs bg-light-green waves-effect"><i class="material-icons">sort</i></button> ';
					btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
					@endpermission
					return btns;
				}
			}
		],
		buttons: [
			'copyHtml5', 'excelHtml5', 'print'
		],
		language:{
			url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
		}
	});

	function showConfirmDelete(id){
		swal({
			title: "@lang('product::default.are_you_sure')",
			text: "@lang('product::default.warning_delete_msg')",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			cancelButtonText: "@lang('product::default.cancel')",
			confirmButtonText: "@lang('product::default.confirm')",
			closeOnConfirm: false
		}, function (action) {
			if(action)
			$.ajax({
				url: "{{ route('category.delete') }}",
				data: "id="+id,
				processData: false,
				contentType: false,
				type: 'GET',
				success: function (result) {
					if(result.status == 'success') {
						swal("@lang('product::default.success_delete')", "@lang('product::default.success_delete_msg')", "success");
						dataTable.ajax.reload()
					}
				},
				error: function (){

				}
			});
		});
	}

	$('.dd').nestable({
		maxDepth: 1
	});
	function submitSort() {
		var categories = $('.dd').nestable('serialize');
		$.ajax({
			url: "{{ route('category.sortCategories') }}",
            data: "categories="+JSON.stringify(categories),
				contentType: 'json',
			dataType: 'json',
            type: 'GET',
            success: function (result) {
                if(result.status == "success") {
					$("#sortModal").modal("hide");
					swal("@lang('product::default.success_edit')", "@lang('product::default.success_edit_msg')", "success");
                } else {
	               	alert('مشکلی به وجودآمده است، لطفا دوباره امتحان کنید');
				}

            },
            error: function () {
                alert('مشکلی به وجودآمده است، لطفا دوباره امتحان کنید');
            }
		});
	}
	function sortBox(id, parent_id, parent_parent_id) {
		$(".dd-list").html('');
		$.ajax({
            url: "{{ route('category.getChild') }}",
            data: 'parent_id='+parent_id+"&parent_parent_id="+parent_parent_id,
            type: 'GET',
            success: function (result) {
                if(result != '') {
                    $.each(result, function (key, value) {
                        $(".dd-list").append(
							'<li class="dd-item" data-id="'+value.id+'">'+
        					'<div class="dd-handle">'+value.name+'</div>'+
    						'</li>');
                    });
                }
            },
            error: function () {
                alert('مشکلی به وجودآمده است، لطفا دوباره امتحان کنید');
            }
        });
		$("#sortModal").modal("show");
	}
</script>
@endsection
