<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\ProductCategory;
use Entrust;
use Validator;
use File;
use \App\Classes\FileManager;
use App\Medium;
use DataTables;
use Spatie\Activitylog\Models\Activity;

class CategoryController extends Controller
{

    public function create() {
        Entrust::can('category.create') ?: abort(403);

        // Get all parents
        $categories = ProductCategory::where([
            'parent_id' => 0,
            'parent_parent_id' => 0
        ])->get();
        return view('product::category.create', compact('categories'));
    }

    public function store(Request $request) {
        Entrust::can('category.create') ?: abort(403);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'description' => 'max:256',
            'image' => 'max:1024',
            'icon' => 'max:1024'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $exist = ProductCategory::where([
            'name'  => $request->name,
            'parent_id' => ($request->parent_id) ? $request->parent_id : 0,
            'parent_parent_id' => ($request->parent_parent_id) ? $request->parent_parent_id : 0
        ])->first();
        if(count($exist))
            return back()
                ->with('msg', \Lang::get('setting::default.must_unique'))
                ->with('msg_color', 'bg-red');

        $data['name'] = $request->name;
        $data['description'] = $request->description;

        if ($request->parent_id == 0) {
            $data['parent_id'] = 0;
            $data['parent_parent_id'] = 0;
        } else {
            $data['parent_id'] = $request->parent_id;
            if ($request->parent_parent_id)
                $data['parent_parent_id'] = $request->parent_parent_id;
            else
                $data['parent_parent_id'] = 0;
        }

        // Insert data to database
        $category = ProductCategory::create($data);
		if($request->icon)
			Medium::make($request->icon, 'Product Category', [
				'position' 	=> 0,
				'model' 	=> 'product_categories',
				'model_id' 	=> $category->id,
				'description'	=> 'Product Category'
			], 'icon')->store();

		if($request->image)
			Medium::make($request->image, 'Product Category', [
				'position' 	=> 0,
				'model' 	=> 'product_categories',
				'model_id' 	=> $category->id,
				'description'	=> 'Product Category'
			], 'image')->store();

        // Redirect back with success message
        return back()
            ->with('msg', \Lang::get('product::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    public function list() {
        return view('product::category.list');
    }

    public function dataList(Request $request) {
        Entrust::can('category.list') ?: abort(403);

        return DataTables::of(ProductCategory::query())
                        ->addColumn('icon', function ($query) {
                            return json_decode($query->icon, JSON_UNESCAPED_SLASHES);
                        })
                        ->addColumn('image', function ($query) {
                            return json_decode($query->image, JSON_UNESCAPED_SLASHES);
                        })
                        ->make();
    }

    public function edit($id = 0) {
        Entrust::can('category.edit') ?: abort(403);

        // Get All Parents
        $category = ProductCategory::find($id);
        count($category) ?: abort(404);

        $categories = ProductCategory::where([
            'parent_id' => 0,
            'parent_parent_id' => 0
        ])->get();

        return view('product::category.edit', compact('category', 'categories'));
    }

    public function update($id, Request $request) {
        Entrust::can('category.edit') ?: abort(403);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'description' => 'max:256',
            'image' => 'max:1024',
            'icon' => 'max:1024'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        // Insert data to database
        $category = ProductCategory::where('id', $id)->first();

        $data['name'] = $request->name;
        $data['description'] = $request->description;

		if($request->icon)
			Medium::make($request->icon, 'Product Category', [
				'position' 	=> 0,
				'model' 	=> 'product_categories',
				'model_id' 	=> $category->id,
				'description'	=> 'Product Category'
			], 'icon')->store();
		if($request->image)
			Medium::make($request->image, 'Product Category', [
				'position' 	=> 0,
				'model' 	=> 'product_categories',
				'model_id' 	=> $category->id,
				'description'	=> 'Product Category'
			], 'image')->store();

        if ($request->parent_id == 0) {
            $data['parent_id'] = 0;
            $data['parent_parent_id'] = 0;
        } else {
            $data['parent_id'] = $request->parent_id;
            if ($request->parent_parent_id)
                $data['parent_parent_id'] = $request->parent_parent_id;
            else
                $data['parent_parent_id'] = 0;
        }

        $category->update($data);

        return redirect()->route('category.list')
            ->with('msg', \Lang::get('product::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function delete(Request $request) {
        Entrust::can('category.edit') ?: abort(403);

        // Get category
        $category = ProductCategory::where('id', $request->id)->first();

        // Get category children
        $children = ProductCategory::where('parent_id', $category->id)->orWhere('parent_parent_id', $category->id);

        // // Delete Childs image and icons
        foreach ($children->get() as $children) {
			foreach ($children->media()->get() as $medium) {
				$medium->remove();
			}
        }
        // Delete children
        $children->delete();

		foreach ($category->media()->get() as $medium) {
			$medium->remove();
		}
        // Delete main category
        $category->delete();

        // Retrun success to show success msg in front
        return ['status' => 'success'];
    }

    public function getChild(Request $request) {
        return ProductCategory::where([
            'parent_id' => $request->parent_id,
            'parent_parent_id' => $request->parent_parent_id
        ])->orderBy('order')->get();
    }

	public function sortCategories(Request $request) {
		Entrust::can('category.edit') ?: abort(403);

		$categories = json_decode($request->categories);

		foreach ($categories as $key => $category) {
			ProductCategory::where('id', $category->id)
						->update([
							'order' => $key
						]);
		}
		return ['status' => 'success'];
	}

	public function deleteImage(Request $request) {
		Entrust::can('category.edit') ?: abort(403);

		Medium::find($request->key)->remove();

        return ['status' => 'success'];
	}
}
