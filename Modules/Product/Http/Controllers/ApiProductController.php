<?php

namespace Modules\Product\Http\Controllers;

use App\Classes\Encryption;
use App\Classes\PersianTools;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Entities\ProductRating;
use Modules\Product\Entities\Wishlist;
use App\User;
use App\Role;
use Entrust;
use Validator;

class ApiProductController extends Controller {

    public function getAllProductCategories(Request $request) {
        $limit = $request->limit;
        $offset = $request->offset;

        if($limit)
            $data = ProductCategory::limit($limit)->offset($offset)->get();
        else
            $data = ProductCategory::all();
        return Encryption::encryptor($data, $request->key);
    }

    public function getProductCategoryChildren(Request $request) {
        $limit = $request->limit;
        $offset = $request->offset;
        $id = $request->id;

        if($limit)
            $data = ProductCategory::where('parent_id' , $id)->limit($limit)->offset($offset)->get();
        else
            $data = ProductCategory::where('parent_id' , $id)->get();
        return Encryption::encryptor($data, $request->key);
    }

    public function rateProduct(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if(!$request->id) {
            $data = ['error' => 2, 'msg' => \Lang::get('product::default.product_id_needed')];
            return Encryption::encryptor($data, $request->key);
        }
        $id = $request->id;
        ProductRating::where(['user_id' => $user->id, 'product_id' => $id])->delete();
        ProductRating::create([
            'user_id' => $user->id,
            'product_id' => $id,
            'rate' => $request->rate
        ]);
        $data = ['error' => 0, 'msg' => \Lang::get('product::default.success_edit')];
        return Encryption::encryptor($data, $request->key);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function addWishlist(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if(!$request->id) {
            $data = ['error' => 2, 'msg' => \Lang::get('product::default.product_id_needed')];
            return Encryption::encryptor($data, $request->key);
        }
        $id = $request->id;
        $data = Wishlist::where(['user_id' => $user->id, 'product_id' => $id])->get();
        if(!count($data)) {
            $data = array();
            $data['product_id'] = $id;
            $data['user_id'] = $user->id;
            Wishlist::create($data);
            $data = ['error' => 0, 'msg' => \Lang::get('product::default.wishlist_add')];
            return Encryption::encryptor($data, $request->key);
        }
        $data = ['error' => 3, 'msg' => \Lang::get('product::default.wishlist_exist')];
        return Encryption::encryptor($data, $request->key);
    }

    public function deleteWishlist(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if(!$request->id) {
            $data = ['error' => 2, 'msg' => \Lang::get('product::default.product_id_needed')];
            return Encryption::encryptor($data, $request->key);
        }
        $id = $request->id;
        Wishlist::where(['user_id' => $user->id, 'product_id' => $id])->forceDelete();
        $data = ['error' => 0, 'msg' => \Lang::get('product::default.success_delete_msg')];
        return Encryption::encryptor($data, $request->key);
    }

    public function getWishList(Request $request){
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if($request->limit) {
            $limit = $request->limit;
            $offset = $request->offset;
            $data = Wishlist::with('product')
                ->where('user_id', $user->id)
                ->orderBy('id', 'desc')
                ->offset($offset)
                ->limit($limit)
                ->get();
        }
        else {
            $data = Wishlist::with('product')
                ->where('user_id', $user->id)
                ->orderBy('wishlists.id', 'desc')
                ->get();
        }
        foreach($data as $item){
            $item->description = strip_tags($item->description);
        }
        return Encryption::encryptor($data, $request->key);
    }

}
