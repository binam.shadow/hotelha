<?php

Route::group([
  'middleware' => ['web', 'permission:product*|category*'],
  'prefix' => 'admin/product',
  'namespace' => 'Modules\Product\Http\Controllers'
], function() {

    /** Start Product **/
    Route::get('product-create', [
      'as'  => 'product.create',
      'uses'=> 'ProductController@create'
    ]);
    Route::post('product-create', [
      'as'  => 'product.create',
      'uses'=> 'ProductController@store'
    ]);
    Route::get('product-list', [
      'as'  => 'product.list',
      'uses'=> 'ProductController@list'
    ]);
    Route::get('product-dataList', [
      'as'  => 'category.dataList',
      'uses'=> 'ProductController@dataList'
    ]);
    /** End Product **/

    /** Start Category **/
    Route::get('category-create', [
      'as'  => 'category.create',
      'uses'=> 'CategoryController@create'
    ]);
    Route::post('category-create', [
      'as'  => 'category.create',
      'uses'=> 'CategoryController@store'
    ]);
    Route::get('category-list', [
      'as'  => 'category.list',
      'uses'=> 'CategoryController@list'
    ]);
    Route::get('category-dataList', [
      'as'  => 'category.dataList',
      'uses'=> 'CategoryController@dataList'
    ]);
    Route::get('category-edit/{id?}', [
      'as'  => 'category.edit',
      'uses'=> 'CategoryController@edit'
    ]);
    Route::post('category-edit/{id?}', [
      'as'  => 'category.edit',
      'uses'=> 'CategoryController@update'
    ]);
    Route::get('category-delete/{id?}', [
      'as'  => 'category.delete',
      'uses'=> 'CategoryController@delete'
    ]);
    Route::get('category-get-child', [
      'as'	=> 'category.getChild',
      'uses'=> 'CategoryController@getChild'
    ]);
	Route::get('/sort-categories',[
        'as'	=> 'category.sortCategories',
        'uses'	=> 'CategoryController@sortCategories'
	]);
	Route::post('category-delete-image', [
      'as'  => 'category.deleteImage',
      'uses'=> 'CategoryController@deleteImage'
    ]);
    /** End Category **/
});
