<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Product\Http\Controllers'
], function() {

    Route::get('getAllProductCategories', [
        'uses'	=> 'ApiProductController@getAllProductCategories'
    ]);
    Route::get('getProductCategoryChildren', [
        'uses'	=> 'ApiProductController@getProductCategoryChildren'
    ]);
    Route::get('rateProduct', [
        'uses'	=> 'ApiProductController@rateProduct'
    ]);
    Route::get('addWishlist', [
        'uses'	=> 'ApiProductController@addWishlist'
    ]);
    Route::get('deleteWishlist', [
        'uses'	=> 'ApiProductController@deleteWishlist'
    ]);
    Route::get('getWishList', [
        'uses'	=> 'ApiProductController@getWishList'
    ]);
});
