@extends('adminbsb.layouts.master')

@section('title')
    @lang('log::default.list_title')
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
    <style>
        .modal-open {
            padding-right: 0px !important;
        }
    </style>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>@lang('log::default.list_title')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable Log-list" width="100%">
                            <thead>
                            <tr>
                                <th>@lang('log::default.id')</th>
                                <th>@lang('log::default.user')</th>
                                <th>@lang('log::default.date')</th>
                                <th>@lang('log::default.description')</th>
                                <th>@lang('log::default.setting')</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="userModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">@lang('log::default.view_description')</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">@lang('log::default.close')</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('adminbsb.layouts.datatable_files')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var dataTable = $('.Log-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('log.dataList', ['user' => $user]) }}'
            },
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {
                    data: 'causer_id',
                    render: function(data, type, row){
                        return '<button onclick="showUser('+data+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="کاربر ثبت کننده" type="button" class="btn btn-xs btn-primary waves-effect"><i class="material-icons">account_circle</i></button> ';
                    }
                },
                {data: 'persian_created_at'},
                {data: 'description'},
            ],
            columnDefs: [
                {
                    targets: 4,
                    render: function(data, type, row) {
                        var btns = '';
                        btns += '<button onclick="logData('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="مشاهده جزییات" type="button" class="btn btn-xs bg-indigo waves-effect"><i class="material-icons">description</i></button>';
                        return btns;
                    }
                }
            ],
            buttons: [
                'copyHtml5', 'excelHtml5', 'print'
            ],
            language:{
                url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
            }
        });

        function showUser(id){
            $.ajax({
                url: "{{ route('log.userData') }}/"+id,
//                data: "id=" + id,
                processData: false,
                contentType: false,
                type: 'GET',
                success: function (result) {
                    var m_modal = $("#userModal");
                    m_modal.find('.modal-body').html('');
                    var text_data = '<p class="col-md-6">شناسه : '+result.id+'</p>'+
                        '<p class="col-md-6">نام : '+result.name+'</p>'+
                        '<p class="col-md-6">شماره تلفن : '+result.phone+'</p>';
                    m_modal.find('.modal-body').html(text_data);
                    m_modal.modal("show");
                },
                error: function (){
                    alert('مشکلی به وجود آمده است، لطفا دوباره امتحان کنید');
                }
            });
        }
        
        function logData(id) {
            $.ajax({
                url: "show/"+id,
                //data: "_method=delete",
                processData: false,
                contentType: false,
                type: 'GET',
                success: function (result) {
                    var m_modal = $("#userModal");
                    m_modal.find('.modal-body').html('');
                    var text_data = '<p class="col-md-6">تاریخ : '+result.persian_created_at+'</p>'+
                                    '<p class="col-md-12">جزییات : '+result.description+'</p>';
                    if(result.properties.old == undefined){
                        text_data += '<table class="table details-table"><thead><tr>'+
                                        '<th>نام فیلد</th>'+
                                        '<th>مقدار</th></thead><tbody>';
                        $.each(result.properties.attributes, function(key, value) {
                            text_data += '<tr>'+
                                    '<th>' + key + '</th>'+
                                    '<td>' + value + '</td>'+
                                '</tr>';
                        });
                    } else {
                        text_data += '<table class="table details-table"><thead><tr>'+
                                        '<th>نام فیلد</th>'+
                                        '<th>مقدار جدید</th>'+
                                        '<th>مقدار قبلی</th>'+
                                        '</thead><tbody>';
                        $.each(result.properties.attributes, function(key, value) {
                            text_data += '<tr>'+
                                    '<th style="background: #F5F5F5;border-bottom: 1px solid #d8d8d8;">' + key + '</th>'+
                                    '<td style="background: #CDFFD8;border-bottom: 1px solid #d8d8d8;">' + value + '</td>'+
                                    '<td style="background: #FFDCE0;border-bottom: 1px solid #d8d8d8;">' + result.properties.old[key] + '</td>'+
                                '</tr>';
                        });
                    }
                        
                    text_data += '</tbody></table>';
                    m_modal.find('.modal-body').html(text_data);
                    m_modal.modal("show");
                },
                error: function (){

                }
            });
        }

        function showConfirmDelete(id){
            swal({
                title: "@lang('log::default.are_you_sure')",
                text: "@lang('log::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('symbol::default.cancel')",
                confirmButtonText: "@lang('symbol::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "log/"+id,
                        //data: "_method=delete",
                        processData: false,
                        contentType: false,
                        type: 'DELETE',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('log::default.success_delete')", "@lang('log::default.success_delete_msg')", "success");
                                dataTable.ajax.reload()
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
    </script>
@endsection
