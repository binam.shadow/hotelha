<?php
/**
 * Created by PhpStorm.
 * User: Saeid
 * Date: 30/12/2017
 * Time: 12:23 AM
 */

return [
    'id'    => 'شناسه',
    'title' => 'عنوان',
    'list_title' => 'عنوان لیست',
    'name' => 'نام',
    'description' => 'توضیحات',
    'setting' => 'تنظیمات',
    'user'  => 'کاربر',
    'create' => 'ایجاد',
    'edit' => 'ویرایش',
    'cancel' => 'لغو',
    'confirm' => 'تأیید',
    'view_description' => 'نمایش توضحیات',
    'close' => 'بستن',
    'date' => 'تاریخ',

    'are_you_sure' => 'آیا به انجام این عملیات اطمینان دارید؟',
    'warning_delete_msg' => 'در صورت حذف، داده‌ها قابل بازیابی نیستند!',
    'success_delete' => 'حذف شد!',
    'success_delete_msg' => 'عملیات حذف با موفقیت انجام شد!',
];
