<?php
/**
 * Created by PhpStorm.
 * User: Saeid
 * Date: 15/01/2018
 * Time: 10:00 PM
 */
namespace Modules\Log\Config;

use App\Menu;
use App\Permission;
use Artisan;
use App\Role;

class ModuleSetting
{
    public $en_name = 'Log';
    public $fa_name = 'فعالیت';
    public $fa_name_plural = 'فعالیت ها';
    public $icon = 'toc';

    public function setup(){

        $permissions = [
            'display_name' => 'لیست ' . $this->fa_name_plural
        ];
        $list_permission = Permission::updateOrCreate(['name' => 'log.list'], $permissions);

        $menu1 = [
            'display_name'  => $this->fa_name,
            'icon'          => $this->icon
        ];
        $menu_parent = Menu::updateOrCreate(['name' => 'Log'], $menu1);


        $menu3 = [
            'display_name' => 'لیست ' . $this->fa_name_plural,
            'parent_id'    => $menu_parent->id,
            'url'          => 'log.list'
        ];
        Menu::updateOrCreate([
            'name' => 'log.list'
        ], $menu3);

        $role = Role::where('name','super-admin')->first();
        if(!is_null($role)){
            $role->attachPermission($list_permission);
        }

        Artisan::call('module:migrate', ['module' => 'Log']);

    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', 'log%')->delete();

        Menu::where('name', 'like', 'log%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => 'Log' ]);
    }
}