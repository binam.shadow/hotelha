<?php

Route::group([
    'middleware' => ['web', 'permission:user*'],
    'prefix' => 'admin/user',
    'namespace' => 'Modules\User\Http\Controllers'
], function() {

	/*** Start User ***/
	Route::get('create', [
		'as'	=> 'user.create',
		'uses'	=> 'UserController@create'
	]);
	Route::post('create', [
		'as'	=> 'user.create',
		'uses'	=> 'UserController@store'
	]);
	Route::get('edit/{id?}', [
		'as'	=> 'user.edit',
		'uses'	=> 'UserController@edit'
	]);
	Route::post('edit/{id?}', [
		'as'	=> 'user.edit',
		'uses'	=> 'UserController@update'
	]);
	Route::get('list', [
		'as'	=> 'user.list',
		'uses'	=> 'UserController@list'
	]);
	Route::get('dataList', [
		'as'	=> 'user.dataList',
		'uses'	=> 'UserController@dataList'
	]);
	Route::get('activate', [
		'as'	=> 'user.activate',
		'uses'	=> 'UserController@activate'
	]);
	Route::get('activat', [
		'as'	=> 'user.activat',
		'uses'	=> 'UserController@activat'
	]);
	Route::get('delete', [
		'as'	=> 'user.delete',
		'uses'	=> 'UserController@delete'
	]);
	Route::post('avatarDelete', [
		'as'	=> 'user.avatarDelete',
		'uses'	=> 'UserController@avatarDelete'
	]);
	/*** End User ***/

});
