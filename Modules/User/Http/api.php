<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\User\Http\Controllers'
], function() {

    Route::get('login', [
        'uses'	=> 'ApiUserController@login'
    ]);
    Route::get('updateProfile', [
        'uses'	=> 'ApiUserController@updateProfile'
    ]);
    Route::get('showProfile', [
        'uses'	=> 'ApiUserController@showProfile'
    ]);
    Route::get('avatarDelete', [
        'uses'	=> 'ApiUserController@avatarDelete'
    ]);
});
Route::group([
    'middleware' => ['auth:api', 'role:user'],
    'prefix' => 'api',
    'namespace' => 'Modules\User\Http\Controllers'
], function() {

    Route::post('uploadAvatar', [
        'uses'	=> 'ApiUserController@uploadAvatar'
    ]);
});

Route::group([
    'prefix' => 'api',
    'namespace' => 'Modules\User\Http\Controllers'
], function() {

    Route::get('register', [
        'uses'	=> 'ApiUserController@register'
    ]);
    Route::get('getToken', [
        'uses'	=> 'ApiUserController@getToken'
    ]);
});