<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Facades\jDateTime;
use App\Classes\FileManager;
use App\User;
use App\Role;
use App\Medium;
use Validator;
use Entrust;
use DataTables;
use Modules\Hotel\Entities\Hotel;

class UserController extends Controller {

    public function create() {
        Entrust::can('user.create') ?  : abort(403);

        return view('user::create', compact('roles'));
    }

    public function store(Request $request) {
        Entrust::can('user.create') ?  : abort(403);

        $validator = Validator::make($request->all(), [
            'phone'      => 'required',
            'email'     => 'nullable|email',
            'password'  => 'nullable|min:5',
            'confirm_password'  => 'same:password'
        ]);
        if($validator->fails()){
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Get name and email
        $user = $request->all();//only('name', 'email', 'phone');
        $errors = new Collection();
        $errors->push(\Lang::get('admin::default.phone_not_unique'));
        $user_phone = User::where('phone', $user['phone'])->first();
        if (count($user_phone))
            return back()
                ->with('errors', $errors)
                ->withInput();

        // Hash Password
        $user['password'] = bcrypt($request->password);

        // Check sctive or deactive (Default is false)
        $user['active'] = 0;
        if($request->active)
            $user['active'] = 1;


        // Create admin
        $user = User::create($user);

        // Attach role to new user
        $role = Role::where('name', 'user')->first();
        $user->attachRole($role->id);

        if($request->avatar)
    		Medium::make($request->avatar, 'User Avatar', [
    			 'position' => 0,
    			 'model' 	=> 'users',
    			 'model_id' => $user->id,
    			 'description'	=> 'User Avatar'
    		 ])->store();

        // Redirect with success message
        return redirect()->route('user.create')
                    ->with('msg', 'کاربر با موفقیت ایجاد شد')
                    ->with('msg_color', 'bg-green');
    }

    public function edit($id = 0) {
        Entrust::can('user.edit') ?  : abort(403);
        // Check Admin exist
        $user = User::where('id', $id)->first();
        if(!count($user)) abort(404);

        return view('user::edit', compact('user'));
    }

    public function update($id, Request $request) {
        Entrust::can('user.edit') ?  : abort(403);

        $validator = Validator::make($request->all(), [
            'phone'      => 'required',
            'email'     => 'nullable|email',
            'password'  => 'nullable|min:5',
            'confirm_password'  => 'same:password'
        ]);
        if($validator->fails()){
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $errors = new Collection();
        $errors->push(\Lang::get('admin::default.phone_not_unique'));
        $user_phone = User::where('phone', $request->phone)->first();
        if (count($user_phone) && $user_phone->id != $id)
            return back()
                ->with('errors', $errors)
                ->withInput();
        $user = User::find($id);
        $data = [
            'phone' => $request->phone
        ];
        // Check new password Entered
        if($request->password)
            $data['password'] = bcrypt($request->password);
        if($request->email)
            $data['email'] = $request->email;
        if($request->name)
            $data['name'] = $request->name;

        // Check active
        $data['active'] = 0;
        if($request->active)
            $data['active'] = 1;


		$user->update($data);

		if($request->avatar){
			if(!is_null($user->thumb)) $user->thumb->remove();
		   	Medium::make($request->avatar, 'User Avatar', [
				'position' 		=> 0,
				'model' 		=> 'users',
				'model_id' 		=> $id,
				'description'	=> 'User Avatar'
				])->store();
		}
        return redirect()->route('user.list')
                    ->with('msg', \Lang::get('user::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    public function list() {
        Entrust::can('user.list') ?  : abort(403);

        return view('user::list');
    }

    public function dataList(Request $request) {
        Entrust::can('user.list') ?  : abort(403);

        // $data['draw'] =  $request->draw;
        // $data['data'] = Role::with(['users' => function ($query) use ($request) {
        //     $order_column = $request->order[0]['column'];
        //     $order_dir = $request->order[0]['dir'];
        //     $query->where(function ($query) use($request) {
        //         $query->where("name", "LIKE", "%" . $request->search['value'] . "%")
        //             ->orWhere("phone", "LIKE", "%" . $request->search['value'] . "%");
        //     })
        //         ->orderBy('is_seen', 'ASC')
        //         ->orderBy($request->columns[$order_column]['data'], $order_dir)
        //         ->limit($request->length)
        //         ->offset($request->start);
        //     }])
        //     ->where('name', 'user')
        //     ->get()[0]->users;
        //
        // DB::table('users')->update(['is_seen' => 1]);
        // $count = Role::with(['users' => function ($query) use ($request) {
        //     $query->where(function ($query) use($request) {
        //         $query->where("name", "LIKE", "%" . $request->search['value'] . "%")
        //             ->orWhere("phone", "LIKE", "%" . $request->search['value'] . "%");
        //     });}])
        //     ->where('name', 'user')
        //     ->get()[0]->users->count();
        //
        // $data['recordsTotal'] = $count;
        // $data['recordsFiltered'] = $count;//$data['data']->count();
		DB::table('users')->update(['is_seen' => 1]);
		$model = Role::with('users')
		            ->where('name', 'user')
		            ->get()[0]->users;

    	$data = DataTables::of($model)
						->addColumn('date_req_persian', function(User $user) {
							$date_req = explode(' ', $user->created_at);
					        return $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
                		})->toJson();
        // foreach ($data['data'] as $item) {
        //     $date_req = explode(' ', $item->created_at);
        //     $item->date_req_persian = $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
        // }
        return $data;
    }

    public function activate(Request $request) {
        Entrust::can('user.edit') ?  : abort(403);

        // Set default False (if action is active set True)
        $data['active'] = $request->action;

        // Update User
        User::where('id', $request->id)->update($data);

        // Redirect with success message
        return ['status' => 'success'];
    }

    public function activat(Request $request) {
        
        // Set default False (if action is active set True)
        $data['active'] = $request->action;

        // Update User
        Hotel::where('id', $request->id)->update($data);

        // Redirect with success message
        return ['status' => 'success'];
    }
    
    

    public function delete(Request $request) {
        Entrust::can('user.edit') ?  : abort(403);

        $user = User::find($request->id);
        FileManager::delete($user->avatar);
        $user->delete();
        return ['status' => 'success'];
    }

    public function avatarDelete(Request $request) {
        Entrust::can('user.edit') ?  : abort(403);

		Medium::find($request->key)->remove();

        return ['status' => 'success'];
    }
}
