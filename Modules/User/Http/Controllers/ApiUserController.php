<?php

namespace Modules\User\Http\Controllers;

use App\Classes\Encryption;
use App\Classes\FileManager;
use App\Classes\PersianTools;
use App\Classes\Smsir;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use App\Role;
use Entrust;
use Validator;
use App\Classes\Notification;

class ApiUserController extends Controller {

    /**
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request) {
        $rules = [
            'phone'   => 'required|min:10',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails())
            return ['error' => 1, 'msg' => \Lang::get('user::default.wrong_input')];
        $request->phone = PersianTools::numberConvertor($request->phone);
        $activation_code = (string)rand(1000, 9999);
        $user = User::where('phone', $request->phone)->first();
        if(!count($user)) {
            $code = str_random(10);
            while(count(User::where('code', $code)->first()))
                $code = str_random(10);
            $user_data = [
                'phone' => $request->phone,
                'password' => bcrypt($activation_code),
                'activation_code' => $activation_code,
                'code' => $code,
                'active' =>  1
            ];
            if($request->recommend_code != '') $user_data['recommend_code'] = $request->recommend_code;
            $user = User::create($user_data);
            $role = Role::where('name', 'user')->first();
            $user->roles()->save($role);
            Smsir::sendVerification($activation_code, $request->phone);
            $content = \Lang::get('user::default.new_user');
            $notification = new Notification();
            $notification->sendWebNotification($content, url('admin/user/list'));
            $token = $user->createToken('Login Token')->accessToken;
            return ['error' => 0, 'token' => $token];
        }
        else {
            $user->activation_code = $activation_code;
            $user->verified = 0;
            $user->password = bcrypt($activation_code);
            $user->save();
            Smsir::sendVerification($activation_code, $request->phone);
            $token = $user->createToken('Login Token User ' . $user->id)->accessToken;
            return ['error' => 0, 'token' => $token];
        }
        return ['error' => 2, 'msg' => \Lang::get('user::default.phone_exist')];
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getToken(Request $request) {
        $rules = [
            'phone'   => 'required|min:10',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails())
            return ['error' => 1, 'msg' => \Lang::get('user::default.wrong_input')];
        $request->phone = PersianTools::numberConvertor($request->phone);
        $activation_code = (string)rand(1000, 9999);
        $user = User::where('phone', $request->phone)->first();
        if(count($user)) {
            $user->activation_code = $activation_code;
            $user->verified = 0;
            $user->password = bcrypt($activation_code);
            $user->save();
            Smsir::sendVerification($activation_code, $request->phone);
            $token = $user->createToken('Login Token User ' . $user->id)->accessToken;
            return ['error' => 0, 'token' => $token];
        }
        return ['error' => 2, 'msg' => \Lang::get('user::default.phone_not_exist')];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function login(Request $request) {
        $user = \Auth::user();
        if($user->phone == '09014889596' && $request->activation_code == '9596'){
            $user->verified = 1;
            $user->save();
            $data = [
                'error' => 0,
                'msg' => \Lang::get('user::default.login_success')
            ];
            return Encryption::encryptor($data, $request->key);
        }
        if($request->activation_code == $user->activation_code) {
            $activation_code = (string)rand(1000, 9999);
            $user->activation_code = $activation_code;
            $user->verified = 1;
            $user->save();
            if(!$user->active){
                $data = ['error' => 2, 'msg' => \Lang::get('user::default.not_active_by_admin')];
                return Encryption::encryptor($data, $request->key);
            }
            $data = [
                'error' => 0,
                'msg' => \Lang::get('user::default.login_success'),
            ];
            if($user->city_id)
                $data['user_city'] = $user->city_id;
            if($user->province_id)
                $data['user_province'] = $user->province_id;
            return Encryption::encryptor($data, $request->key);
        }
        $data = ['error' => 1, 'msg' => \Lang::get('user::default.wrong_code')];
        return Encryption::encryptor($data, $request->key);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateProfile(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $user = User::find($user->id);
        if($request->birthday != '') $user->birthday = $request->birthday;
        if($request->name != '') $user->name = $request->name;
        if($request->sex != '') $user->sex = $request->sex;
        if($request->recommend_code != '') $user->recommend_code = $request->recommend_code;
        if($request->email != '') $user->email = $request->email;
        if($request->address != '') $user->address = $request->address;
        if($request->bio != '') $user->bio = $request->bio;
        if($request->city_id != '') $user->city_id = $request->city_id;
		if($request->province_id != '') $user->province_id = $request->province_id;
		if($request->one_signal != '') $user->one_signal = $request->one_signal;
		if($request->client_type != '') $user->client_type = $request->client_type;
        if($request->unread_message != '') $user->unread_message = $request->unread_message;
        $user->save();
        $data = [
            'error' => 0,
            'msg' => \Lang::get('user::default.success_edit')
        ];
        return Encryption::encryptor($data, $request->key);
    }

    public function uploadAvatar(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if ($request->avatar) {
            $image = $request->avatar;
            $image_path = FileManager::uploader('images/avatars', $image);
            FileManager::delete($user->avatar);
            User::find($user->id)->update(['avatar' => $image_path]);
            $data = ['error' => 0, 'msg' => \Lang::get('user::default.success')];
            return Encryption::encryptor($data, $request->key);
        }
    }

    /**
     * @param Request $request
     */
    public function avatarDelete(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $user = User::find($user->id);
        FileManager::delete($user->avatar);
        $user->avatar = null;
        $user->save();

        $data = ['error' => 0, 'msg' => \Lang::get('user::default.success')];
        return Encryption::encryptor($data, $request->key);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function showProfile(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $data = User::find($user->id);
        return Encryption::encryptor($data, $request->key);
    }

}
