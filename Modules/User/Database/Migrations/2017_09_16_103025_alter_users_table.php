<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone', 20)->unique();
            $table->string('avatar', 256)->nullable();
            $table->string('one_signal', 256)->nullable();
            $table->string('client_type', 256)->nullable();
            $table->integer('unread_message')->unsigned()->default(0);
            $table->string('code', 10)->nullable();
            $table->string('activation_code', 256)->nullable();
            $table->boolean('is_seen')->default(0);
            $table->string('address', 256)->nullable();
            $table->string('postal_code', 15)->nullable();
            $table->integer('province_id')->unsigned()->default(1);
            $table->integer('city_id')->unsigned()->default(1);

            $table->text('bio')->nullable();
            $table->string('birthday', 15)->nullable();
            $table->string('sex', 15)->nullable();
            $table->string('recommend_code', 25)->nullable();
            $table->timestamp('logged_in_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn([
                'phone',
                'avatar',
                'one_signal',
                'client_type',
                'unread_message',
                'activation_code',
                'is_seen',
                'address',
                'postal_code',
                'province_id',
                'city_id',
                'birthday',
                'sex',
                'recommend_code',
                'logged_in_at'
            ]);
        });
    }
}
