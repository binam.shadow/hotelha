<?php

Route::group([
    'middleware' => ['web', 'role:admin|role:owner'],
    'prefix' => 'admin/payment',
    'namespace' => 'Modules\Payment\Http\Controllers'
], function() {

    Route::get('edit', [
        'as'    => 'payment.edit',
        'uses'  => 'PaymentController@edit'
    ]);
    Route::post('edit', [
        'as'    => 'payment.edit',
        'uses'  => 'PaymentController@update'
    ]);
    
});

Route::group([
    'middleware' => ['web'],
    'prefix' => 'payment',
    'namespace' => 'Modules\Payment\Http\Controllers'
], function() {
    Route::any('/callbackURL', [
        'uses' => 'UserPaymentController@paymentResult'
    ]);
    Route::get('/', [
        'uses' => 'UserPaymentController@paymentStart'
    ]);
});