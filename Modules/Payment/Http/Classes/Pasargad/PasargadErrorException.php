<?php

namespace Modules\Payment\Http\Classes\Pasargad;

use Modules\Payment\Http\Classes\Exceptions\BankException;

class PasargadErrorException extends BankException {}
