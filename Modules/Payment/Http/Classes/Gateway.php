<?php

namespace Modules\Payment\Http\Classes;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Modules\Payment\Http\Classes\GatewayResolver
 */
class Gateway extends Facade
{
	/**
	 * The name of the binding in the IoC container.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'gateway';
	}
}
