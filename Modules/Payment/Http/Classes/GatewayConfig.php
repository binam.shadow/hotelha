<?php
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 9/17/2017
 * Time: 10:51 AM
 */

namespace Modules\Payment\Http\Classes;

use App\Setting;

class GatewayConfig {
    public $config;
    public $default;

    public function getConfig() {
        $gatewayConfigList = Setting::whereIn('key', ['saman', 'mellat', 'jahanpay', 'parsian', 'pasargad', 'payline', 'sadad', 'saman', 'zarinpal'])->get();
        $config['timezone'] = 'Asia/Tehran';
        $config['table'] = 'gateway_transactions';

        foreach($gatewayConfigList as $bank){
            $bankGetwaySettings = json_decode($bank->value, true);
            $config[$bank->key] = $bankGetwaySettings;
        }
        $this->config = $config;
        return $this;
    }

    public function setDefault($bank){
        $this->default = $bank;
    }

    public function get($key) {
        $keys = explode('.', $key);
        if(count($keys) == 1)
            return $this->config[$key];
        if($keys[0] == 'gateway' && count($keys) == 2){
            $key = $keys[1];
            return $this->config[$key];
        }
        else if($keys[0] == 'gateway'){
            $this->default = $keys[1];
            $key = $keys[2];
        }
        else{
            $this->default = $keys[0];
            $key = $keys[1];
        }
        return $this->config[$this->default][$key];
    }
}