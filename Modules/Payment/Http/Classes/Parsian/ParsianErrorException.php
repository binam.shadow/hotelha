<?php

namespace Modules\Payment\Http\Classes\Parsian;

use Modules\Payment\Http\Classes\Exceptions\BankException;

class ParsianErrorException extends BankException {}
