<?php

return [
	'new_payment' => 'پرداختی جدید توسط کاربر انجام شد',
	'email'		=> 'ایمیل',
	'password'	=> 'رمز عبور',
	'roham_web'	=> 'رهام وب',
	'admin_panel' => 'پنل مدیریت',
	'dashboard'	=> 'داشبورد',
	'home'		=> 'خانه',
	'name'		=> 'نام',
	'user_name'		=> 'نام کاربری',
	'setting'	=> 'تنظیمات',
	'success_edit'=> 'تغییرات با موفقیت ذخیره شد',
	'are_you_sure'=> 'آیا شما مطمئن هستید',
	'warning_delete_msg' => 'در صورت حذف، اطلاعات قابل بازگردانی نیستند.',
	'success_delete_msg' => 'اطلاعات مورد نظر با موفقیت حذف شدند',
	'cancel'	=> 'انصراف',
	'confirm'	=> 'تایید',
	'success_delete' => 'حذف شد!',
	'success_create' => 'اطلاعات با موفقیت ذخیره شدند',
	'fa_name'	=> 'نام فارسی',
	'en_name'	=> 'نام انگلیسی',
	'yes'		=> 'بله',
	'no'		=> 'خیر',
	'active'	=> 'فعال',
	'deactive'	=> 'غیر فعال',
	'payment_settings' => 'تنظیمات درگاه',
	'merchant' => 'مرچنت',
	'callback_url' => 'CallBack Url',
	'terminal_id' => 'Terminal ID',
	'transaction_key' => 'Transaction Key',
	'mobile' => 'موبایل',
	'pin' => 'پین',
	'description' => 'توضیحات',
	'server' => 'سرور',
	'type' => 'نوع',
];
