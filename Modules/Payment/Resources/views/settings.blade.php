@extends('adminbsb.layouts.master')

@section('title')
@lang('payment::default.payment_settings')
@endsection

@section('styles')
<!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
.file-footer-caption, .file-thumbnail-footer{
	display: none;
}
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('payment::default.payment_settings')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<ul class="nav nav-tabs tab-nav-right" role="tablist">
					<li role="presentation" class="active"><a href="#mellat" data-toggle="tab" aria-expanded="false"><img src="{{ asset('images/mellat.png') }}" height="90px"></a></li>
					<li role="presentation"><a href="#saman" data-toggle="tab" aria-expanded="false"><img src="{{ asset('images/sb.png') }}" height="90px"></a></li>
					<li role="presentation"><a href="#bmi" data-toggle="tab" aria-expanded="true"><img src="{{ asset('images/bmi.png') }}" height="90px"></a></li>
					<li role="presentation"><a href="#parsian" data-toggle="tab" aria-expanded="true"><img src="{{ asset('images/parsian.png') }}" height="90px"></a></li>
					<li role="presentation"><a href="#zarinpal" data-toggle="tab" style="padding-top: 20px;padding-bottom: 20px;"><img src="{{ asset('images/zarinpal.png') }}" height="70px"></a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="mellat">
						<form action="{{ route('payment.edit') }}?gateway=mellat" method="post">
							{{ csrf_field() }}
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="username" @if(isset($mellat['username'])) value="{{$mellat['username']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.user_name')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="password"  @if(isset($mellat['password'])) value="{{$mellat['password']}}" @endif required="" aria-required="true" type="text" >
									<label class="form-label">@lang('payment::default.password')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="terminalId" @if(isset($mellat['terminalId'])) value="{{$mellat['terminalId']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.terminal_id')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="callback-url" @if(isset($mellat['callback-url'])) value="{{$mellat['callback-url']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.callback_url')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="switch">
									<label>@lang('payment::default.active') <input name="active" @if(in_array('mellat', $active_gateways)) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('payment::default.deactive')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<button type="submit" class="btn btn-primary">ذخیره تنظیمات</button>
							</div>
						</form>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="saman">
						<form action="{{ route('payment.edit') }}?gateway=saman" method="post">
							{{ csrf_field() }}
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="merchant" @if(isset($saman['merchant'])) value="{{$saman['merchant']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.merchant')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="password" @if(isset($saman['password'])) value="{{$saman['password']}}" @endif required="" aria-required="true" type="text" >
									<label class="form-label">@lang('payment::default.password')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="callback-url" @if(isset($saman['callback-url'])) value="{{$saman['callback-url']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.callback_url')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="switch">
									<label>@lang('payment::default.active') <input name="active" @if(in_array('saman', $active_gateways)) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('payment::default.deactive')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<button type="submit" class="btn btn-primary">ذخیره تنظیمات</button>
							</div>
						</form>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="bmi">
						<form action="{{ route('payment.edit') }}?gateway=sadad" method="post">
							{{ csrf_field() }}
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="merchant"  @if(isset($sadad['merchant'])) value="{{$sadad['merchant']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.merchant')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="transactionKey"  @if(isset($sadad['transactionKey'])) value="{{$sadad['transactionKey']}}" @endif required="" aria-required="true" type="text" >
									<label class="form-label">@lang('payment::default.transaction_key')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="terminalId" @if(isset($sadad['terminalId'])) value="{{$sadad['terminalId']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.terminal_id')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="callback-url" @if(isset($sadad['callback-url'])) value="{{$sadad['callback-url']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.callback_url')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="switch">
									<label>@lang('payment::default.active') <input name="active" @if(in_array('sadad', $active_gateways)) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('payment::default.deactive')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<button type="submit" class="btn btn-primary">ذخیره تنظیمات</button>
							</div>
						</form>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="parsian">
						<form action="{{ route('payment.edit') }}?gateway=parsian" method="post">
							{{ csrf_field() }}
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="pin" @if(isset($parsian['pin'])) value="{{$parsian['pin']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.pin')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="callback-url" @if(isset($parsian['callback-url'])) value="{{$parsian['callback-url']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.callback_url')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="switch">
									<label>@lang('payment::default.active') <input name="active" @if(in_array('parsian', $active_gateways)) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('payment::default.deactive')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<button type="submit" class="btn btn-primary">ذخیره تنظیمات</button>
							</div>
						</form>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="zarinpal">
						<form action="{{ route('payment.edit') }}?gateway=zarinpal" method="post">
							{{ csrf_field() }}
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="merchant" @if(isset($zarinpal['merchant'])) value="{{$zarinpal['merchant']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.merchant')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<label class="form-label">@lang('payment::default.type')</label><br>
								<select name="type">
									<option value="zarin-gate" @if(isset($zarinpal['type']) && $zarinpal['type'] == 'zarin-gate') selected @endif>Zarin Gate</option>
									<option value="normal" @if(isset($zarinpal['type']) && $zarinpal['type'] == 'normal') selected @endif>Normal</option>
								</select>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="callback-url" @if(isset($zarinpal['callback-url'])) value="{{$zarinpal['callback-url']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.callback_url')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<label class="form-label">@lang('payment::default.server')</label><br>
								<select name="server">
									<option value="germany" @if(isset($zarinpal['server']) && $zarinpal['server'] == 'germany') selected @endif>Germany</option>
									<option value="iran" @if(isset($zarinpal['server']) && $zarinpal['server'] == 'iran') selected @endif>Iran</option>
									<option value="test" @if(isset($zarinpal['server']) && $zarinpal['server'] == 'test') selected @endif>Test</option>
								</select>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="email" @if(isset($zarinpal['email'])) value="{{$zarinpal['email']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.email')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="mobile" @if(isset($zarinpal['mobile'])) value="{{$zarinpal['mobile']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.mobile')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="description" @if(isset($zarinpal['description'])) value="{{$zarinpal['description']}}" @endif required="" aria-required="true" type="text">
									<label class="form-label">@lang('payment::default.description')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="switch">
									<label>@lang('payment::default.active') <input name="active" @if(in_array('zarinpal', $active_gateways)) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('payment::default.deactive')</label>
								</div>
							</div>
							<div class="form-group form-float">
								<button type="submit" class="btn btn-primary">ذخیره تنظیمات</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/select2/select2.min.js') }}"></script>
<script>
$('#adminCreate').validate({
	rules: {
		'name': {
			required: true
		}
	}
});
</script>
@endsection
