<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $table = 'gateway_transactions';

    protected $fillable = [
      'id' , 'port' , 'price' , 'status' , 'ip'
    ];
}
