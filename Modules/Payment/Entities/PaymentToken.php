<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;

class PaymentToken extends Model
{
    public $table = 'payment_tokens';

    protected $fillable = [
      'id' , 'user_id', 'token'
    ];
}
