<?php
namespace Modules\Message\Entities;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $table = 'messages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title',
        'description',
        'type',
        'user_id',
        'receiver_id',
        'phone',
        'is_seen',
		'show_in_inbox'
    ];
}
