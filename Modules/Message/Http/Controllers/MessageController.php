<?php

namespace Modules\Message\Http\Controllers;

use App\Classes\Notification;
use App\Classes\Smsir;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Entrust;
use Modules\Message\Entities\Message;
use Modules\Message\Http\Requests\MessageRequest;
use Morilog\Jalali\Facades\jDateTime;

class MessageController extends Controller {

    public function create() {
        Entrust::can('message.create') ?  : abort(403);
        return view('message::create');
    }

    public function store(MessageRequest $request) {
        Entrust::can('message.create') ?  : abort(403);

        $message = $request->only(['title', 'description', 'type']);

		// show in inbox?
		$message['show_in_inbox'] = 0;
		if($request->show_in_inbox)
			$message['show_in_inbox'] = 1;

        $message['receiver_id'] = $request->receiver_id;
        $message_id = Message::create($message);

		// update unread_message for 1 user
		if($request->receiver_id != 0 && $request->show_in_inbox){
			$user = User::find($request->receiver_id);
			$user->unread_message = 1;
			$user->save();
		}

		// update unread_message for all users
		if($request->receiver_id == 0 && $request->show_in_inbox){
			User::where('unread_message', 0)->update([
				'unread_message'=> 1,
				'updated_at'	=> \DB::raw('updated_at')
			]);
		}

        // Redirect with success message
        return redirect()->route('message.create')
            ->with('msg', \Lang::get('admin::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    public function edit($id = 0) {
        Entrust::can('message.edit') ?  : abort(403);

        $message = Message::find($id);

		$user = User::where('id', $message->receiver_id)->first();

        return view('message::edit', compact('message', 'user'));
    }

    public function update($id, MessageRequest $request) {
        Entrust::can('message.edit') ?  : abort(403);

		$show_in_inbox = 0;
		if($request->show_in_inbox)
			$show_in_inbox = 1;

        $message = Message::find($id);
        $message->title = $request->title;
        $message->description = $request->description;
		$message->type = $request->type;
		$message->receiver_id = $request->receiver_id;
        $message->show_in_inbox = $show_in_inbox;
        $message->save();

        return redirect()->route('message.list')
            ->with('msg', \Lang::get('admin::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function list() {
        Entrust::can('message.list') ?  : abort(403);

        $users = User::select('id', 'name', 'phone')->get();
        return view('message::list', compact('users'));
    }

    public function dataList(Request $request) {
        Entrust::can('message.list') ?  : abort(403);

        $limit = $request->length;
        $offset = $request->start;
        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $data['draw'] =  $request->draw;
        $data['data'] = Message::Where(function ($query) {
            $query->where("type", "sms")
                ->orWhere("type", "notification");
            })->Where(function ($query) use($request) {
            $query->where("title", "LIKE", "%" . $request->search['value'] . "%")
                ->orWhere("description", "LIKE", "%" . $request->search['value'] . "%");
            })
            ->offset($offset)
            ->limit($limit)
            ->orderBy('is_seen', 'ASC')
            ->orderBy($request->columns[$order_column]['data'], $order_dir)
            ->get();

        Message::offset($offset)->limit($limit)->update(['is_seen' => 1]);

        $count = Message::count();
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();

        foreach ($data['data'] as $item) {
            $date_req = explode(' ', $item->created_at);
            $item->date_req_persian = $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
        }
        return $data;
    }

    public function activate(Request $request) {
        Entrust::can('message.edit') ?  : abort(403);

        // Set default False (if action is active set True)
        $data['active'] = $request->action;

        // Update Message
        Message::find($request->id)->update($data);

        // Redirect with success message
        return ['status' => 'success'];
    }

    public function delete(Request $request) {
        Entrust::can('message.edit') ?  : abort(403);
        Message::find($request->id)->forceDelete();
        return ['status' => 'success'];
    }

    public function sendAll(Request $request) {
        Entrust::can('message.list') ?  : abort(403);
        $message = Message::find($request->id);
        $content = $message->title . "\n" . strip_tags($message->description);
        if($message->type == 'notification'){
            $notification = new Notification();
            $notification->sendNotificationToAll($content);
        }
        elseif($message->type == 'sms'){
            $users = User::get();
            $messages = array();
            $user_numbers = array();
            foreach($users as $user){
                array_push($messages, $content);
                array_push($user_numbers, $user->phone);
            }
            Smsir::sendMessage($messages, $user_numbers);
        }
        return ['status' => 'success'];
    }
    public function send($id = null, Request $request) {
        Entrust::can('message.list') ?  : abort(403);
        $message = Message::select('title', 'type', 'user_id', 'show_in_inbox', 'description')->find($request->id)->toArray();
        $message['receiver_id'] = $id;
        Message::create($message);
        $content = $message['title'] . "\n" . strip_tags($message['description']);
        if($message['type'] == 'notification'){
			$user = User::find($id);
           	$notification = new Notification();
           	$notification->sendNotification($content, [$user->one_signal]);
        }
        elseif($message['type'] == 'sms'){
            $user = User::find($id);
            $messages = array();
            $user_numbers = array();
            array_push($messages, $content);
            array_push($user_numbers, $user->phone);
            Smsir::sendMessage($messages, $user_numbers);
        }
        return ['status' => 'success'];
    }
    public function getUserData(Request $request) {
        Entrust::can('message.list') ?  : abort(403);

        return User::find($request->id);
    }

	public function getUsers(Request $request) {
		Entrust::can('message.list') ?  : abort(403);

		$users = User::select('id', 'phone')
					->where('phone', 'like', '%' . $request->phone . '%')
					->get();

		return $users;
	}
}
