<?php

namespace Modules\Message\Http\Controllers;

use App\Classes\Encryption;
use App\Classes\PersianTools;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use App\Role;
use Entrust;
use Modules\Message\Entities\Message;
use Morilog\Jalali\Facades\jDate;
use Validator;
use App\Classes\Notification;

class ApiMessageController extends Controller {

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendMessageByUser(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $message = array();
        $message['title'] = $request->title;
        $message['description'] = $request->description;
        $message['type'] = $request->type;
        $message['user_id'] = $user->id;
        $message_id = Message::create($message);
        //
        // push notification to Admin Panel
        $content = \Lang::get('message::default.new_received_message');
        $notification = new Notification();
        $notification->sendWebNotification($content, url('admin/message/receivedList'));
        //
        $data = ['error' => 0, 'id' => $message_id->id, 'msg' => \Lang::get('message::default.success_edit')];
        return Encryption::encryptor($data, $request->key);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function consultationRequest(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $message = array();
        $message['title'] = $request->title;
        $message['description'] = $request->description;
        $message['type'] = 'consultationRequest';
        $message['user_id'] = $user->id;
        $message_id = Message::create($message);
        //
        // push notification to Admin Panel
        $content = \Lang::get('message::default.new_received_message');
        $notification = new Notification();
        $notification->sendWebNotification($content, url('admin/message/receivedList'));
        //
        $data = ['error' => 0, 'id' => $message_id->id, 'msg' => \Lang::get('message::default.success_edit')];
        return Encryption::encryptor($data, $request->key);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function messageTitles(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $titles = Setting::where('key', 'message_received_title')->pluck('value');
        $data = ['error' => 0, 'msg' => $titles];
        return Encryption::encryptor($data, $request->key);
    }

    public function getMessages(Request $request){
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if($request->limit) {
            $limit = $request->limit;
            $offset = $request->offset;
            $data = Message::where(function ($query) use($user) {
				$query->where('receiver_id', $user->id)
                	->where('show_in_inbox', 1)
                    ->orWhere('receiver_id', 0);
                })
                ->orderBy('id', 'desc')
                ->offset($offset)
                ->limit($limit)
                ->get();
        }
        else {
            $data = Message::where(function ($query) use($user) {
                $query->where('receiver_id', $user->id)
					->where('show_in_inbox', 1)
                    ->orWhere('receiver_id', 0);
                })
                ->orderBy('messages.id', 'desc')
                ->get();
        }
        foreach($data as $item){
            $item->description = strip_tags($item->description);
            $item->date = jDate::forge($item->created_at)->format('date');
            $weekDay = date('l', strtotime($item->created_at));
            if(strtolower($weekDay) == 'saturday'){
                $item->weekDay = 'شنبه';
            }
            else if(strtolower($weekDay) == 'sunday'){
                $item->weekDay = 'یک شنبه';
            }
            else if(strtolower($weekDay) == 'monday'){
                $item->weekDay = 'دو شنبه';
            }
            else if(strtolower($weekDay) == 'tuesday'){
                $item->weekDay = 'سه شنبه';
            }
            else if(strtolower($weekDay) == 'wednesday'){
                $item->weekDay = 'چهار شنبه';
            }
            else if(strtolower($weekDay) == 'thursday'){
                $item->weekDay = 'پنج شنبه';
            }
            else if(strtolower($weekDay) == 'friday'){
                $item->weekDay = 'جمعه';
            }
        }
        return Encryption::encryptor($data, $request->key);
    }

}
