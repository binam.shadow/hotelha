<?php

namespace Modules\Message\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Entrust;
use Modules\Message\Entities\Message;
use Modules\Message\Http\Requests\MessageRequest;
use App\User;
use Morilog\Jalali\jDateTime;

class ReceivedMessageController extends Controller
{
    public function receivedList() {
        Entrust::can('message.received_list') ?  : abort(403);

        return view('message::received.list');
    }

    public function receivedShow($id) {
        Entrust::can('message.received_list') ?  : abort(403);

        $message = Message::find($id);
        $user = User::find($message->user_id);

        return view('message::received.show', compact('message', 'user'));
    }

    public function receivedDataList(Request $request) {
        Entrust::can('message.received_list') ?  : abort(403);

        $limit = $request->length;
        $offset = $request->start;
        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $data['draw'] =  $request->draw;
        $message = Message::where('type', '!=', 'sms')->where('type', '!=', 'notification')
            ->Where(function ($query) use($request) {
            $query->where("title", "LIKE", "%" . $request->search['value'] . "%")
                ->orWhere("description", "LIKE", "%" . $request->search['value'] . "%");
            })
            ->offset($offset)
            ->limit($limit)
            ->orderBy('is_seen', 'ASC')
            ->orderBy($request->columns[$order_column]['data'], $order_dir);

        $data['data'] = $message->get();
        $message->update(['is_seen' => 1]);

        $count = $data['data']->count();
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();

        foreach ($data['data'] as $item) {
            $date_req = explode(' ', $item->created_at);
            $item->date_req_persian = $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
        }
        return $data;
    }

    public function delete(Request $request) {
        Entrust::can('message.received_edit') ?  : abort(403);
        Message::find($request->id)->delete();
        return ['status' => 'success'];
    }

    public function getReceivedMessageTitle() {
        Entrust::can('message.received_title_edit') ?  : abort(403);

        $titles = Setting::where('key', 'message_received_title')->get();
        return view('message::received.edit_title', compact('titles'));
    }

    public function postReceivedMessageTitle(Request $request) {
        Entrust::can('message.received_title_edit') ?  : abort(403);

        if(isset($request->titles)) {
            Setting::where('key', 'message_received_title')->delete();
            foreach ($request->titles as $title) {
                Setting::create([
                    'key' => 'message_received_title',
                    'value' => $title
                ]);
            }

            // Redirect with success message
            return back()
                ->with('msg', \Lang::get('message::default.success_edit'))
                ->with('msg_color', 'bg-green');
        }
        return back()
            ->with('msg', \Lang::get('message::default.title_empty'))
            ->with('msg_color', 'bg-red');
    }

}
