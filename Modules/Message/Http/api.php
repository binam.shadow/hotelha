<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Message\Http\Controllers'
], function() {

    Route::get('consultationRequest', [
        'uses'	=> 'ApiMessageController@consultationRequest'
    ]);
    Route::get('sendMessageByUser', [
        'uses'	=> 'ApiMessageController@sendMessageByUser'
    ]);
    Route::get('getMessages', [
        'uses'	=> 'ApiMessageController@getMessages'
    ]);
    Route::get('messageTitles', [
        'uses'	=> 'ApiMessageController@messageTitles'
    ]);
});