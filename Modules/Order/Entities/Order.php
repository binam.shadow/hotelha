<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 * @package Modules\Product\Entities
 * @version September 6, 2017, 8:28 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection carts
 * @property \Illuminate\Database\Eloquent\Collection OrderItem
 * @property \Illuminate\Database\Eloquent\Collection orderItemsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsProductCategories
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property integer user_id
 * @property integer city_id
 * @property integer province_id
 * @property string address
 * @property string postal_code
 * @property bigInteger total_price
 * @property string status
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tracking_code',
        'user_id',
        'user_name',
        'city_name',
        'province_name',
        'address',
        'postal_code',
        'total_price',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tracking_code' => 'string',
        'user_id' => 'integer',
        'user_name' => 'string',
        'city_name' => 'string',
        'province_name' => 'string',
        'address' => 'string',
        'postal_code' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orderItems()
    {
        return $this->hasMany(\Modules\Order\Entities\OrderItem::class);
    }
}
