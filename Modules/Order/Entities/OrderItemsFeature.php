<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderItemsFeature
 * @package Modules\Order\Entities
 * @version September 6, 2017, 8:30 am UTC
 *
 * @property \Modules\Order\Entities\Feature feature
 * @property \Modules\Order\Entities\OrderItem orderItem
 * @property \Illuminate\Database\Eloquent\Collection carts
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsOrderCategories
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property integer order_item_id
 * @property integer feature_id
 * @property bigInteger price
 */
class OrderItemsFeature extends Model
{
    use SoftDeletes;

    public $table = 'order_items_features';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_item_id',
        'feature_id',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_item_id' => 'integer',
        'feature_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function feature()
    {
        return $this->belongsTo(\Modules\Product\Entities\Feature::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function orderItem()
    {
        return $this->belongsTo(\Modules\Order\Entities\OrderItem::class);
    }
}
