<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracking_code')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('user_name')->nullable();
            $table->string('city_name')->nullable();
            $table->string('province_name')->nullable();
            $table->string('address', 256)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->bigInteger('total_price')->nullable();
            $table->string('status', 256)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
