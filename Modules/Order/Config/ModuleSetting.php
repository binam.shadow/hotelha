<?php
namespace Modules\Order\Config;
use App\Menu;
use App\Permission;
use App\Role;
use App\User;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    public $en_name = 'Order';
    public $fa_name = 'سفارش';

    /**
     *
     */
    public function setup(){

        Artisan::call('module:migrate', ['module' => $this->en_name]);
        $permissions = [
            'display_name' => 'آرشیو ' . $this->fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.list'], $permissions);

        $menu = [
            'display_name'  => $this->fa_name,
            'icon'          => 'shopping_cart'
        ];
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($this->en_name)], $menu);

        // Sub menu
        $menu2 = [
            'display_name' => 'لیست ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.list'),
        ], $menu2);

    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', strtolower($this->en_name) . '%')->delete();
        Menu::where(['name' => strtolower($this->en_name)])->delete();
        Artisan::call('module:migrate-rollback', ['module' => $this->en_name ]);
    }
}