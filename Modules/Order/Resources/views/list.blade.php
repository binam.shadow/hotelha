@extends('adminbsb.layouts.master')

@section('title')
@lang('order::default.order_list')
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
<link id="datepickerTheme" href="{{asset('css/persian-datepicker.css')}}" rel="stylesheet"/>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
    <div class="dataContent col-lg-12 col-xs-12 col-md-12">
        <div class="tblContainer">
            <div class="panel panel-default" style="width: 100%;margin-right: auto;">
                <div class="panel-heading">
                    <h3 class="panel-title">جستجوی پیشرفته</h3>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label for="searchText">جستجو (کد کاربر یا شماره فاکتور)</label>
                            <input type="text" id="searchText" name="searchText" style="width: 100%;">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">تاریخ سفارش</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="form-line">
                                    <input class="form-control persianDatepicker" name="orderDate1"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                    <input class="form-control persianDatepicker" name="orderDate2"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">جمع کل فاکتور</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="form-line">
                                    <input type="text" class="form-control" name="total1"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                    <input type="text" class="form-control" name="total2"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3" style="float: left">
                        <a class="btn btn-success" onclick="sendData()">جستجو</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('order::default.order_list')</h2>
					</div>
				</div>
			</div>
            <div class="body">
                <div class="table-responsive">
    				<table class="table table-bordered table-striped table-hover dataTable js-exportable user-list">
    					<thead>
                            <tr>
                                <th>id</th>
                                <th>@lang('order::default.user_name')</th>
                                <th>@lang('order::default.date')</th>
                                <th>@lang('order::default.tracking_code')</th>
                                <th>@lang('order::default.total_price')</th>
                                <th>@lang('order::default.status')</th>
                                <th>@lang('order::default.setting')</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>id</th>
                                <th>@lang('order::default.user_name')</th>
                                <th>@lang('order::default.date')</th>
                                <th>@lang('order::default.tracking_code')</th>
                                <th>@lang('order::default.total_price')</th>
                                <th>@lang('order::default.status')</th>
                                <th>@lang('order::default.setting')</th>
                            </tr>
                        </tfoot>
                        <tbody>

                       	</tbody>
    				</table>
                </div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="userModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">@lang('order::default.view_description')</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">@lang('order::default.close')</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
	@include('adminbsb.layouts.datatable_files')
    <script src="{{ asset('js/persian-date.min.js')}}"></script>
    <script src="{{ asset('js/persian-datepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
	<script type="text/javascript">
        $(document).ready(function() {
            $('.persianDatepicker').pDatepicker({
                altFormat: 'LLLL',
                initialValue: false,
                persianDigit: false,
                format: 'YYYY-MM-DD'
            });
        });
		var dataTable = $('.user-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('order.dataList') }}'
            },
            dom: 'Bfrtip',
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {
                    data: 'user_id',
                    "bSortable": false,
                    render: function(data, type, row) {
                        return '<button onclick="showData('+data+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="کاربر ثبت کننده" type="button" class="btn btn-xs btn-primary waves-effect"><i class="material-icons">account_circle</i></button> ';
                    }
                },
                {data: 'date_req_persian', "bSortable": false},
                {data: 'tracking_code'},
                {data: 'total_price'},
                {data: 'status'}
            ],
            columnDefs: [
                {
                    targets: 6,
                    render: function(data, type, row) {
                        var btns = '';
                        @permission('admin.edit')
                            btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
                        @endpermission
                        btns += '&nbsp;<a href="{{ route('order.show') }}/'+row.id+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="نمایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
                        return btns;
                    }
                }
            ],
			buttons: [
            	'copyHtml5', 'excelHtml5', 'print'
        	],
        	language:{
        		url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
        	}
		});

        function showData(id) {
            $.ajax({
                url: "{{ route('order.getUserData') }}",
                data: "id=" + id,
                processData: false,
                contentType: false,
                type: 'GET',
                success: function (result) {
                    var m_modal = $("#userModal");
                    var text_data = '<p class="col-md-6">نام : '+result.name+'</p>'+
                            '<p class="col-md-6">ایمیل : '+result.email+'</p>'+
                            '<p class="col-md-6">شماره تلفن : '+result.phone+'</p>'+
                            '<p class="col-md-6">کد ملی : '+result.national_id+'</p>'+
                            '<p>آدرس</p>'+result.address;
                    m_modal.find('.modal-body').html(text_data);
                    m_modal.modal("show");
                },
                error: function (){
                    alert('مشکلی به وجود آمده است، لطفا دوباره امتحان کنید');
                }
            });
        }

        function showConfirmDelete(id){
            swal({
                title: "@lang('order::default.are_you_sure')",
                text: "@lang('order::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('order::default.cancel')",
                confirmButtonText: "@lang('order::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "{{ route('order.delete') }}",
                        data: "id="+id,
                        processData: false,
                        contentType: false,
                        type: 'GET',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('order::default.success_delete')", "@lang('order::default.success_delete_msg')", "success");
                                dataTable.ajax.reload()
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
	</script>
@endsection
