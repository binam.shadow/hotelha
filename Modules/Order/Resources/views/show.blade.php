@extends('adminbsb.layouts.master')
@section('title')
    @lang('order::default.order_show')
@endsection
@section('styles')
<style>
    .logo {
        float:right;
    }
    .factor_title {
        float: right;
    }
</style>
@endsection
@section('content')
    <div id="min-wrapper"   style="height:auto;">
        @if(session()->has('message'))
            <p id="snackbar">{{ session()->get('message') }}</p>
        @endif
        <div id="main-content">

            <div class="panel panel-default">
                <div class="add_slider_head">
                    <h1>
                        @lang('order::default.order_show')
                    </h1>
                </div>
                <div class="pull-left">
                    <button class="btn btn-success" onclick="print('factor')">@lang('order::default.print')</button>
                </div>
            </div>

            <div id="factor" class="panel panel-default">
                <div class="add_slider_head">
                    <div class="panel-heading">
                        <div class="col-md-4 logo">
                            <img width="265" height="265" style="margin-top:25px;margin-right:-20px;" src="{{ asset('uploads/' . $logo) }}" />
                        </div>

                        <div class="col-md-8 factor_title">
                            <h3>فاکتور فروش</h3>
                            <table class="responsive table-responsive">
                                <tr>
                                    <td>@lang('order::default.name') : </td>
                                    <td>
                                        {{ $user->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>@lang('order::default.address') : </td>
                                    <td>
                                        {{ $user->address }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>@lang('order::default.phone') : </td>
                                    <td>
                                        {{ $user->phone }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>@lang('order::default.postal_code') : </td>
                                    <td>
                                        {{ $user->postal_code }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>@lang('order::default.province')</th>
                                    <th>@lang('order::default.city')</th>
                                    <th>@lang('order::default.factor')</th>
                                    <th>@lang('order::default.status')</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tbody>
                                <tr>
                                    <td>
                                        {{$order->province_name}}
                                    </td>
                                    <td>
                                        {{$order->city_name}}
                                    </td>
                                    <td>
                                        {{$order->id}}
                                    </td>
                                    <td>
                                        {{$order->status}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                    <div style="clear: both"></div>

                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable order-items-list">
                                <thead>
                                <tr>
                                    <th>@lang('order::default.product_id')</th>
                                    <th>@lang('order::default.product_name')</th>
                                    <th>@lang('order::default.count')</th>
                                    <th>@lang('order::default.single_price')</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>@lang('order::default.product_id')</th>
                                    <th>@lang('order::default.product_name')</th>
                                    <th>@lang('order::default.count')</th>
                                    <th>@lang('order::default.single_price')</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            </div>


        </div>


@endsection
@section('scripts')
<script>
function print(elem) {
    console.log(elem);
    var mywindow = window.open('', 'PRINT', 'height='+screen.height+',width='+screen.width+',fullscreen=yes');

    mywindow.document.write('<html><head>'+
            '<style>'+
            '.logo {float:right;}'+
            '.factor_title {float: right;}</style>'+
            '<link href="{{ asset('backend/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">'+
            '<link href="{{ asset('backend/plugins/bootstrap/css/bootstrap-rtl.min.css') }}" rel="stylesheet">'+
            '<link href="{{ asset('backend/css/style-rtl.css') }}" rel="stylesheet">'+
            '<link href="{{ asset('backend/css/themes/all-themes.css') }}" rel="stylesheet">'+
            '<title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    return true;
}
</script>
@include('adminbsb.layouts.datatable_files')
<script type="text/javascript">
    var dataTable = $('.order-items-list').DataTable({
        "order": [[ 0, "desc" ]],
        ajax: {
            url: '{{ route('order_items.dataList') . '/' . $id }}'
        },
        dom: 'Bfrtip',
        responsive: true,
        serverSide: true,
        columns: [
            {data: 'product_id'},
            {data: 'product_name'},
            {data: 'count'},
            {data: 'price'},
                ],
        buttons: [
        ],
        language:{
            url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
        }
    });
</script>
@endsection
