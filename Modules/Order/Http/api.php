<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Order\Http\Controllers'
], function() {

    Route::get('createOrder', [
        'uses'	=> 'ApiOrderController@createOrder'
    ]);
    Route::get('createOrderWithoutCart', [
        'uses'	=> 'ApiOrderController@createOrderWithoutCart'
    ]);
    Route::get('getUserOrders', [
        'uses'	=> 'ApiOrderController@getUserOrders'
    ]);
    Route::get('getOrderDetails', [
        'uses'	=> 'ApiOrderController@getOrderDetails'
    ]);
    Route::get('addToCart', [
        'uses'	=> 'ApiOrderController@addToCart'
    ]);
    Route::get('getOrderStatus', [
        'uses'	=> 'ApiOrderController@getOrderStatus'
    ]);
    Route::get('getUserCart', [
        'uses'	=> 'ApiOrderController@getUserCart'
    ]);
    Route::get('cartCount', [
        'uses'	=> 'ApiOrderController@cartCount'
    ]);
    Route::get('removeFromCart', [
        'uses'	=> 'ApiOrderController@removeFromCart'
    ]);
    Route::get('removeAllInCart', [
        'uses'	=> 'ApiOrderController@removeAllInCart'
    ]);
});
