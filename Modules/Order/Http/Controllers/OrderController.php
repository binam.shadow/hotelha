<?php

namespace Modules\Order\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Entrust;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Modules\Order\Entities\OrderItemsFeature;
use Morilog\Jalali\Facades\jDateTime;
use App\Setting;

class OrderController extends Controller
{
    public function show($id = null) {
        Entrust::can('order.list') ?  : abort(403);
        $order = Order::find($id);
        $user = User::find($order->user_id);
        $logo = Setting::where('key', 'logo')->first()->value;
        return view('order::show', compact('id', 'user', 'order', 'logo'));
    }

    public function orderItemsDataList($id =null, Request $request) {
        Entrust::can('order.list') ?  : abort(403);
        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $limit = $request->length;
        $offset = $request->start;
        $data['draw'] =  $request->draw;
        $data['data'] = OrderItem::where('order_id', $id)
            ->limit($limit)
            ->offset($offset)
            ->get();

        $count = OrderItem::count();
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();

        return $data;
    }

    public function userOrdersList($id = null) {
        return view('order::userorderslist');
    }

    public function userOrdersDataList(Request $request)
    {
        $user = \Auth::user();
        $user_id = $user->id;

        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $limit = $request->length;
        $offset = $request->start;
        $data['draw'] =  $request->draw;
        $data['data'] = Order::where('user_id' , $user_id)
            ->limit($limit)
            ->offset($offset)
            ->orderBy($request->columns[$order_column]['data'], $order_dir)
            ->get();

        $count = Order::count();
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();

        return $data;
    }

    public function list($id = null) {
        Entrust::can('order.list') ?  : abort(403);

        return view('order::list');
    }

    public function dataList($id = null, Request $request) {
        Entrust::can('order.list') ?  : abort(403);
        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $offset = $request->start;
        $limit = $request->length;
        $data = array();
        $search = json_decode($request->search['value']);

        if ($id) {
            $data = Order::where(['user_id' => $id])
                ->orderBy($request->columns[$order_column]['data'], $order_dir)
                ->get();
        }
        else{
            if (empty($search)) {
                $data['data'] = Order::orderBy($request->columns[$order_column]['data'], $order_dir)
                    ->offset($offset)->limit($limit)->get();
                $countAll = Order::count();
            } else {
                $keyword = $search->searchText;
                $keyword = trim($keyword);
                if($keyword == '')
                    $dataField = Order::get();
                else
                    $dataField = Order::Where(function ($query) use($keyword) {
                            $query->where("user_id", "LIKE","%$keyword%")
                                ->orWhere("tracking_code", "LIKE", "%$keyword%");
                        });
                if($search->orderDate1 && $search->orderDate2){
                    $Date1 = explode('-', $search->orderDate1);
                    $Date2 = explode('-', $search->orderDate2);
                    $Date1 = implode('-', jDateTime::toGregorian($Date1[0], $Date1[1], $Date1[2]));
                    $Date2 = implode('-', jDateTime::toGregorian($Date2[0], $Date2[1], $Date2[2]));
                    $dataField->whereBetween('created_at', [$Date1, $Date2]);
                }

                if($search->sendDate1 && $search->sendDate2){
                    $Date1 = explode('-', $search->sendDate1);
                    $Date2 = explode('-', $search->sendDate2);
                    $Date1 = implode('-', jDateTime::toGregorian($Date1[0], $Date1[1], $Date1[2]));
                    $Date2 = implode('-', jDateTime::toGregorian($Date2[0], $Date2[1], $Date2[2]));
                    $dataField->whereBetween('date_send', [$Date1, $Date2]);
                }

                if($search->total1 && $search->total2){
                    $total1 = $search->total1;
                    $total2 = $search->total2;
                    $dataField->whereBetween('full_price', [(int)$total1, (int)$total2]);
                }

                if($search->statusIds != -1){
                    $dataField->where('status', $search->statusIds);
                }

                $t = $dataField->toSql();
                $countAll = $dataField->count();
                $dataField = $dataField->offset($offset)->limit($limit)
                    ->orderBy($request->columns[$order_column]['data'], $order_dir)
                    ->get();

                $data['data'] = $dataField;
            }
        }
        $data['recordsTotal'] = Order::count();
        $data['recordsFiltered'] = $countAll;

        foreach ($data['data'] as $item) {
            $date_req = explode(' ', $item->created_at);

            $item->date_req_persian = $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
        }
        return $data;
    }

    public function getUserData(Request $request) {
        Entrust::can('product.list') ?  : abort(403);

        return User::find($request->id);
    }

    public function delete(Request $request) {
        Entrust::can('order.edit') ?  : abort(403);

        Order::find($request->id)->delete();
        OrderItem::where('order_id', $request->id)->delete();

        return ['status' => 'success'];
    }
}
