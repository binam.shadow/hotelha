<?php

namespace Modules\Setting\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Slider extends Model {
	use LogsActivity;


    public $table = 'sliders';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'title',
        'description',
        'image',
        'active',
        'type',
        'url'
    ];

	protected static $logFillable = true;

	public function getDescriptionForEvent(string $eventName): string {
		switch ($eventName) {
			case 'created':
				return "ثبت اسلایدر جدید";
				break;

			case 'updated':
				return "ویرایش اسلایدر";
				break;

			case 'deleted':
				return "حذف اسلایدر";
				break;

			default:
				return '';
				break;
		}
    }
}
