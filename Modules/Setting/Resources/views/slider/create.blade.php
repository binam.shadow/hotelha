@extends('adminbsb.layouts.master')

@section('title')
@lang('setting::default.slider_create')
@endsection

@section('styles')
<!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('setting::default.slider_create')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="sliderEdit" method="post" action="{{ route('slider.create') }}">
					{{ csrf_field() }}
					<div class="col-md-12 text-center">
						@include('partials._single_fileinput', [
							'field_name'	=> 'image'
						])
					</div>
					<div class="row clearfix"></div>
					<div class="row clearfix">
						<div class="col-md-12">
							<label class="form-label">@lang('setting::default.type')</label>
							<select class="col-md-8" name="type">
								<option value="app">@lang('setting::default.app')</option>
								<option value="web">@lang('setting::default.web')</option>
							</select>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="title" value="{{ old('title') }}" required="" aria-required="true" type="text">
									<label class="form-label">@lang('setting::default.title') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<textarea class="form-control ckeditor" name="description" required="" aria-required="true" type="text">{{ old('description') }}</textarea>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="url" value="{{ old('url') }}" aria-required="true" type="text">
									<label class="form-label">@lang('setting::default.url')</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="switch">
							<label>@lang('setting::default.active') <input name="active" checked="" type="checkbox"><span class="lever switch-col-blue"></span> @lang('setting::default.deactive')</label>
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('setting::default.slider_create')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/config.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/lang/fa.js') }}"></script>
<script type="text/javascript">
function responsive_filemanager_callback(field_id){
	$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

}
function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
function deleteImage(field_id) {
	$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
	$('#'+field_id).val('');
}
$('#sliderEdit').validate({
	rules: {
		'name': {
			required: true
		},
		'category_id': {
			required: true
		}
	}
});
</script>
@endsection
