@extends('adminbsb.layouts.master')

@section('title')
@lang('admin::default.android_version_edit')
@endsection


@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('admin::default.android_version_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<div class="row">
					<form class="col-sm-12" method="post" action="{{ route('android_version.edit') }}" >
						{{ csrf_field() }}
						<div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="version" required="" aria-required="true" value="{{ $data->where('key', 'android_version')->first()->value }}">
	                            <label class="form-label">ورژن <span class="col-red">*</span></label>
	                        </div>
	                    </div>
	                    <div class="form-group form-float">
	                   		<p class="text-right">اپلیکیشن فعال است؟</p>
	                        <div class="switch">
	                            <label>@lang('setting::default.active') <input name="active" type="checkbox" @if($data->where('key', 'android_active')->first()->value) checked @endif><span class="lever switch-col-blue"></span> @lang('setting::default.deactive')</label>
	                        </div>
							<br>
							<p class="text-right">این ورژن اجباری است؟</p>
	                        <div class="switch">
	                            <label>@lang('setting::default.yes') <input name="required" type="checkbox" @if($data->where('key', 'android_required')->first()->value) checked @endif><span class="lever switch-col-blue"></span> @lang('setting::default.no')</label>
	                        </div>
	                    </div>
	                    <div class="form-group form-float">
	                        <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('admin::default.edit')</button>
	                    </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
