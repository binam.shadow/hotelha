@extends('adminbsb.layouts.master')

@section('title')
    @lang('setting::default.contact_edit')
@endsection

@section('content')
        <!-- Widgets -->
<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-6">
                        <h2>@lang('setting::default.contact_edit')</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form id="contactEdit" method="post" action="{{ route('contact.edit') }}">
                    {{ csrf_field() }}
                    <div class="row clearfix"></div>
                    <div class="col-md-12 text-center">
                        <p class="text-center">@lang('blog::default.feature_image')</p>
                        <div class="col-sm-3 col-center text-center">
                            <img style="width: 100%" id="image-show" @if($contact->image == '') src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" @else src="{{ asset('uploads/' . $contact->image) }}" @endif>
                        </div>
                        <input id="image" type="hidden" name="image" @if($contact->image != '') value="{{$contact->image}}" @endif>
                        <a href="javascript:void(0)" onclick="deleteImage('image')">حذف</a> <a class="btn btn-info" type="button" href="javascript:open_popup('{{url('filemanager/dialog.php?fldr=images/about&relative_url=1&type=1&amp;popup=1&amp;field_id=image')}}')" >@lang('blog::default.choose_file')</a>
                    </div>
                    <div class="row clearfix"></div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input class="form-control" name="name" value="{{ $contact->name }}" required="" aria-required="true" type="text">
                            <label class="form-label">@lang('setting::default.name') <span class="col-red">*</span></label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control ckeditor" name="description" required="" aria-required="true" type="text">{{ $contact->description }}</textarea>
                        </div>
                    </div>
                    <label class="form-label">اطلاعات تماس</label>
                    <div class="row">
                        <div class="col-sm-4">
                            @include('partials._xcloner', [
                                'fields'=> json_decode($contact->contacts)
                            ])
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    @include('partials._googlemap', [
                        'latitude' => $contact->latitude,
                        'longitude' => $contact->longitude,
                    ])
                    <div class="clearfix"></div>
                    <div class="form-group form-float">
                        <div class="switch">
                            <label>@lang('setting::default.active') <input name="active" @if($contact->active) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('setting::default.deactive')</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('setting::default.contact_edit')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/config.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/lang/fa.js') }}"></script>

    <script>
    function responsive_filemanager_callback(field_id){
        $('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

    }
    function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
    function deleteImage(field_id) {
        $('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
        $('#'+field_id).val('');
    }
    $('#contactEdit').validate({
        rules: {
            'name': {
                required: true
            },
            'description': {
                required: true
            }
        }
    });
    </script>
@endsection
