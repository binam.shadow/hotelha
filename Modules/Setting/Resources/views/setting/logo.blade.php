@extends('adminbsb.layouts.master')

@section('title')
@lang('admin::default.edit_logo')
@endsection


@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('admin::default.edit_logo')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<div class="row">
					<form class="col-sm-6" id="adminCreate" method="post" action="{{ route('logo.edit') }}">
						{{ csrf_field() }}
						<style>
							.col-center{
								float: none;
								margin: 0 auto;
							}
						</style>
						<div class="row clearfix"></div>
						<div class="col-md-12 text-center">
							<p class="text-center">@lang('blog::default.feature_image')</p>
							<div class="col-sm-3 col-center text-center">
								<img style="width: 100%" id="image-show" @if($logo->value == '') src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" @else src="{{ asset('uploads/' . $logo->value) }}" @endif>
							</div>
							<input id="image" type="hidden" name="image" @if($logo->value != '') value="{{$logo->value}}" @endif>
							<a href="javascript:void(0)" onclick="deleteImage('image')">حذف</a> <a class="btn btn-info" type="button" href="javascript:open_popup('{{url('filemanager/dialog.php?fldr=images/logo&relative_url=1&type=1&amp;popup=1&amp;field_id=image')}}')" >@lang('blog::default.choose_file')</a>
						</div>
                        <div class="form-group form-float">
	                        <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('admin::default.edit')</button>
	                    </div>
					</form>
					<div class="col-sm-6">
						<p class="text-right">@lang('setting::default.current_logo')</p>
						<p class="text-center">
							<img src="{{ asset('uploads/' . $logo->value) }}" class="img-responsive">
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
	function responsive_filemanager_callback(field_id){
		$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

	}
	function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
	function deleteImage(field_id) {
		$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
		$('#'+field_id).val('');
	}
</script>
@endsection