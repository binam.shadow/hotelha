@extends('adminbsb.layouts.master')

@section('title')
@lang('setting::default.city_create')
@endsection

@section('styles')
<!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
.file-footer-caption, .file-thumbnail-footer{
	display: none;
}
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('setting::default.city_create')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="adminCreate" method="post" action="{{ route('city.create') }}">
					{{ csrf_field() }}
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
							<label class="form-label">@lang('setting::default.name') <span class="col-red">*</span></label>
						</div>
					</div>
					<div class="form-group form-float">
						<label class="form-label">@lang('setting::default.province') <span class="col-red">*</span></label>
						<select name="province">
							@foreach($provinces as $province)
								<option value="{{ $province->id }}">{{ $province->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('setting::default.city_create')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/select2/select2.min.js') }}"></script>
<script>
$('#adminCreate').validate({
	rules: {
		'name': {
			required: true
		}
	}
});
</script>
@endsection
