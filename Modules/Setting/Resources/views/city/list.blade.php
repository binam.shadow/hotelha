@extends('adminbsb.layouts.master')

@section('title')
@lang('setting::default.city_list')
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('setting::default.city_list')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover dataTable js-exportable user-list">
						<thead>
							<tr>
								<th>@lang('setting::default.id')</th>
								<th>@lang('setting::default.city_name')</th>
								<th>@lang('setting::default.province')</th>
								<th>@lang('setting::default.setting')</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
@include('adminbsb.layouts.datatable_files')
<script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript">
var dataTable = $('.user-list').DataTable({
	"order": [[ 0, "desc" ]],
	ajax: {
		url: '{{ route('city.dataList') }}'
	},
	responsive: true,
	serverSide: true,
	columns: [
		{data: 'id', name:'id'},
		{data: 'name'}
	],
	columnDefs: [
		{
			targets: 2,
			render: function(data, type, row) {
				return row.province.name;
			}
		},
		{
			targets: 3,
			render: function(data, type, row) {
				var btns = '';
				@permission('city.edit')
				btns += '<a href="{{ route('city.edit') }}/'+row.id+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
				@endpermission
				btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
				return btns;
			}
		}
	],
	buttons: [
		'copyHtml5', 'excelHtml5', 'print'
	],
	language:{
		url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
	}
});

function showConfirmDelete(id){
	swal({
		title: "@lang('setting::default.are_you_sure')",
		text: "@lang('setting::default.warning_delete_msg')",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		cancelButtonText: "@lang('setting::default.cancel')",
		confirmButtonText: "@lang('setting::default.confirm')",
		closeOnConfirm: false
	}, function (action) {
		if(action)
		$.ajax({
			url: "{{ route('city.delete') }}",
			data: "id="+id,
			processData: false,
			contentType: false,
			type: 'GET',
			success: function (result) {
				if(result.status == 'success') {
					swal("@lang('setting::default.success_delete')", "@lang('setting::default.success_delete_msg')", "success");
					dataTable.ajax.reload()
				}
			},
			error: function (){

			}
		});
	});
}
</script>
@endsection
