<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Entrust;
use Validator;
use App\Province;

class ProvinceController extends Controller {

    public function create() {
        Entrust::can('province.create') ? : abort(403);

        return view('setting::province.create');
    }

    public function store(Request $request) {
        Entrust::can('province.create') ? : abort(403);

        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:256',
        ]);
        if($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $exist = Province::where('name', $request->name)->first();
        if(count($exist))
            return back()
                ->with('msg', \Lang::get('setting::default.must_unique'))
                ->with('msg_color', 'bg-red');

        Province::create($request->only(['name']));

        // Redirect with success message
        return redirect()->route('province.create')
                    ->with('msg', \Lang::get('setting::default.success_create'))
                    ->with('msg_color', 'bg-green');
    }

    public function list()  {
        Entrust::can('province.list') ? : abort(403);

        return view('setting::province.list');
    }

    public function dataList(Request $request) {
        Entrust::can('province.list') ? : abort(403);
        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $data['draw'] =  $request->draw;
        $data['data'] = Province::where('name', 'like', '%'.$request->search['value'].'%')
            ->limit($request->length)
            ->offset($request->start)
            ->orderBy($request->columns[$order_column]['data'], $order_dir)
            ->get();

        $count = Province::count();
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();
        return $data;
    }

    public function edit($id = 0) {
        Entrust::can('province.edit') ? : abort(403);

        // Check Province exist
        $province = Province::where('id', $id)->first();
        if(!count($province)) abort(404);

        return view('setting::province.edit', compact('province'));
    }

    public function update($id = 0, Request $request) {
        Entrust::can('province.edit') ? : abort(403);

        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:256',
        ]);
        if($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        // Check Province exist
        $province = Province::where('id', $id)->first();
        if(!count($province)) abort(404);

        $province->update([ 'name' => $request->name ]);

        // Redirect with success message
        return redirect()->route('province.list')
                    ->with('msg', \Lang::get('setting::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    public function delete(Request $request) {
        Entrust::can('province.edit') ? : abort(403);

        Province::find($request->id)->forceDelete();
        return ['status' => 'success'];
    }


}
