<?php

namespace Modules\Setting\Http\Controllers;

use App\Classes\FileManager;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Setting;
use Entrust;
use File;
use Validator;

class SettingController extends Controller {

    public function index(){
        return view('admin::index');
    }

    public function aboutGalleryDelete(Request $request) {
        $id = $request->key;
        $data = json_decode(Setting::where('key', 'about_gallery')->first()->value);
        foreach($data as $i => $element) {
            if($id == $element->id) {
                FileManager::delete($element->image);
                unset($data[$i]);
            }
        }
        $result = array();
        $i = 0;
        foreach($data as $element) {
            $element->id = $i++;
            array_push($result, $element);
        }
        $data = json_encode($result);
        Setting::where('key', 'about_gallery')->update(['value' => $data]);
        return ['status' => 'success'];
    }

    public function editOpeningHours() {
        Entrust::can('contact.opening_hours') ?  : abort(403);

        $opening_hours = json_decode(Setting::where('key', 'opening_hours')->first()->value);

        return view('setting::setting.openinghours', compact('opening_hours'));
    }

    public function updateOpeningHours(Request $request) {
        Entrust::can('contact.opening_hours') ?  : abort(403);

        $opening_hours = $request->openinghours;

        Setting::where('key', 'opening_hours')->update(['value' => $opening_hours]);
        return redirect()->route('contact.opening_hours')
            ->with('msg', \Lang::get('setting::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function editAbout() {
        Entrust::can('about.edit') ?  : abort(403);

        $about = json_decode(Setting::where('key', 'about')->first()->value);
        $galleries = json_decode(Setting::where('key', 'about_gallery')->first()->value);

        return view('setting::setting.aboutus', compact('about', 'galleries'));
    }

    public function updateAbout(Request $request) {
        Entrust::can('about.edit') ?  : abort(403);

        $about = json_decode(Setting::where('key', 'about')->first()->value);
        if(isset($request->images)) {
            $gallery = array();
            foreach ($request->images as $i => $image) {
                array_push($gallery, ['id' => $i, 'image' => $image]);
            }
            $gallery = json_encode($gallery);
            Setting::where('key', 'about_gallery')->update(['value' => $gallery]);
        }
        else{
            Setting::where('key', 'about_gallery')->update(['value' => '{}']);
        }

        $about_data = $request->except(['_token', 'images']);

        $about_data['image'] = $request->image;
        Setting::where('key', 'about')->update(['value' => json_encode($about_data)]);
        return redirect()->route('about.edit')
            ->with('msg', \Lang::get('setting::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function editContact() {
        Entrust::can('contact.edit') ?  : abort(403);

        $contact = json_decode(Setting::where('key', 'contact')->first()->value);

        return view('setting::setting.contactus', compact('contact'));
    }

    public function updateContact(Request $request) {
        Entrust::can('contact.edit') ?  : abort(403);

        $contact_data = $request->except(['_token']);
        
        $contacts = [];
        foreach($request->contacts as $contact => $type){
            foreach ($type as $key => $value){
                if(!empty($value)){
                    $contacts[] = [
                        'type' => $contact,
                        'value' => $value,
                    ];
                }
            }
        }
        
        $contact_data['contacts'] = json_encode($contacts);

        $contact_data['image'] = $request->image;
        Setting::where('key', 'contact')->update(['value' => json_encode($contact_data)]);
        return redirect()->route('contact.edit')
            ->with('msg', \Lang::get('setting::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function editLogo() {
        Entrust::can('logo.edit') ? : abort(403);

        $logo = Setting::where('key', 'logo')->first();
        return view('setting::setting.logo', compact('logo'));
    }

    public function updateLogo(Request $request) {
        Entrust::can('logo.edit') ? : abort(403);

        // Update logo url in database
        Setting::where('key', 'logo')->update(['value' => $request->image]);

        return back()
                    ->with('msg', 'لوگو با موفقیت آپلود شد')
                    ->with('msg_color', 'bg-green');
    }

    public function getTitleDescriptionEdit() {
        Entrust::can('title_description.edit') ? : abort(403);

        // Get title & description
        $data = Setting::whereIn('key', ['title', 'description'])
                        ->orderBy('key', 'DESC')
                        ->get()
                        ->pluck('value');

        $home_text = Setting::where('key', 'home_text')->first();
        $adver_text = Setting::where('key', 'adver_text')->first();
        // offset 0 -> title
        // offset 1 -> description
        return view('setting::setting.title_description', compact('data', 'home_text', 'adver_text'));
    }

    public function postTitleDescriptionEdit(Request $request) {
        Entrust::can('title_description.edit') ? : abort(403);

        $validator = Validator::make($request->all(), [
            'title'         => 'required|max:256',
            'description'   => 'required|max:256'
        ]);
        if($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        Setting::where('key', 'title')->update(['value' => $request->title]);
        Setting::where('key', 'description')->update(['value' => $request->description]);

        Setting::updateOrCreate(['key' => 'home_text'], ['key' => 'home_text', 'value' => $request->home_text]);
        Setting::updateOrCreate(['key' => 'adver_text'], ['key' => 'adver_text', 'value' => $request->adver_text]);

        return back()
                    ->with('msg', \Lang::get('admin::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    public function editAndroidVersion() {
        Entrust::can('android_version.edit') ? : abort(403);

        $data = Setting::whereIn('key', ['android_version', 'android_active', 'android_required'])
                        ->orderBy('key', 'asc')
                        ->get();
        return view('setting::setting.android_version', compact('data'));
    }

    public function updateAndroidVersion(request $request) {
        Entrust::can('android_version.edit') ? : abort(403);

        // Update adnroid version
        Setting::where('key', 'android_version')->update(['value' => $request->version]);

        // Update android version active or deactive
        $active = 0;
        if($request->active)
            $active = 1;
        Setting::where('key', 'android_active')->update(['value' => $active]);

        // Update android version required
        $required = 0;
        if($request->required)
            $required = 1;
        Setting::where('key', 'android_required')->update(['value' => $required]);

        // Redirect with success message
        return back()
                    ->with('msg', \Lang::get('admin::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    public function editIosVersion() {
        Entrust::can('ios_version.edit') ? : abort(403);

        $data = Setting::whereIn('key', ['ios_version', 'ios_active', 'ios_required'])
                        ->orderBy('key', 'desc')
                        ->get();
        return view('setting::setting.ios_version', compact('data'));
    }

    public function updateIosVersion(Request $request) {
        Entrust::can('ios_version.edit') ? : abort(403);

        // Update ios version
        Setting::where('key', 'ios_version')->update(['value' => $request->version]);

        // Update ios version active or deactive
        $active = 0;
        if($request->active)
            $active = 1;
        Setting::where('key', 'ios_active')->update(['value' => $active]);

        // Update ios version required
        $required = 0;
        if($request->required)
            $required = 1;
        Setting::where('key', 'ios_required')->update(['value' => $required]);

        // Redirect with success message
        return back()
                    ->with('msg', \Lang::get('admin::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }
}
