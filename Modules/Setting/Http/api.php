<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Setting\Http\Controllers'
], function() {

    Route::get('about', [
        'uses'	=> 'ApiSettingController@about'
    ]);
    Route::get('contact', [
        'uses'	=> 'ApiSettingController@contact'
    ]);
    Route::get('getAllCities', [
        'uses'	=> 'ApiSettingController@getAllCities'
    ]);
    Route::get('getCities', [
        'uses'	=> 'ApiSettingController@getCities'
    ]);
    Route::get('getSliders', [
        'uses'	=> 'ApiSettingController@getSliders'
    ]);
    Route::get('getProvinces', [
        'uses'	=> 'ApiSettingController@getProvinces'
    ]);
    Route::get('openingHours', [
        'uses'	=> 'ApiSettingController@openingHours'
    ]);
});
Route::get('api/android', [
    'uses'	=> 'Modules\Setting\Http\Controllers\ApiSettingController@android'
]);
Route::get('api/ios', [
    'uses'	=> 'Modules\Setting\Http\Controllers\ApiSettingController@ios'
]);
