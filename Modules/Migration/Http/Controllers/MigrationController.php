<?php
namespace Modules\Migration\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
// use Modules\Migration\Entities\Migration;


class MigrationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('migration::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $migration = new Migration();
        return view('migration::create', compact('migration'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('migration.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Migration::create($request->all());
        return back()
            ->with('msg', \Lang::get('migration::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Migration $migration)
    {
        return view('migration::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Migration $migration)
    {
        return view('migration::edit', compact('migration'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Migration $migration, Request $request)
    {
        Entrust::can('migration.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $migration->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Migration $migration)
    {
        try {
            $migration->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('migration.list')
                ->with('msg', \Lang::get('migration::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function migrationList(){
          return DataTables::of(Migration::query())
                      ->make(true);
     }
}
