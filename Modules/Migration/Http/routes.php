<?php

Route::group(['middleware' => 'web', 'prefix' => 'migrations', 'namespace' => 'Modules\Migration\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'migration.list',
        'uses'	=> 'MigrationController@index'
    ]);
    Route::get('create', [
        'as'	=> 'migration.create',
        'uses'	=> 'MigrationController@create'
    ]);
    Route::post('store', [
        'as'	=> 'migration.store',
        'uses'	=> 'MigrationController@store'
    ]);
    Route::get('{migration}/edit', [
        'as'	=> 'migration.edit',
        'uses'	=> 'MigrationController@edit'
    ]);
    Route::patch('{migration}', [
        'as'	=> 'migration.update',
        'uses'	=> 'MigrationController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'migration.dataList',
        'uses'	=> 'MigrationController@migrationList'
    ]);
    Route::delete('{migration}', [
        'as'	=> 'migration.delete',
        'uses'	=> 'MigrationController@destroy'
    ]);
});