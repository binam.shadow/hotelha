@extends('adminbsb.layouts.master')

@section('title')
    @lang('migration::default.create')
@endsection

@section('styles')
    @include('migration::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('migration::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('migration::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('migration.store') }}">
                        {{ csrf_field() }}
                        @include('migration::_form', ['saveButtonLabel'=> 'create'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('migration::partials._script')
@endsection
