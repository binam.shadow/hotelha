@extends('adminbsb.layouts.master')

@section('title')
    @lang('migration::default.list_title')
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>@lang('migration::default.list_title')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable migration-list" width="100%">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>@lang('migration::default.migrationname')</th>
                                <th>@lang('migration::default.priority')</th>
                                <th>@lang('migration::default.icon')</th>
                                <th>@lang('migration::default.setting')</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('adminbsb.layouts.datatable_files')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var dataTable = $('.migration-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('migration.dataList') }}'
            },
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {data: 'name'},
                {data: 'priority'},
                {
                    data: 'icon',
                    "bSortable": false,
                    render: function (data, type, row) {
                        if(data != null && data != '')
                            return '<a href="{{ url('uploads') }}/'+ data +'" target="_blank"><img src="{{ url('uploads') }}/'+ data +'" width="60px"></a>';
                        else
                            return '<img src="{{ asset('/uploads/images/nopic.jpg') }}" style="width: 60px;">';
                    }
                },
            ],
            columnDefs: [
                {
                    targets: 4,
                    render: function(data, type, row) {
                        var btns = '';
					@permission('migration.edit')
                        btns += '<a href="migration/'+row.id+'/edit'+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
                        btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
					@endpermission
                        return btns;
                    }
                }
            ],
            buttons: [
                'copyHtml5', 'excelHtml5', 'print'
            ],
            language:{
                url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        function showConfirmDelete(id){
            swal({
                title: "@lang('migration::default.are_you_sure')",
                text: "@lang('migration::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('migration::default.cancel')",
                confirmButtonText: "@lang('migration::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "migrations/"+id,
                        //data: "_method=delete",
                        processData: false,
                        contentType: false,
                        type: 'DELETE',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('migration::default.success_delete')", "@lang('migration::default.success_delete_msg')", "success");
                                dataTable.ajax.reload()
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
    </script>
@endsection
