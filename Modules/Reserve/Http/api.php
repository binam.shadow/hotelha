<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Reserve\Http\Controllers'
], function() {

    Route::get('', [
        'uses'	=> 'ApiReserveController@MethodName'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/