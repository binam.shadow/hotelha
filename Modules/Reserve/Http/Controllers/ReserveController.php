<?php
namespace Modules\Reserve\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Reserve\Entities\Reserve;
use GuzzleHttp\Client;
use Modules\Hotel\Entities\Hotel;


class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('reserve::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $reserve = new Reserve();

        $hotels = Hotel::all();




   return view('reserve::create', compact('reserve' ,'hotels' ));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('reserve.create') ?: abort(403);
        $this->validate($request,[
      //     'name' => 'required',

        ]);
return $request->all();


        Reserve::create($request->all());
        return back()
            ->with('msg', \Lang::get('reserve::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Reserve $reserve)
    {
        return view('reserve::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Reserve $reserve)
    {
        return view('reserve::edit', compact('reserve'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Reserve $reserve, Request $request)
    {
        Entrust::can('reserve.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $reserve->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Reserve $reserve)
    {
        try {
            $reserve->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('reserve.list')
                ->with('msg', \Lang::get('reserve::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function reserveList(){
          return DataTables::of(Reserve::query())
                      ->make(true);
     }
}
