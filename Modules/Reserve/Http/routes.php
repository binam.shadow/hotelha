<?php

Route::group(['middleware' => 'web', 'prefix' => 'reserves', 'namespace' => 'Modules\Reserve\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'reserve.list',
        'uses'	=> 'ReserveController@index'
    ]);
    Route::get('create', [
        'as'	=> 'reserve.create',
        'uses'	=> 'ReserveController@create'
    ]);
    Route::post('store', [
        'as'	=> 'reserve.store',
        'uses'	=> 'ReserveController@store'
    ]);
    Route::get('{reserve}/edit', [
        'as'	=> 'reserve.edit',
        'uses'	=> 'ReserveController@edit'
    ]);
    Route::patch('{reserve}', [
        'as'	=> 'reserve.update',
        'uses'	=> 'ReserveController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'reserve.dataList',
        'uses'	=> 'ReserveController@reserveList'
    ]);
    Route::delete('{reserve}', [
        'as'	=> 'reserve.delete',
        'uses'	=> 'ReserveController@destroy'
    ]);
});