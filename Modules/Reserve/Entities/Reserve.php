<?php

namespace Modules\Reserve\Entities;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    protected $fillable = ['hotelCode',
        'roomTypeCodes',
        'numberOfRooms',
        'startDate',
        'nightCount',
'requestNumber',
'requestPNR'];
}
