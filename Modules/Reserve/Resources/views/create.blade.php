@extends('adminbsb.layouts.master')

@section('title')
    @lang('reserve::default.create')
@endsection

@section('styles')
    @include('reserve::partials._style')


    <!-- Bootstrap Select Css -->
    <link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <style>
        .file-footer-caption, .file-thumbnail-footer{
            display: none;
        }
    </style>
<link rel="stylesheet" href="https://unpkg.com/persian-datepicker@latest/dist/css/persian-datepicker.css"/>
@endsection

@section('content')




    <input type="text" class="normal-example" />









    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('reserve::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('reserve::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('reserve.store') }}">
                        {{ csrf_field() }}


                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label class="form-label">هتل ها</label>
                                <select class="col-md-8" name="hotelCode">
                                    <option value="0" selected>انتخاب کنید</option>
                                    @foreach($hotels as $hotel)
                                        <option value="{{ $hotel->Code }}">{{ $hotel->Name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{--<input type="text" class="normal-example" />--}}




                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control normal-example" name="startDate" value="{{ old('startDate') }}" required="" aria-required="true" type="text">
                                <label class="form-label">تاریخ شروع <span class="col-red">*</span></label>
                            </div>
                        </div>



                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control normal-example" name="endDate" value="{{ old('endDate') }}" required="" aria-required="true" type="text">
                                <label class="form-label">تاریخ اتمام <span class="col-red">*</span></label>
                            </div>
                        </div>



                        {{--'hotelCode',--}}
                        {{--'roomTypeCodes',--}}
                        {{--'numberOfRooms',--}}
                        {{--'startDate',--}}
                        {{--'nightCount',--}}
                        {{--'requestNumber',--}}
                        {{--'requestPNR'];--}}



                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">ایجاد</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('reserve::partials._script')

    <script src="https://unpkg.com/persian-date@latest/dist/persian-date.js"></script>
    <script src="https://unpkg.com/persian-datepicker@latest/dist/js/persian-datepicker.js"></script>



    <script type="text/javascript">
        $('.normal-example').persianDatepicker({
            initialValueType: 'gregorian'
        });


    </script>
@endsection
