@extends('adminbsb.layouts.master')

@section('title')
    @lang('reserve::default.create')
@endsection

@section('styles')
    @include('reserve::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('reserve::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('reserve::partials._errors')
                    <form id="reserveEdit" method="post" action="{{ route('reserve.update', ['reserve' => $reserve])) }}">
                        {{ csrf_field() }}
                        @include('reserve::_form', ['saveButtonLabel'=> 'edit'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('reserve::partials._script')
@endsection
