<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reserves', function (Blueprint $table) {

            $table->increments('id');
            $table->string('hotelCode');
            $table->string('roomTypeCodes', 256);
            $table->string('numberOfRooms', 256)->nullable();
            $table->string('startDate', 256)->nullable();
            $table->string('nightCount')->nullable();
            $table->string('requestNumber', 256)->nullable();
            $table->string('requestPNR')->nullable();

       //     Hotel/Reserve/Room/{hotelCode}/{roomTypeCodes}/{numberOfRooms}/{startDate}/{nightCount}/{userGuid}
       //     /{requestNumber}/{requestPNR

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
