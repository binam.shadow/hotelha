@extends('adminbsb.layouts.master')

@section('title')
@lang('blog::default.blog_edit')
@endsection
@section('styles')
<style>
	.file-preview{
		display: block !important;
	}
</style>
<link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}">
<!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="{{ asset('backend/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('blog::default.blog_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="blogEdit" method="post" action="{{ route('blog.edit', ['id' => $blog->id]) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					@if(count($categories) > 1)
						<div class="row clearfix">
							<div class="col-md-12">
								<label class="form-label">@lang('blog::default.category') <span class="col-red">*</span></label>
								<select class="col-md-8" name="category_id">
									@foreach($categories as $category)
										<option value="{{ $category->id }}" @if($blog->category_id == $category->id) {{'selected'}} @endif>{{ $category->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					@else
						<input type="hidden" name="category_id" value="@php if(count($categories)) echo $categories[0]->id; @endphp" />
					@endif
					
					<div class="col-md-12 text-center">
						<p class="text-center">@lang('blog::default.feature_image')</p>
						@include('partials._single_fileinput', [
							'field_name' => 'image',
							'old_image'	 => $blog->main_image,
							'delete_url' => route('blog.imageDelete')
						])
					</div>
					<div class="row clearfix">
						<div class="col-md-12">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="name" value="{{ $blog->name }}" required="" aria-required="true" type="text">
									<label class="form-label">@lang('blog::default.name') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<textarea class="form-control ckeditor" name="description" required="" aria-required="true" type="text">{{ $blog->description }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="form-line">
									<div class="bootstrap-tagsinput"></div><input placeholder="@lang('blog::default.tag')" name="tags" id="tagsinput" type="text" class="form-control" data-role="tagsinput" value="{{ $blog->tags }}" style="display: none;">
								</div>
							</div>
							<div class="row clearfix"></div>
							<div class="row">
						        <div class="col-sm-12">
						            <label> گالری عکس </label>
						            <input type="file" id="images" name="images[]" multiple>
						        </div>
								@include('partials._multiple_fileinput', [
									'field_name'=> 'images',
									'gallery'	=> $blog->gallery
								])
							</div>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="switch">
							<label>@lang('blog::default.active') <input name="active" @if($blog->active) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('blog::default.deactive')</label>
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('blog::default.blog_edit')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/locales/fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/config.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/lang/fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script type="text/javascript">
	function responsive_filemanager_callback(field_id){
		if(field_id != 'temp_image') {
			// show image in src
			$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());
		} else {
			// add new box with random id
			var rand_x = Math.floor((Math.random() * 99999) + 1);
			var new_box = '<div id="imageG'+rand_x+'" class="col-xs-6 col-md-3">'+
					'<input name="images[]" type="hidden" value="'+$('#'+field_id).val()+'">'+
					'<a class="thumbnail"><img src="{{ url('uploads') }}/' + $('#'+field_id).val()+'" class="img-responsive"></a><p class="text-center"><a class="btn btn-danger btn-sm waves-effect" onclick="deleteGallery('+rand_x+')"><i class="material-icons">delete_forever</i> حذف</a></p></div>';
			$('#galleryBlock').append(new_box);
		}
	}
	function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
	function deleteImage(field_id) {
		$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
		$('#'+field_id).val('');
	}
	function deleteGallery(box_id) {
		$("#imageG"+box_id).remove();
	}
</script>

<script type="text/javascript">
$('#tagsinput').tagsinput({
	maxTags: 10
});
$("#gallery").fileinput({
	showUpload: false,
	overwriteInitial: false,
	@if(count($galleries))
	initialPreview: [
		@foreach($galleries as $gallery)
				"{{ asset('uploads/' . $gallery->image) }}",
		@endforeach
	],
	@endif
	initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
	language: 'fa',
	initialPreviewFileType: 'image',
	maxFileCount: 5,
	allowedFileExtensions: ["jpg", "png", "gif"]
});
$('#blogEdit').validate({
	rules: {
		'name': {
			required: true
		},
		'category_id': {
			required: true
		}
	}
});
</script>
@endsection
