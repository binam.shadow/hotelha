@extends('adminbsb.layouts.master')

@section('title')
@lang('blog::default.category_list')
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('blog::default.category_list')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover dataTable js-exportable category-list" width="100%">
						<thead>
							<tr>
								<th>id</th>
								<th>@lang('blog::default.icon')</th>
								<th>@lang('blog::default.image')</th>
								<th>@lang('blog::default.name')</th>
								<th>@lang('blog::default.description')</th>
								<th>@lang('blog::default.setting')</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>id</th>
								<th>@lang('blog::default.icon')</th>
								<th>@lang('blog::default.image')</th>
								<th>@lang('blog::default.name')</th>
								<th>@lang('blog::default.description')</th>
								<th>@lang('blog::default.setting')</th>
							</tr>
						</tfoot>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
@include('adminbsb.layouts.datatable_files')
<script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript">
	var dataTable = $('.category-list').DataTable({
		ajax: {
			url: '{{ route('blogcategory.dataList') }}'
		},
		responsive: true,
		serverSide: true,
		columns: [
			{data: 'id', name:'id'},
			{
				data: 'icon',
				"bSortable": false,
				render: function (data, type, row) {
					if(data != null && data != '')
						return '<a href="{{ route('show_img') }}/'+ data.file_name +'" target="_blank"><img src="{{ route('show_img') }}/'+ data.file_name +'" width="60px"></a>';
					else
						return '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default.png') }}" style="width: 60px;">';
				}
			},
			{
				data: 'image',
				"bSortable": false,
				render: function (data, type, row) {
					if(data != null && data != '')
						return '<a href="{{ route('show_img') }}/'+ data.file_name +'" target="_blank"><img src="{{ route('show_img') }}/'+ data.file_name +'" width="60px"></a>';
					else
						return '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default.png') }}" style="width: 60px;">';
				}
			},
			{data: 'name'},
			{data: 'description'}
		],
		columnDefs: [
			{
				targets: 5,
				render: function(data, type, row) {
					var btns = '';
					@permission('blogcategory.edit')
					btns += '<a href="{{ route('blogcategory.edit') }}/'+row.id+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
					btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
					@endpermission
					return btns;
				}
			}
		],
		buttons: [
			'copyHtml5', 'excelHtml5', 'print'
		],
		language:{
			url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
		}
	});

	function showConfirmDelete(id){
		swal({
			title: "@lang('blog::default.are_you_sure')",
			text: "@lang('blog::default.warning_delete_msg')",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			cancelButtonText: "@lang('blog::default.cancel')",
			confirmButtonText: "@lang('blog::default.confirm')",
			closeOnConfirm: false
		}, function (action) {
			if(action)
			$.ajax({
				url: "{{ route('blogcategory.delete') }}",
				data: "id="+id,
				processData: false,
				contentType: false,
				type: 'GET',
				success: function (result) {
					if(result.status == 'success') {
						swal("@lang('blog::default.success_delete')", "@lang('blog::default.success_delete_msg')", "success");
						dataTable.ajax.reload()
					}
				},
				error: function (){

				}
			});
		});
	}
</script>
@endsection
