@extends('adminbsb.layouts.master')

@section('title')
@lang('blog::default.category_edit')
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('blog::default.category_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="categoryEdit" method="post" action="{{ route('blogcategory.edit', ['id' => $blogcategory->id]) }}">
					{{ csrf_field() }}
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="name" value="{{ $blogcategory->name }}" required="" aria-required="true" type="text">
							<label class="form-label">@lang('blog::default.name') <span class="col-red">*</span></label>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="description" value="{{ $blogcategory->description }}" aria-required="true" type="text">
							<label class="form-label">@lang('blog::default.description')</label>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب آیکن</p>
							@include('partials._single_fileinput', [
								'field_name' => 'icon',
								'old_image' => $blogcategory->icon
							])
						</div>
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب تصویر</p>
							@include('partials._single_fileinput', [
								'field_name' => 'image',
								'old_image' => $blogcategory->image
							])
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('blog::default.category_edit')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('backend/plugins/bootstrap-fileinput/js/locales/fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script type="text/javascript">
function responsive_filemanager_callback(field_id){
    $('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

}
function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
function deleteImage(field_id) {
    $('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
    $('#'+field_id).val('');
}
$('select').select2({
	dir: 'rtl'
});

$('select[name=parent_id]').val('{{ $blogcategory->parent_id }}').trigger('change');
var delete_image_url = "{{ route('blogcategory.imageDelete') }}?_token={{ csrf_token() }}";
var delete_icon_url = "{{ route('blogcategory.iconDelete') }}?_token={{ csrf_token() }}";
$(".file-icon").fileinput({
	@if(!is_null($blogcategory->icon))
	initialPreview: [
		"{{ asset('uploads/' . $blogcategory->icon) }}"
	],
	initialPreviewConfig: [
		{
			url: delete_icon_url,
			key: '{{ $blogcategory->id }}'
		}
	],
	initialPreviewFileType: 'image',
	initialPreviewAsData: true,
	@endif
	language: 'fa',
	browseOnZoneClick: true,
	overwriteInitial: true,
	maxFileSize: 1500,
	showClose: false,
	showCaption: false,
	browseLabel: '',
	removeLabel: '',
	browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
	removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
	removeTitle: 'Cancel or reset changes',
	elErrorContainer: '#kv-avatar-errors-1',
	msgErrorClass: 'alert alert-block alert-danger',
	defaultPreviewContent: '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" style="width: 140px;margin-bottom: 10px" alt="Your Avatar">',
	layoutTemplates: {main2: '{preview} {remove}'},
	allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif']
});

$(".file-image").fileinput({
	@if(!is_null($blogcategory->image))
	initialPreview: [
		"{{ asset('uploads/' . $blogcategory->image) }}"
	],
	initialPreviewConfig: [
		{
			url: delete_image_url,
			key: '{{ $blogcategory->id }}'
		}
	],
	initialPreviewFileType: 'image',
	initialPreviewAsData: true,
	@endif
	language: 'fa',
	browseOnZoneClick: true,
	overwriteInitial: true,
	maxFileSize: 1500,
	showClose: false,
	showCaption: false,
	browseLabel: '',
	removeLabel: '',
	browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
	removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
	removeTitle: 'Cancel or reset changes',
	elErrorContainer: '#kv-avatar-errors-1',
	msgErrorClass: 'alert alert-block alert-danger',
	defaultPreviewContent: '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" style="width: 140px;margin-bottom: 10px" alt="Your Avatar">',
	layoutTemplates: {main2: '{preview} {remove}'},
	allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif']
});
</script>
@endsection
