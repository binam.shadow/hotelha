<?php
namespace Modules\Blog\Config;
use App\Menu;
use App\Permission;
use App\Role;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    public $en_name = 'Blog';
    public $fa_name = 'مطلب';
    /**
     *
     */
    public function setup(){
		
		$role = Role::where('name', 'super-admin')->first();

        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $create = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $this->fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.list'], $permissions);

		$role->attachPermission($create);
		$role->attachPermission($edit);
		$role->attachPermission($list);

        $menu = [
            'display_name'  => $this->fa_name,
            'icon'          => 'create'
        ];
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($this->en_name)], $menu);

        // Sub menu
        $menu2 = [
            'display_name' => 'ایجاد ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.create')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.create'),
        ], $menu2);

        $menu2 = [
            'display_name' => 'لیست ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.list'),
        ], $menu2);

        $category_en_name = 'BlogCategory';
        $category_fa_name = 'دسته بندی مطالب';

        $permissions = [
            'display_name' => 'ایجاد ' . $category_fa_name
        ];
        $create = Permission::updateOrCreate(['name' => strtolower($category_en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $category_fa_name
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($category_en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $category_fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($category_en_name) . '.list'], $permissions);

		$role->attachPermission($create);
		$role->attachPermission($edit);
		$role->attachPermission($list);

        $menu2 = [
            'display_name' => 'ایجاد ' . $category_fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($category_en_name . '.create')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($category_en_name . '.create'),
        ], $menu2);

        $menu3 = [
            'display_name' => 'آرشیو ' . $category_fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($category_en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($category_en_name . '.list'),
        ], $menu3);


        Artisan::call('module:migrate', ['module' => $this->en_name]);
    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', strtolower($this->en_name) . '%')->delete();

        Role::where(['name' => strtolower($this->en_name)])->delete();

        Menu::where(['name' => strtolower($this->en_name)])->delete();

        $category_en_name = 'BlogCategory';
        Menu::where('name', 'like', strtolower($category_en_name) . '%')->delete();
        Permission::where('name', 'like', strtolower($category_en_name) . '%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => $this->en_name]);
    }
}
