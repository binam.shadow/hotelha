<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Medium;

class Blog extends Model {
	use LogsActivity;

    public $table = 'blogs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $fillable = [
        'category_id',
        'name',
        'description',
        'image',
        'active',
        'type',
        'tags',
        'url'
    ];

	protected static $logFillable = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blogCategory()
    {
        return $this->belongsTo(\Modules\Blog\Entities\BlogCategory::class);
    }
	
    // public function blogGalleries() {
    //     return $this->hasMany(\Modules\Blog\Entities\BlogGallery::class);
    // }
	
	
	public function getMainImageAttribute() {
		return $this->media()->where('manner', 'main')->first();
	}
	
	public function getGalleryAttribute() {
		return $this->media()->where('manner', 'gallery')->get();
	}

	public function media() {
		return $this->morphMany(Medium::class, 'mediumable');
	}

	public function getDescriptionForEvent(string $eventName): string {
		switch ($eventName) {
			case 'created':
				return "ثبت مطلب جدید";
				break;

			case 'updated':
				return "ویرایش مطلب";
				break;

			case 'deleted':
				return "حذف مطلب";
				break;

			default:
				return '';
				break;
		}
    }
}
