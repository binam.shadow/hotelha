<?php
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 9/20/2017
 * Time: 8:15 AM
 */

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Medium;


class BlogCategory extends Model {
    use SoftDeletes;
	use LogsActivity;

    public $table = 'blog_categories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        // 'image',
        // 'icon'
    ];

	protected static $logFillable = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function blogs() {
        return $this->hasMany(\Modules\Blog\Entities\Blog::class);
    }
    
    public function media() {
        return $this->morphMany(Medium::class, 'mediumable');
    }

	public function getIconAttribute(){
		return $this->media()->where('manner', 'icon')->first();
	}

	public function getImageAttribute(){
		return $this->media()->where('manner', 'image')->first();
    }
	

	public function getDescriptionForEvent(string $eventName): string {
		switch ($eventName) {
			case 'created':
				return "ثبت دسته بندی بلاگ";
				break;

			case 'updated':
				return "ویرایش دسته بندی بلاگ";
				break;

			case 'deleted':
				return "حذف دسته بندی بلاگ";
				break;

			default:
				return '';
				break;
		}
    }
}
