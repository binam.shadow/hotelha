<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BlogGallery
 * @package Modules\Blog\Entities
 * @version September 6, 2017, 8:32 am UTC
 *
 * @property \Modules\Blog\Entities\Blog blog
 * @property integer blog_id
 * @property string image
 */
class BlogGallery extends Model
{
    public $table = 'blog_galleries';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'blog_id',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'blog_id' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blog()
    {
        return $this->belongsTo(\Modules\Blog\Entities\Blog::class);
    }
}
