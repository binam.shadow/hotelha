<?php

Route::group([
    'middleware' => ['web', 'permission:blog*'],
    'prefix' => 'admin/blog',
    'namespace' => 'Modules\Blog\Http\Controllers'
], function() {

    /*** Start Blog ***/
    Route::get('create', [
        'as'    => 'blog.create',
        'uses'  => 'BlogController@create'
    ]);
    Route::post('create', [
        'as'    => 'blog.create',
        'uses'  => 'BlogController@store'
    ]);

    Route::get('list', [
        'as'    => 'blog.list',
        'uses'  => 'BlogController@list'
    ]);
    Route::get('dataList', [
        'as'    => 'blog.dataList',
        'uses'  => 'BlogController@dataList'
    ]);

    Route::get('edit/{id?}', [
        'as'    => 'blog.edit',
        'uses'  => 'BlogController@edit'
    ]);
    Route::post('edit/{id?}', [
        'as'    => 'blog.edit',
        'uses'  => 'BlogController@update'
    ]);
    Route::get('activate', [
        'as'	=> 'blog.activate',
        'uses'	=> 'BlogController@activate'
    ]);
    Route::get('delete', [
        'as'	=> 'blog.delete',
        'uses'	=> 'BlogController@delete'
    ]);
    Route::post('galleryDelete', [
        'as'    => 'blog.galleryDelete',
        'uses'  => 'BlogController@galleryDelete'
    ]);
    Route::post('imageDelete', [
        'as'    => 'blog.imageDelete',
        'uses'  => 'BlogController@imageDelete'
    ]);
    /*** End Blog ***/
    Route::group([
        'prefix' => 'category',
    ], function() {
        /*** Start Category Blog ***/
        Route::get('create', [
            'as' => 'blogcategory.create',
            'uses' => 'BlogCategoryController@create'
        ]);
        Route::post('create', [
            'as' => 'blogcategory.create',
            'uses' => 'BlogCategoryController@store'
        ]);

        Route::get('list', [
            'as' => 'blogcategory.list',
            'uses' => 'BlogCategoryController@list'
        ]);
        Route::get('dataList', [
            'as' => 'blogcategory.dataList',
            'uses' => 'BlogCategoryController@dataList'
        ]);

        Route::get('edit/{id?}', [
            'as' => 'blogcategory.edit',
            'uses' => 'BlogCategoryController@edit'
        ]);
        Route::post('edit/{id?}', [
            'as' => 'blogcategory.edit',
            'uses' => 'BlogCategoryController@update'
        ]);
        Route::get('activate', [
            'as' => 'blogcategory.activate',
            'uses' => 'BlogCategoryController@activate'
        ]);
        Route::get('delete', [
            'as'	=> 'blogcategory.delete',
            'uses'	=> 'BlogCategoryController@delete'
        ]);
        Route::post('imageDelete', [
            'as'    => 'blogcategory.imageDelete',
            'uses'  => 'BlogCategoryController@imageDelete'
        ]);
        Route::post('iconDelete', [
            'as'    => 'blogcategory.iconDelete',
            'uses'  => 'BlogCategoryController@iconDelete'
        ]);
        /*** End Category Blog ***/
    });
});
