<?php

namespace Modules\Blog\Http\Controllers;

use App\Classes\FileManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\Blog;
use Entrust;
use Modules\Blog\Entities\BlogCategory;
use Modules\Blog\Entities\BlogGallery;
use Modules\Blog\Http\Requests\BlogRequest;
use App\Medium;
use DataTables;

class BlogController extends Controller {

    public function create() {
        Entrust::can('blog.create') ?  : abort(403);
        $categories = BlogCategory::get();

        return view('blog::create', compact('categories'));
    }

    public function store(BlogRequest $request) {
        Entrust::can('blog.create') ?: abort(403);

        $data = $request->all();

        if($data['active'] == 'on')
            $data['active'] = 1;
        else
            $data['active'] = 0;

        $blog = Blog::create($data);
        if($request->image)
            Medium::make($request->image, 'Blog "' . $blog->name . '"', [
                 'position' => 0,
                 'model' 	=> 'blogs',
                 'model_id' => $blog->id,
                 'description'	=> 'Blog "' . $blog->name . '"'
             ], 'main')->store();
             
        if(isset($request->images)) {
            foreach ($request->images as $image) {
                Medium::make($image, 'Blog "' . $blog->name . '"', [
                     'position' => 0,
                     'model' 	=> 'blogs',
                     'model_id' => $blog->id,
                     'description'	=> 'Blog "' . $blog->name . '"'
                 ], 'gallery')->store();
            }
        }
        
        // Redirect with success message
        return redirect()->route('blog.create')
            ->with('msg', 'مطلب با موفقیت ایجاد شد')
            ->with('msg_color', 'bg-green');
    }

    public function edit($id = 0) {
        Entrust::can('blog.edit') ?  : abort(403);
        $categories = BlogCategory::get();
        $galleries = BlogGallery::where('blog_id', $id)->get();

        $blog = Blog::find($id);

        // return $blog->main_image;
        return view('blog::edit', compact('blog', 'categories', 'galleries'));
    }

    public function update($id, BlogRequest $request) {
        Entrust::can('blog.edit') ?  : abort(403);

        $blog = Blog::find($id);
        $blog->name = $request->name;
        $blog->description = $request->description;
        $blog->tags = $request->tags;
        if($request->active == 'on')
            $blog->active = 1;
        else
            $blog->active = 0;

        if($request->image)
            Medium::make($request->image, $blog->name, [
                 'position' => 0,
                 'model' 	=> 'blogs',
                 'model_id' => $blog->id,
                 'description'	=> $blog->name
             ], 'main')->store();
             
        if(isset($request->images)) {
            foreach ($request->images as $image) {
                Medium::make($image, $blog->name, [
                     'position' => 0,
                     'model' 	=> 'blogs',
                     'model_id' => $blog->id,
                     'description'	=> $blog->name
                 ], 'gallery')->store();
            }
        }

        $blog->save();
        return redirect()->route('blog.list')
            ->with('msg', \Lang::get('blog::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function list() {
        Entrust::can('blog.list') ?  : abort(403);

        return view('blog::list');
    }

    public function dataList(Request $request) {
        Entrust::can('blog.list') ?  : abort(403);

        return DataTables::of(Blog::query())->make();
    }

    public function activate(Request $request) {
        Entrust::can('blog.edit') ?  : abort(403);

        // Set default False (if action is active set True)
        $data['active'] = $request->action;

        // Update Blog
        Blog::find($request->id)->update($data);

        // Redirect with success message
        return ['status' => 'success'];
    }

    public function delete(Request $request) {
        Entrust::can('blog.edit') ?  : abort(403);

        $blog = Blog::find($request->id);
        FileManager::delete($blog->image);
        $galleries = BlogGallery::where('blog_id', $request->id)->get();
        foreach($galleries as $gallery){
            FileManager::delete($gallery->image);
        }
        $blog->delete();

        return ['status' => 'success'];
    }

    public function galleryDelete(Request $request) {
        $id = $request->key;
        $gallery = BlogGallery::find($id);
        FileManager::delete($gallery->image);
        $gallery->forceDelete();

        return ['status' => 'success'];
    }

    public function imageDelete(Request $request) {
        $id = $request->key;
        $blog = Blog::find($id);
        FileManager::delete($blog->image);
        $blog->image = null;
        $blog->save();

        return ['status' => 'success'];
    }
}
