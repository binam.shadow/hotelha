@extends('adminbsb.layouts.master')

@section('title')
    @lang('hotel::default.create')
@endsection

@section('styles')
    @include('hotel::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('hotel::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('hotel::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('hotel.store') }}">
                        {{ csrf_field() }}


                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">نام <span class="col-red">*</span></label>
                            </div>
                        </div>


                        <div class="row clearfix">
                        <div class="col-md-6">
                        <label class="form-label">استان ها</label>
                        <select class="col-md-8" name="pid">
                        <option value="0" selected>انتخاب کنید</option>
                        @foreach($provs as $prov)
                        <option value=" {{ $prov->Code }}">{{ $prov->Name }}</option>
                        @endforeach
                        </select>
                        </div>
                        </div>

                        {{--<div class="form-group row">--}}
                            {{--@foreach($provs as $permission)--}}
                                {{--@php $i = $permission->Code  @endphp--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<input id="md_checkbox_{{$i}}" class="chk-col-red" name="permissions[]" type="checkbox" value="{{$i}}">--}}
                                    {{--<label for="md_checkbox_{{$i}}">{{ $permission->Name }}</label>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}


                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="phone" value="{{ old('phone') }}" required="" aria-required="true" type="text">
                                <label class="form-label">تلفن <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="description" value="{{ old('description') }}" required="" aria-required="true" type="text">
                                <label class="form-label">توضیحات <span class="col-red">*</span></label>
                            </div>
                        </div>








                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect"> ایجاد هتل</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('hotel::partials._script')
@endsection
