@extends('adminbsb.layouts.master')

@section('title')
    @lang('hotel::default.create')
@endsection

@section('styles')
    @include('hotel::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('hotel::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('hotel::partials._errors')
                    <form id="hotelEdit" method="post" action="{{ route('hotel.update', ['hotel' => $hotel])) }}">
                        {{ csrf_field() }}
                        @include('hotel::_form', ['saveButtonLabel'=> 'edit'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('hotel::partials._script')
@endsection
