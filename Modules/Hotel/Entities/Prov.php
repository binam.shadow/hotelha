<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;

class Prov extends Model
{
    protected $fillable = ['name'];
}
