<?php
namespace Modules\Hotel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
use Modules\Hotel\Entities\Hotel;
use Modules\Hotel\Entities\Prov;
use GuzzleHttp\Client;
use \App\Classes\Travel;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('hotel::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
         
        $hotel = new Hotel();
//        $client = new Client();
//        $provs = Prov::all();
            //$client->get('http://api.alaedin.travel/Province/E7B71401-AEFE-4E05-8738-8F3328E4A71C');
//        return $provs;


        $provs = Travel::getProvinces();


        return view('hotel::create', compact('hotel','provs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('hotel.create') ?: abort(403);
        $this->validate($request,[
    //        'name' => 'required',
            // ...
        ]);
       // dLO(i+?7{&06
        Hotel::create($request->all());
        return back()
            ->with('msg', "با موفقیت اضافه شد")
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Hotel $hotel)
    {
        return view('hotel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Hotel $hotel)
    {
        return view('hotel::edit', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Hotel $hotel, Request $request)
    {
        Entrust::can('hotel.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $hotel->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Hotel $hotel)
    {
        try {
            $hotel->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('hotel.list')
                ->with('msg', \Lang::get('hotel::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function hotelList(){
          return DataTables::of(Hotel::query())
                      ->make(true);
     }
}
