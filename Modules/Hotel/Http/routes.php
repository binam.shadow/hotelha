<?php

Route::group(['middleware' => 'web', 'prefix' => 'hotels', 'namespace' => 'Modules\Hotel\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'hotel.list',
        'uses'	=> 'HotelController@index'
    ]);
    Route::get('create', [
        'as'	=> 'hotel.create',
        'uses'	=> 'HotelController@create'
    ]);
    Route::post('store', [
        'as'	=> 'hotel.store',
        'uses'	=> 'HotelController@store'
    ]);
    Route::get('{hotel}/edit', [
        'as'	=> 'hotel.edit',
        'uses'	=> 'HotelController@edit'
    ]);
    Route::patch('{hotel}', [
        'as'	=> 'hotel.update',
        'uses'	=> 'HotelController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'hotel.dataList',
        'uses'	=> 'HotelController@hotelList'
    ]);
    Route::delete('{hotel}', [
        'as'	=> 'hotel.delete',
        'uses'	=> 'HotelController@destroy'
    ]);
});