<?php
namespace Modules\Admin\Http\ViewComposers;
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 7/23/2017
 * Time: 9:04 PM
 */

use App\Menu;
use App\Setting;
use Illuminate\View\View;
use Auth;
use Morilog\Jalali\jDate;

class LeftSideComposer
{
    public function __construct()
    {
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        // $menus = Menu::selectRaw(" *, CASE WHEN parent_id = 0 THEN LPAD(id,4,'0') Else (CASE WHEN parent_parent_id = 0 THEN concat(LPAD(parent_id,4,'0'),id) ELSE concat(LPAD(parent_parent_id,4,'0'),parent_id,id) END) END as parentId")
        //     ->orderBy('parentId')
        //     ->get();
        // $menus = json_decode(json_encode($menus), true);
        
        $menus = Menu::where('parent_id', 0)
                    ->with(['sub_menus' => function ($query) {
                        return $query->with('sub_menus');
                    }])
                    ->get();
                    
        $user = Auth::user();
		
		$title = Setting::where('key', 'panel_title')->first();
        
        $user->last_login = jDate::forge($user->logged_in_at)->ago();
        
        $theme_color = Setting::where('key', 'theme_color')->first();
        if(!$theme_color)
            $theme_color = 'red';
        else
            $theme_color = $theme_color->value;

        $view->with([
            'menus' => $menus,
            'user' => $user,
            'title' => $title,
            'theme_color' => $theme_color
        ]);
    }

}