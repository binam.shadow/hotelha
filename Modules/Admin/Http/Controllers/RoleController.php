<?php

namespace Modules\Admin\Http\Controllers;

use App\RoleUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Permission;
use App\Role;
use Validator;
use Entrust;
use DataTables;

class RoleController extends Controller {

    public function create() {
        Entrust::can('role.create') ? : abort(403);

        $permissions = Permission::get();
        return view('admin::role.create', compact('permissions'));
    }

    public function store(Request $request) {
        Entrust::can('role.create') ? : abort(403);

        $validator = Validator::make($request->all(), [
            'fa_name'  => 'required|max:256',
            'en_name'  => 'required|max:256'
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        // Create role
        $role = ['display_name' => $request->fa_name];
        $role = Role::updateOrCreate(['name' => strtolower($request->en_name)], $role);

        // Attach permissions to role
        $permissions = Permission::whereIn('id', $request->permissions)->get()->pluck('id');

        $role->attachPermissions($permissions);

        // Redirect with success message
        return redirect()->route('role.list')
            ->with('msg', \Lang::get('admin::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    public function roleList() {
        Entrust::can('role.list') ? : abort(403);

        return view('admin::role.list');
    }

    public function dataList(Request $request) {
        Entrust::can('role.list') ? : abort(403);

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }


        $roles = Role::where('name', '!=', 'super-admin');
		$data = DataTables::of($roles)->make();

        return $data;
    }

    public function edit($id) {
        Entrust::can('role.edit') ? : abort(403);

        $role_delete = Role::where('id', $id)->first();

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }
        if(!$is_super_admin && $role_delete->name == 'super-admin')
            abort(403);


        $role = Role::where('id', $id)->with('permissions')->get()->first();
        $permissions = Permission::get();

        return view('admin::role.edit', compact('role', 'permissions'));
    }

    public function update($id = 0, Request $request) {
        Entrust::can('role.edit') ? : abort(403);

        Role::where('id', $id)->update([
            'display_name' => $request->fa_name,
            'name'         => strtolower($request->en_name)
        ]);
        $role = Role::where('id', $id)->first();
        $role->perms()->sync([]);

        // Attach permissions to role
        $permissions = Permission::whereIn('id', $request->permissions)->get()->pluck('id');
        $role->attachPermissions($permissions);

        return redirect()->route('role.list')
            ->with('msg', \Lang::get('admin::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function delete(Request $request) {
        Entrust::can('role.edit') ? : abort(403);

        $role_delete = Role::where('id', $request->id)->first();

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }
        if(!$is_super_admin && $role_delete->name == 'super-admin')
            abort(403);

        // Check role exit
        $role = Role::where('id', $request->id)->first();
        (count($role)) ? : abort(404);

        $role->delete();
        return ['status' => 'success'];
    }
}
