<?php

namespace Modules\Admin\Http\Controllers;

use App\RoleUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Modules\Admin\Http\Requests\CreateAdminRequest;
use Modules\Admin\Http\Requests\EditAdminRequest;
use App\User;
use App\Role;
use Entrust;
use App\Classes\FileManager;
use Morilog\Jalali\Facades\jDateTime;


class AdminController extends Controller {

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function create() {
        Entrust::can('admin.create') ?  : abort(403);

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }
        if(!$is_super_admin)
            $roles = Role::where('name','!=','super-admin')->get();
        else
            $roles = Role::get();
        return view('admin::create', compact('roles'));
    }

    public function store(Request $request) {
        Entrust::can('admin.create') ?  : abort(403);
        $validator = Validator::make($request->all(), [
            'role'      => 'required',
            'phone'      => 'required',
            'email'     => 'email',
            'password'  => 'required|min:5',
            'confirm_password'  => 'same:password'
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        // Get name and email
        $admin = $request->only('name', 'email', 'phone');
        $errors = new Collection();
        $errors->push(\Lang::get('admin::default.phone_not_unique'));
        $user_phone = User::where('phone', $admin['phone'])->first();
        if (count($user_phone))
            return back()
                ->with('errors', $errors)
                ->withInput();
        // Hash Password
        $admin['password'] = bcrypt($request->password);

        // Check sctive or deactive (Default is false)
        $admin['active'] = 0;
        if($request->active)
            $admin['active'] = 1;

        // $admin['avatar'] = $request->avatar;


        // Create admin
        $user = User::create($admin);

        // Attach role to new admin
        $role = Role::find($request->role);
        $user->attachRole($role->id);

        // Redirect with success message
        return redirect()->route('admin.create')
            ->with('msg', 'مدیر با موفقیت ایجاد شد')
            ->with('msg_color', 'bg-green');
    }

    public function edit($id = 0) {
        Entrust::can('admin.edit') ?  : abort(403);
        // Check Admin exist
        $admin = User::where('id', $id)->first();
        $role_user_admin = RoleUser::where('user_id',$admin->id)->first();
        if(!is_null($role_user_admin)) {
            $role_admin = Role::where('id', $role_user_admin->role_id)->first();
        }

        if(!count($admin)) abort(404);

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }
        if(!$is_super_admin && $role_admin->name == 'super-admin')
            abort(403);


        return view('admin::edit', compact('admin'));
    }

    public function update($id, Request $request) {
        Entrust::can('admin.edit') ?  : abort(403);
        $validator = Validator::make($request->all(), [
            'phone'      => 'required',
            'email'     => 'email',
            'password'  => 'nullable|min:5',
            'confirm_password'  => 'same:password'
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'name'  => $request->name,
            'phone'  => $request->phone,
        ];
        $errors = new Collection();
        $errors->push(\Lang::get('admin::default.phone_not_unique'));
        $user_phone = User::where('phone', $data['phone'])->first();
        if (count($user_phone) && $user_phone->id != $id)
            return back()
                ->with('errors', $errors)
                ->withInput();

        $admin = User::where('id', $id)->first();

        // Check new password Entered
        if($request->password)
            $data['password'] = bcrypt($request->password);

        // Check active
        $data['active'] = 0;
        if($request->active)
            $data['active'] = 1;

        $data['avatar'] = $request->avatar;

        $admin->update($data);
        return redirect()->route('admin.list')
            ->with('msg', \Lang::get('admin::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function list() {
        Entrust::can('admin.list') ?  : abort(403);

        return view('admin::list');
    }

    public function dataList(Request $request) {
        Entrust::can('admin.list') ?  : abort(403);

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }

        $data['draw'] =  $request->draw;
        $user_groups = Role::with(['users' => function ($query) use ($request) {
            $order_column = $request->order[0]['column'];
            $order_dir = $request->order[0]['dir'];
            $query->where(function ($query) use($request) {
                $query->where("name", "LIKE", "%" . $request->search['value'] . "%")
                    ->orWhere("email", "LIKE", "%" . $request->search['value'] . "%");
            })
                ->orderBy($request->columns[$order_column]['data'], $order_dir);
        }]);

        if(!$is_super_admin) {
            $user_groups =  $user_groups->where('name','!=','super-admin');
        }
        $user_groups =  $user_groups->where('name', '!=', 'user')
            ->limit($request->length)
            ->offset($request->start)
            ->get();
        $users = new Collection();
        foreach($user_groups as $user_group){
            $users = $users->merge($user_group->users->where('email', '!=', 'rohamadmin'));
        }

        $data['data'] = $users;
        $count = $users->count();
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();
        foreach ($data['data'] as $item) {
            $date_req = explode(' ', $item->created_at);
            $item->date_req_persian = $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
        }
        return $data;
    }

    public function activate(Request $request) {
        Entrust::can('admin.edit') ?  : abort(403);

        // Set default False (if action is active set True)
        $data['active'] = $request->action;

        $admin = User::where('id', $request->id)->first();
        $role_user_admin = RoleUser::where('user_id',$admin->id)->first();
        if(!is_null($role_user_admin)) {
            $role_admin = Role::where('id', $role_user_admin->role_id)->first();
        }

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }
        if(!$is_super_admin && $role_admin->name == 'super-admin')
            abort(403);

        // Update User
        User::where('id', $request->id)->update($data);

        // Redirect with success message
        return ['status' => 'success'];
    }

    public function fileManager(Request $request) {
        Entrust::can('admin.upload') ?  : abort(403);
        ob_start();
        require(public_path()."/filemanagerWithPermission/dialog.php");
        return ob_get_clean();
    }

    public function delete(Request $request) {
        Entrust::can('admin.create') ?  : abort(403);

        $admin = User::where('id', $request->id)->first();
        $role_user_admin = RoleUser::where('user_id',$admin->id)->first();
        if(!is_null($role_user_admin)) {
            $role_admin = Role::where('id', $role_user_admin->role_id)->first();
        }

        $user = \Auth::user();
        $role_user = RoleUser::where('user_id',$user->id)->first();
        $is_super_admin = false;
        if(!is_null($role_user)) {
            $role = Role::where('id', $role_user->role_id)->first();
            if ($role->name == 'super-admin')
                $is_super_admin = true;
        }
        if(!$is_super_admin && $role_admin->name == 'super-admin')
            abort(403);

        User::where('id', $request->id)->delete();
        return ['status' => 'success'];
    }

    public function themeColor(Request $request) {
        Setting::updateOrCreate(['key' => 'theme_color'] ,['value' => $request->color]);
        return ['status' => 'success'];
    }

    public function avatarDelete(Request $request) {
        $id = $request->key;
        $user = User::find($id);
        FileManager::delete($user->avatar);
        $user->avatar = null;
        $user->save();

        return ['status' => 'success'];
    }
}
