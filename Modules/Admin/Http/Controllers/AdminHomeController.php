<?php
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 9/24/2017
 * Time: 10:19 AM
 */

namespace Modules\Admin\Http\Controllers;

use App\User;
use Illuminate\Routing\Controller;
use Modules\Message\Entities\Message;

use App\Classes\Smsir;

class AdminHomeController extends Controller {

    public function index() {
                    
        return view('adminbsb.index');
    }

    public function activeUsers(){
        $results = \Analytics::performQueryReal('rt:activeUsers', []);
        //dd($results);
        return $results->getTotalsForAllResults()['rt:activeUsers'];//$results->totalResults;
    }
}
