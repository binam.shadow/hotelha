<?php
namespace Modules\Admin\Config;
use App\Menu;
use App\Permission;
use App\Role;
use App\Setting;
use App\User;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    public $en_name = 'Admin';
    public $fa_name = 'مدیر';

    /**
     *
     */
    public function setup(){
        //*******************Admin Create or Edit
        $permissions = [
            'display_name' => 'آپلود و ویرایش عکس'
        ];
        Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.upload'], $permissions);

        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $admin_create = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $admin_edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $this->fa_name
        ];
        $admin_list = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.list'], $permissions);

        $role = ['display_name' => $this->fa_name];
        $role = Role::updateOrCreate(['name' => strtolower($this->en_name)], $role);

        $role->attachPermission($admin_create);
        $role->attachPermission($admin_edit);
        $role->attachPermission($admin_list);



//
//        //
//        User::updateOrCreate([
//            'name'  => 'armin',
//            'email' => 'a@a.com',
//            'password'  => bcrypt('12345'),
//            'active'    => 1,
//            'phone'     => '09373934074'
//        ]);
//        $user = User::where(['email' => 'a@a.com'])->first();
//        $user->attachRole(Role::where('name', 'admin')->first()->id);
//-------------------------------------------------------------------------------------------

        $en_super_admin = 'Super-Admin';
        $fa_super_admin = 'مدیر ارشد';
//        $permissions = [
//            'display_name' => 'آپلود و ویرایش عکس'
//        ];
//        Permission::updateOrCreate(['name' => strtolower($en_super_admin) . '.upload'], $permissions);
//
//        $permissions = [
//            'display_name' => 'ایجاد ' .  $fa_super_admin
//        ];
//        $admin_create = Permission::updateOrCreate(['name' => strtolower($en_super_admin) . '.create'], $permissions);
//
//        $permissions = [
//            'display_name' => 'ویرایش ' .  $fa_super_admin
//        ];
//        $admin_edit = Permission::updateOrCreate(['name' => strtolower($en_super_admin) . '.edit'], $permissions);
//
//        $permissions = [
//            'display_name' => 'آرشیو ' . $fa_super_admin
//        ];
//        $admin_list = Permission::updateOrCreate(['name' => strtolower($en_super_admin) . '.list'], $permissions);

        $role = ['display_name' => $fa_super_admin];
        $role = Role::updateOrCreate(['name' => strtolower($en_super_admin)], $role);

        $user_create = Permission::where('name', 'user.create')->first();
        $user_edit = Permission::where('name', 'user.edit')->first();
        $user_list = Permission::where('name', 'user.list')->first();

		$role->attachPermission($user_create);
        $role->attachPermission($user_edit);
        $role->attachPermission($user_list);

        $role->attachPermission($admin_create);
        $role->attachPermission($admin_edit);
        $role->attachPermission($admin_list);

        $user = User::updateOrCreate([
            'name'  => 'rohamadmin',
            'email' => 'rohamadmin',
            'password'  => bcrypt('1q2w3e4r5t6y'),
            'active'    => 1,
            'phone'     => 'abcdefgh'
        ]);
        $user->attachRole(Role::where('name', strtolower($en_super_admin))->first()->id);

        //-------------------------------------------------------------------------------------

        // Admin main menu
        $menu1 = [
            'display_name'  => $this->fa_name,
            'icon'          => 'supervisor_account'
        ];
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($this->en_name)], $menu1);


        // Admin sub menu (Admin > Create Admin)
        $menu2 = [
            'display_name' => 'ایجاد ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.create')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.create'),
        ], $menu2);

        // Admin sub menu (Admin > List Admin)
        $menu3 = [
            'display_name' => 'آرشیو ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.list'),
        ], $menu3);


        $role_en_name = 'Role';
        $role_fa_name = 'نقش';

        $permissions = [
            'display_name' => 'تعریف ' . $role_fa_name
        ];
        $role_create = Permission::updateOrCreate(['name' => strtolower($role_en_name) . '.create'], $permissions);
        $role->attachPermission($role_create);

        $permissions = [
            'display_name' => 'لیست ' . $role_fa_name
        ];
        $role_list = Permission::updateOrCreate(['name' => strtolower($role_en_name) . '.list'], $permissions);
        $role->attachPermission($role_list);

        $permissions = [
            'display_name' => 'ویرایش ' . $role_fa_name
        ];
        $role_list = Permission::updateOrCreate(['name' => strtolower($role_en_name) . '.edit'], $permissions);
        $role->attachPermission($role_list);


        // Admin sub menu (Admin > Create role)
        $menu3 = [
            'display_name' => 'تعریف ' . $role_fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($role_en_name . '.create')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($role_en_name . '.create'),
        ], $menu3);

        // Admin sub menu (Admin > List role)
        $menu3 = [
            'display_name' => 'لیست ' . $role_fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($role_en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($role_en_name . '.list'),
        ], $menu3);


        Artisan::call('module:migrate', ['module' => $this->en_name]);

    }

    /**
     * @throws \Exception
     */
    public function remove(){

		Permission::where('name', 'like', strtolower($this->en_name).'%')->delete();

        Role::where(['name' => strtolower($this->en_name)])->delete();

        Menu::where('name', 'like', strtolower($this->en_name).'%')->delete();
        Menu::where('name', 'setting')->delete();
        Menu::where('name', 'like', 'role%')->delete();

        User::where(['email' => 'rohamadmin'])->delete();

		$en_super_admin = 'Super-Admin';
        $fa_super_admin = 'مدیر ارشد';
        Role::where('name', 'like', strtolower($en_super_admin).'%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => $this->en_name]);
    }
}
