@extends('adminbsb.layouts.master')

@section('title')
@lang('admin::default.create')
@endsection

@section('styles')
<!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
.file-footer-caption, .file-thumbnail-footer{
	display: none;
}
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('admin::default.create')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form id="adminCreate" method="post" action="{{ route('admin.create') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row clearfix">
						<div class="col-md-6">
							<div class="form-group form-float">
		                        <div class="form-line">
		                            <input class="form-control" name="name" value="{{ old('name') }}" aria-required="true" type="text">
		                        	<label class="form-label">@lang('admin::default.name') <span class="col-red">*</span></label>
		                        </div>
		                    </div>
		                    <div class="form-group form-float">
		                        <div class="form-line">
		                            <input class="form-control" name="email" value="{{ old('email') }}" required="" aria-required="true" type="text">
		                        	<label class="form-label">@lang('admin::default.email') <span class="col-red">*</span></label>
		                        </div>
		                    </div>
							<div class="form-group form-float">
		                        <div class="form-line">
		                            <input class="form-control" name="phone" value="{{ old('phone') }}" id="phone" aria-required="true" type="text">
		                        	<label class="form-label">@lang('admin::default.phone') <span class="col-red">*</span></label>
		                        </div>
		                    </div>
		                    <div class="form-group form-float">
		                        <div class="form-line">
		                            <input class="form-control" name="password" value="{{ old('password') }}" id="password" required="" aria-required="true" type="text">
		                        	<label class="form-label">@lang('admin::default.password') <span class="col-red">*</span></label>
		                        </div>
		                    </div>
		                    <div class="form-group form-float">
		                        <div class="form-line">
		                            <input class="form-control" name="confirm_password" value="{{ old('confirm_password') }}" required="" aria-required="true" type="text">
		                        	<label class="form-label">@lang('admin::default.confirm_password') <span class="col-red">*</span></label>
		                        </div>
		                    </div>
						</div>
						<style>
							.col-center{
								float: none;
								margin: 0 auto;
							}
						</style>
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب آواتار</p>
							<div class="col-sm-5 col-center text-center">
								<img style="width: 100%" id="avatar-show" src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}">
							</div>
							<input id="avatar" type="hidden" name="avatar">
							<a href="javascript:void(0)" onclick="deleteImage('avatar')">حذف</a> <a class="btn btn-info" type="button" href="javascript:open_popup('{{url('filemanager/dialog.php?fldr=images/avatars&relative_url=1&type=1&amp;popup=1&amp;field_id=avatar')}}')" >انتخاب تصویر</a>
						</div>
					</div>
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <select name="role" class="form-control">
                            	<option value="" checked>-- انتخاب دسترسی --</option>
                            	@foreach($roles as $role)
									<option value="{{ $role->id }}">{{ $role->display_name }}</option>
                            	@endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="switch">
                            <label>@lang('admin::default.active') <input name="active" checked="" type="checkbox"><span class="lever switch-col-blue"></span> @lang('admin::default.deactive')</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('admin::default.create')</button>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script type="text/javascript">
	function responsive_filemanager_callback(field_id){
		$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

	}
	function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
	function deleteImage(field_id) {
		$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
		$('#'+field_id).val('');
	}
	$('#adminCreate').validate({
        rules: {
            'email': {
                required: true,
                email: true
            },
            'password': {
            	required: true,
            	minlength: 5
            },
            'confirm_password': {
            	required: true,
				equalTo: '#password'
            }
        },
        messages: {
        	'confirm_password':{
        		equalTo: 'تکرار رمز عبور وارد شده باید با هم برابر باشد',
        	}
        }
    });
</script>
@endsection
