@extends('adminbsb.layouts.master')

@section('title')
@lang('admin::default.admin_edit')
@endsection

@section('styles')
<style>
	.file-preview{
		display: block !important;
	}
	.file-thumbnail-footer, .fileinput-remove-button{
		display: none !important;
	}
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('admin::default.admin_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="adminCreate" method="post" action="{{ route('admin.edit', ['id' => $admin->id]) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row clearfix">
						<div class="col-md-6">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="name" value="{{ $admin->name }}" aria-required="true" type="text">
									<label class="form-label">@lang('admin::default.name') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="email" value="{{ $admin->email }}" required="" aria-required="true" type="text" disabled="">
									<label class="form-label">@lang('admin::default.email') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
		                        <div class="form-line">
		                            <input class="form-control" name="phone" value="{{ $admin->phone }}" id="phone" aria-required="true" type="text">
		                        	<label class="form-label">@lang('admin::default.phone') <span class="col-red">*</span></label>
		                        </div>
		                    </div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="password" value="" id="password"  aria-required="true" type="text">
									<label class="form-label">@lang('admin::default.password') <span class="col-red">*</span></label>
								</div>
							</div>
							<p>به منظور تغییر ندادن رمز عبور آن را خالی بگذارید</p>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="confirm_password" value="" aria-required="true" type="text">
									<label class="form-label">@lang('admin::default.confirm_password') <span class="col-red">*</span></label>
								</div>
							</div>
						</div>
						<style>
							.col-center{
								float: none;
								margin: 0 auto;
							}
						</style>
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب آواتار</p>
							<div class="col-sm-5 col-center text-center">
								<img style="width: 100%" id="avatar-show" @if($admin->avatar == '') src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" @else src="{{ asset('uploads/' . $admin->avatar) }}" @endif>
							</div>
							<input id="avatar" type="hidden" name="avatar" @if($admin->avatar != '') value="{{$admin->avatar}}" @endif>
							<a href="javascript:void(0)" onclick="deleteImage('avatar')">حذف</a> <a class="btn btn-info" type="button" href="javascript:open_popup('{{url('filemanager/dialog.php?fldr=images/avatars&relative_url=1&type=1&amp;popup=1&amp;field_id=avatar')}}')" >انتخاب تصویر</a>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="switch">
							<label>@lang('admin::default.active') <input name="active" @if($admin->active) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('admin::default.deactive')</label>
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('admin::default.admin_edit')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script type="text/javascript">
function responsive_filemanager_callback(field_id){
	$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

}
function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
function deleteImage(field_id) {
	$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
	$('#'+field_id).val('');
}
$(".file-input").fileinput({
	@if(!is_null($admin->avatar))
	initialPreview: [
		"{{ asset('uploads/' . $admin->avatar) }}"
	],
	initialPreviewFileType: 'image',
	initialPreviewAsData: true,
	@endif
	language: 'fa',
	browseOnZoneClick: true,
	overwriteInitial: true,
	maxFileSize: 1500,
	showClose: false,
	showCaption: false,
	browseLabel: '',
	removeLabel: '',
	browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
	removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
	removeTitle: 'Cancel or reset changes',
	elErrorContainer: '#kv-avatar-errors-1',
	msgErrorClass: 'alert alert-block alert-danger',
	defaultPreviewContent: '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" style="width: 140px;margin-bottom: 10px" alt="Your Avatar">',
	layoutTemplates: {main2: '{preview} {remove}'},
	allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif']
});
$('#adminCreate').validate({
	rules: {
		'email': {
			required: true,
			email: true
		},
		'password': {
			minlength: 5
		},
		'confirm_password': {
			equalTo: '#password'
		}
	},
	messages: {
		'confirm_password':{
			equalTo: 'تکرار رمز عبور وارد شده باید با هم برابر باشد',
		}
	}
});
</script>
@endsection
