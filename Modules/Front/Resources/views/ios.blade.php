@extends('front::layouts.master')

@section('title')
    راهنمای تصویری نصب اپلیکیشن الوویلا ورژن iOS
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="about-us">
        <div class="container">
            <div class="row p-4">
                <div class="col-md-8">
                    <h4>راهنمای تصویری نصب اپلیکیشن الوویلا ورژن iOS</h4>
                    <div>
                        <div class="row">
                            <div class="container">
                                {!! $description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection