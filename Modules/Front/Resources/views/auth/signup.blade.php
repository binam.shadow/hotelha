@extends('front::layouts.master')

@section('title')
  ثبت نام
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-center clearfix form-login p-5">
                    <h4>ثبت نام</h4><hr/>

                    <form id="signupForm" method="post" action="{{route('front.signup')}}">
                        {{csrf_field()}}

                        <div class="col-md-12 mb-3">
                            <label for="">نام و نام خانوادگی</label>
                            <input type="text" name="name" placeholder="نام و نام خانوادگی" required value="{{old('name')}}" class="form-control text-right-important pr-3">
                            <p class="col-white">{{$errors->first('name')}}</p>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label for="">شماره تلفن</label>
                            <input type="text" name="phone" placeholder="09123456789" required value="{{old('phone')}}" class="form-control pr-3">
                            <p class="col-white">{{$errors->first('phone')}}</p>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label for="">کدملی</label>
                            <input type="text" name="national_code" placeholder="کد ملی" required value="{{old('national_code')}}" class="form-control pr-3">
                            <p class="col-white">{{$errors->first('national_code')}}</p>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label for=""> کد معرف (دلخواه)</label>
                            <input type="text" name="recommend_code" placeholder="کد معرف" value="{{old('recommend_code')}}" class="form-control pr-3">
                            <p class="col-white">{{$errors->first('recommend_code')}}</p>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox text-right" dir="ltr">
                                <label class="inline">ثبت نام در سایت به منزله پذیرش<a class="col-rules" target="_blank" href="{{route('front.rules')}}"> قوانین و مقررات </a>الوویلا میباشد </label>
                            </div>
                            <div class="clearfix"></div>
                            <button type="submit" class="submit send mt-5 pr-5 pl-5">ثبت نام</button>
                            <div class="clearfix"></div>
                            <hr>
                            <a class="btn btn-block btn-bottom-login" href="{{route('front.login')}}"> قبلا در الوویلا ثبت‌ نام کرده‌اید؟ وارد شویــد </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>

    <script>
        function parseArabic(str) {
            if(str == '') return '';
            var out_num = Number( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
                return d.charCodeAt(0) - 1632; // Convert Arabic numbers
            }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
                return d.charCodeAt(0) - 1776; // Convert Persian numbers
            }) );
            return isNaN(out_num) ? 0 : out_num;
        }

        function convertNumbers2English(string) {
            return string.replace(/[\u0660-\u0669]/g, function (c) {
                return c.charCodeAt(0) - 0x0660;
            }).replace(/[\u06f0-\u06f9]/g, function (c) {
                return c.charCodeAt(0) - 0x06f0;
            });
        }

        jQuery.validator.addMethod("valid_national_code", function(value, element) {
            value = convertNumbers2English(value);
            var L=value.length;

            if(L<8 || parseInt(value,10)==0) return false;
            value=('0000'+value).substr(L+4-10);
            if(parseInt(value.substr(3,6),10)==0) return false;
            var c=parseInt(value.substr(9,1),10);
            var s=0;
            for(var i=0;i<9;i++)
                s+=parseInt(value.substr(i,1),10)*(10-i);
            s=s%11;
            return (s<2 && c==s) || (s>=2 && c==(11-s));
            return true;
        }, "کد ملی وارد شده معتبر نیست");

        $("#signupForm").validate({
            errorClass: "error",
            rules: {
                national_code: {
                    valid_national_code: true
                }
            }
        });
    </script>
@endsection