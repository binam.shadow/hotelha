@extends('front::layouts.master')

@section('title')
  ورود
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="login">
        <div class="container">
            <div class="row">


                <div class="col-md-5 col-center clearfix form-login p-5">
                    {{--<p class="text-center">--}}
                        {{--<img src="{{ asset('front/img/logo.svg')}}" width="50">--}}
                    {{--</p>--}}
                    <h3 class="text-right my-0 col-white">ورود</h3>
                    <hr>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (Session::has('msg'))
                        <div class="alert alert-success">{{ Session::get('msg') }}</div>
                    @endif
                    <form method="post" action="{{route('front.login')}}">
                        {{csrf_field()}}
                        <div class="col-md-12 mb-2 @if(Session::has('success_register')) hidden @endif">
                            <label for="">شماره تلفن</label>
                            <input type="text" name="phone" placeholder="09123456789" value="{{old('phone')}}" class="form-control text-left pr-3">
                        </div>
                        <div class="col-md-12">
                            <label for="">رمزعبور</label>
                            <input type="password" name="password" placeholder="*******" autocomplete="off" class="form-control text-left pr-3">
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox text-right">
                                <input id="remember_me" class="chk-col-red" name="remember_me" type="checkbox" value="">
                                <label for="remember_me" class="inline pr-5">مرا به خاطر بسپار</label><br>
                            </div>
                            <div class="clearfix"></div>
                            <a class="col-forget" href="{{route('front.forget')}}">رمز عبور خود را فراموش کرده اید؟</a>
                            <button type="submit" class="submit send mt-5 pr-5 pl-5">ورود</button>
                            <div class="clearfix"></div>
                            <hr>
                            <a class="btn btn-block btn-bottom-login" href="{{route('front.signup')}}"> کاربر جدید هستید؟ ثبت‌ نام در الوویلا </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection