@extends('front::layouts.master')

@section('title')
    فراموشی رمز عبور
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-center clearfix form-login p-5">
                    <h4>فراموشی رمز عبور</h4><hr/>
                    <label>شماره خود را وارد کنید، ما رمز عبور موقت را به شماره شما sms ارسال خواهیم کرد</label>
                    <form method="post" action="{{route('front.forget')}}">
                        {{csrf_field()}}
                        <div class="col-md-12 right mb-2">
                            <label for="">شماره تلفن</label>
                            <input type="text" name="phone" placeholder="09123456789" class="form-control text-center pr-3">
                            <p class="col-white">{{$errors->first('phone')}}</p>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="submit send mt-5 pr-5 pl-5">ارسال</button>
                        </div>
                    </form>





                </div>
            </div>
        </div>
    </section>
@endsection