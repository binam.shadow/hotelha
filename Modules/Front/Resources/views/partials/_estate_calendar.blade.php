@push('stack_styles')
<link href='{{ asset('backend/plugins/fullcalendar/dist/fullcalendar.css')}}' rel='stylesheet' />
<style>
    .fc-event .fc-content {
        text-align: center;
    }
    .fc-event .fc-title {
        font-size: 13px;
    }
</style>
@endpush

@push('stack_scripts')
<script src='{{ asset('backend/plugins/fullcalendar/lib/moment/moment.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/lib/moment/moment-jalaali.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/lib/jquery-ui/jquery-ui.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/dist/fullcalendar.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/lang/fa.js')}}'></script>
<script>

    var now = "{{$now}}";

    <?php
    echo 'var events = '.json_encode($events);
    ?>

    // if(!events.length) {
    //     console.log(events);
    //     initialCalendar();
    // }

    initialCalendar();
    setFullCalendar();

    {{--console.log("{{$events}}");--}}
    console.log(now)
    function setFullCalendar() {
        console.log(events);
        $('#calendar').fullCalendar({
            lang: 'fa',
            isJalaali: true,
            selectable: false,
            isRTL: true,
            defaultDate: now,

            eventLimit: true, // allow "more" link when too many events
            events: events,
            droppable: false,
            editable: false,

            allDays:false,

            dayClick: function(date, jsEvent, view, resource) {

                data = date.format();
                var selected_date = convertToEn(date.format());

                $("input[name=selected_date]").val(selected_date);
                selected_event = events.filter(e => e.start == selected_date);
                if(selected_event.length){
                    $("input[name=date_price]").val(selected_event[0].price)
                    $("input[name=active_date]").prop("checked",selected_event[0].active)
                }
                // $('#calendar').fullCalendar( 'removeEventSource', events );
                // // initialCalendar();
                // $('#calendar').fullCalendar( 'addEventSource', events );
                // date="2018-01-17";
                // $('#calendar').fullCalendar('renderEvent', {
                //     title: "34000",
                //     start: "2018-06-17",
                //     // allDay: true,
                //     backgroundColor:"#ff484a",
                //     resourceEditable: false,
                //
                // });
                // initialCalendar()
                // $("#calendar").fullCalendar('removeEvents', 999);
            },
            select: function(startDate, endDate, jsEvent, view, resource) {
                // alert('selected ' + startDate.format() + ' to ' + endDate.format() + ' on resource ' );
            }
        });
    }




    function convertToEn(str) {
        var fa_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        var en_numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];


        fa_numbers.forEach(function (fa_num, key) {
            str = str.replace(new RegExp(fa_num, 'g'), en_numbers[key]);
        });

        return str;
    }

    function initialCalendar(){
        if(!events.length) {

            last_day = moment(now, 'YYYY-MM-D').add(4, 'month').format('YYYY-MM-D');
            console.log(now);
            console.log(last_day);

            last_day = convertToEn(last_day);
            date = now;
            // date = moment(now, 'YYYY-MM-D').add(1, 'day').format('YYYY-MM-D');
            date = convertToEn(date);

            while(date <= last_day){

                var day_weekend = moment(date).day();
//                    events.push({
//                            title: 'قیمت:' ,
//                            start: date,
//                            active:1,
//                            reserve:1,
//                            price:0,
//                            day_weekend : day_weekend,
//                            backgroundColor:"#337ab7",
//                            resourceEditable: false,
//                        }
//                    )
                date = moment(date, 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD');
                date = convertToEn(date);
            }


        } else {
//                events.forEach(function (event,key) {
//                    if(event.reserve){
//                        event.title = "قیمت :"+ event.price +" رزرو شده";
//                        event.backgroundColor="#15b70f";
//                    }
//
//                });
        }


        // setFullCalendar();
        $('input[name=calendar]').val(JSON.stringify(events));


    }

    function addCompare(id) {
        $.ajax({
            url: '{{route('front.estate.addCompare')}}',
            data: {
                id: id
            },
            method: 'GET',
            success: function(result) {
                if(result.status == "added") {
                    $("#compareText").html('به لیست اضافه شد');
                }
                if(result.status == "full") {
                    $("#compareText").html('لیست شما پرشده است');
                }
                if(result.status == "already_exists") {
                    $("#compareText").html('قبلا در لیست وجود داشته');
                }
                $("#compareCount").text(result.count);
            },
            error: function() {
                alert("مشکلی در ارسال اطلاعات به وجود آمده است!");
            }
        })
    }

    $("#commentForm").validate({
        submitHandler: function(form) {
            $("#sendCommentBtn").html('درحال ارسال ...');
            $.ajax({
                url: $(form).attr('action'),
                data: $(form).serialize(),
                method: 'POST',
                success: function(result) {
                    if(result.status == 'success') {
                        $("#commentForm").fadeOut(1500);
                        $("#successSend").fadeIn(1500);
                    }
                },
                error: function() {
                    alert("مشکلی در ارسال اطلاعات به وجود آمده است!");
                }
            }).always(function() {
                $("#sendCommentBtn").html('ارسال نظر');
            })
            return false;
        }
    });

    $('.datepicker').persianDatepicker({
        responsive: true,
        initialValue: false,
        observer: true,
        format: 'YYYY/MM/DD',
        autoClose: true
    });

    $("#gallery").owlCarousel({
        center: true,
        items: 3,
        loop: true,
        margin: 0,
        nav:true,
        dots:false,
//            responsive: {
//                0: {
//                    items: 1
//                },
//                600: {
//                    items: 3
//                },
//                1000: {
//                    items: 3
//                }}
    });

    $("#trip").owlCarousel({
        center: true,
        items: 3,
        loop: true,
        margin: 0,
        nav:true,
        dots:false,
//            responsive: {
//                0: {
//                    items: 1
//                },
//                600: {
//                    items: 3
//                },
//                1000: {
//                    items: 3
//                }}
    });

    @if(!is_null($estate->lat))
    function estateMap() {
        var myCenter = new google.maps.LatLng({{$estate->lat}},{{$estate->lng}});
        var mapCanvas = document.getElementById("map");
        var mapOptions = {center: myCenter, zoom: 16};
        var map = new google.maps.Map(mapCanvas, mapOptions);
        var marker = new google.maps.Marker({position:myCenter});
        marker.setMap(map);
    }
    @endif
</script>
@endpush