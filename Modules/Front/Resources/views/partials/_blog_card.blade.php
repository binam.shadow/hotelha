<a href="{{route('front.blog.show', ['blog' => $blog])}}">
    <div class="item p-2 m-3">
        <div class="img-reviwe">
            <img src="{{ asset('uploads/' . $blog->image)}}" class="img-reponsive blog-img mb-2">
            {{--<span class="p-2">پیشنهاد ویژه</span>--}}
        </div>
        <div class="title-reviwe text-right">
            <h4> {{$blog->name}}</h4>
        </div>
        <div class="desc-reviwe text-right" dir="rtl">
            <p>{{ str_limit(strip_tags($blog->description), 80) }}</p>
        </div>
        <div class="comment m-4 text-right">
            <span class="pr-4 pl-4 pt-2 pb-2">{{$blog->likes_count}} <i class="far fa-heart"></i></span>
        </div>
    </div>
</a>