<div class="infinite-scroll">
    @foreach($estates as $estate)
        <a href="{{route('front.estate.show', [
    'estate' => $estate
])}}">
            @include('front::partials._estate_card', ['col_lg' => 'col-lg-4'])
        </a>
    @endforeach
    @if(!$estates->count())
        <h2 class="text-center">ملکی با مقادیر وارد شده یافت نشد</h2>
    @endif
    {{ $estates->appends(request()->query())->links() }}
</div>