<a href="{{route('front.estate.show', [
    'estate' => $estate
])}}">
    <div class="col-sm-6 col-md-4 @if(isset($col_lg)) col-lg-4 @else col-lg-3 @endif estate-box mb-5">
        <div class="item-hotel">
            <div class="img-hotel p-2">
                <img src="{{$estate->HotelPictures0Url}}" style="width: 100%" class="img-responsive">
                <span class="pr-5 pl-5 pt-2 pb-2">{{$estate->Name}}</span>
            </div>
            <div class="clearfix title-hotel m-3">
                <span class="hotel-label hotel-location-label"><i class="fa fa-map-marker"></i> {{$estate->Name}}</span>
                @if($estate->id == -1)
                    <span class="price">توافقی</span>
                @else
                    <span class="price"><b>{{$estate->HotelTypeCode}}</b>  ستاره</span>
                @endif
            </div>
            <hr/>
            <div class="desc-hotel">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center pt-2">
                        <span>تعداد اتاق</span><br>
                        <img class="meter-icon" src="{{asset('front/img/team-purple.svg')}}" alt="تعداد اتاق">
                        <p>{{$estate->NumberOfRooms}}</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center pt-2">
                        <span>ظرفیت</span><br>
                        <img class="meter-icon" src="{{asset('front/img/team-purple.svg')}}" alt="ظرفیت">
                        <p>{{$estate->NumberOfBeds}}</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center pt-2">
                        <span>تعداد طبقه</span><br>
                        <img class="meter-icon" src="{{asset('front/img/house-size-purple.svg')}}" alt="تعداد طبقه">
                        <p>{{$estate->NumberOfFloors}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>