<header class="@if(!isset($header_type)) top-header @else main-header mb-5 @endif p-3">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				 <a href="{{route('front.index')}}">
                    <img src="{{ asset('front/img/alovila.png') }}" class="img-reponsive pull-left mb-2" width="40" height="40">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse padd-nav" id="bs-example-navbar-collapse">
                <a href="{{route('front.index')}}">
                    <img src="{{ asset('front/img/alovila.png') }}" class="img-reponsive right hidden-sm hidden-xs" width="50" height="50">
                </a>
                <ul class="nav navbar-nav">
                    <li><a href="{{route('front.index')}}">صفحه اصلی</a></li>
                    {{--@auth--}}
                    {{--<li><a href="{{route('dashboard.ad.create')}}">ثبت رایگان آگهی</a></li>--}}
                    {{--@else--}}
                    {{--<li><a href="{{route('front.login')}}">ثبت رایگان آگهی</a></li>--}}
                    {{--@endauth--}}
                    <li><a href="#">درباره ما</a></li>
                    <li><a href="#">تماس با ما</a></li>
                    <li><a href="#">چرا هتلی ها؟؟؟</a></li>
                    <li><a href="#">شعب</a></li>
                </ul>
                <div class="pull-left">
                    @guest
					<a class="btn icon-btn btn-purple" href="{{route('front.signup')}}"><span class="img-circle text-primary"></span>ثبت نام</a>&nbsp;
					<!-- <span class="font-18">قبلا ثبت نام نکرده اید؟</span>&nbsp;&nbsp; --> <a class="font-18" href="{{route('front.login')}}">ورود </a>
                    @else
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a class="btn icon-btn btn-purple ml-4 mr-5" href="#"><span class="img-circle text-primary"></span>پنل کاربری</a>
                        </li>
                        <li>
                            <a class="mr-2" href="#">خروج</a>
                        </li>
                    </ul>
                    @endguest
                </div>

            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</header>