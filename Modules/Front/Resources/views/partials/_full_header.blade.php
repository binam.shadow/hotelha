<div class="image-bg" style="background-image: url('https://alaedin.travel/Files/Original/Hotels/tehran/persia/Alaedin-Travel-Agency-Tehran-Persia-Hotel-Lobby-7.jpg') !important;" >
    @include('front::partials._nav_menu')
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-banner">
                <div class="text-banner">
                    <h1>ویلا و آپارتمان، به صورت <b> روزانه</b> اجاره کن</h1>
                </div>
                <div class="search mt-5 p-2 col-md-8 col-center home-search-box">



                    <form id="SymbolCreate" method="post" action="{{ route('reserve.store') }}" align="center">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group form-float city-search">
                                    <label class="form-label">جستجو بر اساس نام هتل یا شهر</label>
                                    <br>
                                    <select class="chosen" name="hotelCode">
                                                                    <option value="0" selected>جستجو</option>
                                                                    @foreach($hhs as $hotel)
                                                                        <option value="{{ $hotel->Code }}">{{ $hotel->Name }}</option>
                                                                    @endforeach
                                                                </select>
                                </div>
                            </div>
                        </div>
                        
                        {{--<input type="text" class="normal-example" />--}}




                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label col-md-12">تاریخ رفت <span class="col-red">*</span></label>
                                        <div class="col-xs-12">
                                            <input class="form-control col-md-12 normal-example" name="startDate" value="{{ old('startDate') }}" required="" aria-required="true"
                                                type="text">
                                        </div>
                                
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">تاریخ برگشت <span class="col-red">*</span></label>
                                        <div class="col-xs-12">
                                            <input class="form-control normal-example" name="endDate" value="{{ old('endDate') }}" required="" aria-required="true"
                                                type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        



                        {{--<div class="form-group form-float">--}}
                            {{--<div class="form-line">--}}
                                {{--<input class="form-control normal-example" name="endDate" value="{{ old('endDate') }}" required="" aria-required="true" type="text">--}}
                                {{--<label class="form-label">تاریخ برگشت <span class="col-red">*</span></label>--}}
                            {{--</div>--}}
                        {{--</div>--}}<br>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label col-md-12">تعداد نفرات <span class="col-red">*</span></label>
                                        <div class="col-md-12">
                                            <select class="selectric" name="count">
                                                <option value="0" selected>انتخاب تعداد نفرات</option>
                                                <option value="1">1</option>
                                                <option value="1">2</option>
                                                <option value="1">3</option>
                                                <option value="1">4</option>
                                                <option value="1">5</option>
                                                <option value="1">6</option>
                                                <option value="1">7</option>
                                                <option value="1">8</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <label class="form-label col-md-12">تعداد اتاق <span class="col-red">*</span></label>
                                <div class="col-md-12">
                                    <select class="selectric" name="count">
                                        <option value="0" selected>انتخاب تعداد اتاق</option>
                                        <option value="1">1</option>
                                        <option value="1">2</option>
                                        <option value="1">3</option>
                                        <option value="1">4</option>
                                        <option value="1">5</option>
                                        <option value="1">6</option>
                                        <option value="1">7</option>
                                        <option value="1">8</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group form-float mb-0">
                            <button type="submit" class="btn btn-info btn-block btn-show-all py-3">جستجو</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>