@extends('front::layouts.master')

@section('title')
 لیست تمام آگهی ها
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('front/css/bootstrap-slider.css')}}">
    <link rel="stylesheet" href="{{ asset('front/plugins/simplebar/dist/simplebar.css') }}">
@endsection

@section('content')
    @include('front::partials._nav_menu')
    {{--<div class="sidebar" style="margin-right: -240px;">--}}
        {{--<div class="title-sidebar">--}}
            {{--<a id="navbar-toggle"><i class="fas fa-arrow-right menu-icon" aria-hidden="true"></i>جستجوی پیشرفته الوویلا</a>--}}
        {{--</div>--}}
        {{--<br>--}}
        {{--<div class="content-sidebar pr-4" style="display: none;">--}}
            {{----}}
        {{--</div>--}}
    {{--</div>--}}

    <section class="pt-5 bg-gray">
	<div class="container-fluid">
        <div class="row" data-sticky_parent>
            <div class="col-xs-12 col-md-3">
                <div class="mr-1" id="sidebar" style="background: #FFF;box-shadow: 0 2px 4px rgba(0,0,0,0.1);" data-sticky_column >
                    <p class="pt-1 pr-3 pl-3">ویژگی های اقامتگاه موردنظر خود را دراین قسمت تنظیم کنید، و سپس روی
                        <button id="searchBtn" class="btn btn-search pr-4 pl-4 mr-2 ml-2"><span class="fa fa-search"></span> جستجو</button>لمس کنید
                    </p>
                    <hr>
                    <form id="searchForm" class="mb-4">
                        <h4 class="mt-4 pr-3 pl-3"> محدودیت قیمت اجاره روزانه اقامتگاه</h4><br>
                        <div class="range p-4 text-right mb-4 mr-3 ml-3 " dir="rtl">
                            <input name="min_price" value="50000" type="hidden">
                            <input name="max_price" value="3000000" type="hidden">
                            <input class="flat-slider" id="priceSlider"
                                   data-slider-min="50000"
                                   data-slider-max="3000000"
                                   data-slider-value="[50000,3000000]"
                                   data-slider-step="10000"
                                   data-slider-tooltip="hide">
                            <h5> از <b id="minPriceLabel">50,000</b> الی <b id="maxPriceLabel">3,000,000</b> تومان</h5>
                        </div>
                        <br/>
                        <div class="form-group mr-3 ml-3">
                            <select name="province" class="form-control nav choose-city">
                                <option value="0" selected>همه استان ها</option>
                                @foreach($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                @endforeach
                            </select>
                            <!-- <span class="input-group-addon">
                                                <i class="fa fa-map-marker"></i>
                                            </span> -->
                            <br/>
                            <select name="city" class="form-control nav choose-city">
                                <option value="0">همه شهرها</option>
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                            <!-- <span class="input-group-addon">
                                                <i class="fa fa-map-marker"></i>
                                            </span> -->
                        </div>
                        <hr/>
                        <h4 class="mt-3 pr-3 pl-3"> محدوده متراژ اقامتگاه </h4><br/>
                        <div class="range p-4 mb-4 mr-3 ml-3 text-right c-range">
                            <input name="min_size" value="30" type="hidden">
                            <input name="max_size" value="6000" type="hidden">
                            <input class="flat-slider" id="sizeSlider"
                                   data-slider-min="30"
                                   data-slider-max="6000"
                                   data-slider-value="[90,6000]"
                                   data-slider-step="10"
                                   data-slider-tooltip="hide">
                            <h5> از <b id="minSizeLabel">30</b> الی <b id="maxSizeLabel">6000</b> مترمربع</h5>
                        </div>
                        <hr/>
                        <h4 class="mt-3 pr-3 pl-3"> ظرفیت </h4><br/>
                        <div class="range p-4 mb-4 mr-3 ml-3 text-right c-range">
                            <input name="min_capacity" value="1" type="hidden">
                            <input name="max_capacity" value="24" type="hidden">
                            <input class="flat-slider" id="capacitySlider"
                                   data-slider-min="1"
                                   data-slider-max="24"
                                   data-slider-value="[1,24]"
                                   data-slider-step="1"
                                   data-slider-tooltip="hide">
                            <h5> از <b id="minCapacityLabel">1</b> الی <b id="maxCapacityLabel">24</b> نفر </h5>
                        </div>
                        <hr/>
                        <h4 class="mt-3 pr-3 pl-3"> تعداد اتاق</h4><br>
                        <div class="range p-4 mb-4 mr-3 ml-3 text-right c-range">
                            <input name="room" type="hidden">
                            <input class="flat-slider" id="roomSlider"
                                   data-slider-min="1"
                                   data-slider-max="6"
                                   data-slider-value="1"
                                   data-slider-step="1"
                                   data-slider-tooltip="hide">
                            <h5> <b id="roomLabel">1</b> اتاق</h5>
                        </div>
                        <hr/>
                        <div class="table-facility mt-4">
                            <div class="form-group form-float">
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="apartment[]" value="apartment" type="checkbox"><span class="lever switch-col-blue"></span>آپارتمان</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="apartment[]" value="villa" type="checkbox"><span class="lever switch-col-blue"></span>ویلا</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="apartment[]" value="suite" type="checkbox"><span class="lever switch-col-blue"></span>سوئیت</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 px-0">
                                    <div class="switch">
                                        <label><input name="apartment[]" value="rural" type="checkbox"><span class="lever switch-col-blue"></span>خانه روستایی</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix mt-1 mb-4">
                                <hr>
                            </div>
                            <div class="form-group form-float">
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="forest" type="checkbox"><span class="lever switch-col-blue"></span>جنگلی                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="jac" type="checkbox"><span class="lever switch-col-blue"></span>جکوزی                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="furnished" type="checkbox"><span class="lever switch-col-blue"></span>مبله                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="shore" type="checkbox"><span class="lever switch-col-blue"></span>ساحلی                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="pool" type="checkbox"><span class="lever switch-col-blue"></span>استخر                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="exclusive" type="checkbox"><span class="lever switch-col-blue"></span>دربست                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="parking" type="checkbox"><span class="lever switch-col-blue"></span>پارکینگ                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="switch">
                                        <label><input name="elevator" type="checkbox"><span class="lever switch-col-blue"></span>آسانسور                                    </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-4 mb-5">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-9 col-xs-12">
                <div id="dynamicContent">
                    <div class="infinite-scroll">
                        @foreach($estates as $estate)
                            @include('front::partials._estate_card', ['col_lg' => 'col-lg-4'])
                        @endforeach
                        {{ $estates->appends(request()->except(['page','_token']))->links() }}
                    </div>
                </div>
            </div>
        </div> </div>
    </section>
@endsection


@section('scripts')
    <script src="{{ asset('front/js/bootstrap-slider.js') }}"></script>
    <script src="{{ asset('front/js/jquery.jscroll.min.js') }}"></script>
    <script src="{{ asset('front/js/jquery.sticky-kit.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/block-ui/block-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('front/simplebar/dist/simplebar.js') }}"></script>
    <script>

//        $("[data-sticky_column]").stick_in_parent({
//            parent: "[data-sticky_parent]",
//            spacer: false
//        });

//        $(window).scroll(function(){
//            console.log($(this).scrollTop())
//            $("#sidebar").css("top",Math.max(0,450-$(this).scrollTop()));
//        });

        var priceSlider = $('#priceSlider').slider({});
        $('#priceSlider').on('slide', function(data_range) {
            $("#minPriceLabel").html(numberFormat(data_range.value[0]));
            $("input[name=min_price]").val(data_range.value[0]);

            $("#maxPriceLabel").html(numberFormat(data_range.value[1]));
            $("input[name=max_price]").val(data_range.value[1]);
        });


        $('#sizeSlider').slider({});
        $('#sizeSlider').on('slide', function(data_range) {
            $("#minSizeLabel").html(numberFormat(data_range.value[0]));
            $("input[name=min_size]").val(data_range.value[0]);

            $("#maxSizeLabel").html(numberFormat(data_range.value[1]));
            $("input[name=max_size]").val(data_range.value[1]);

        });

        $('#capacitySlider').slider({});
        $('#capacitySlider').on('slide', function(data_range) {
            $("#minCapacityLabel").html(data_range.value[0]);
            $("input[name=min_cpacity]").val(data_range.value[0]);

            $("#maxCapacityLabel").html(data_range.value[1]);
            $("input[name=max_cpacity]").val(data_range.value[1]);

        });

        $('#roomSlider').slider({});
        $('#roomSlider').on('slide', function(data) {
            var label = data.value;
            if(data.value == 6)
                label = 'بیشتر از 5';

            $("input[name=room]").val(data.value);
            $("#roomLabel").html(label);
        });

        $('#searchBtn').on('click', function() {
            $.blockUI({message: '<h1>لطفا صبر کنید...</h1>'});
            $.ajax({
                url: "{{route('front.estate.search')}}",
                data: $("#searchForm").serialize(),
                success: function(result) {
                    $('#dynamicContent').html(result);
                    initJscroll()
                    $('.sidebar').css('margin-right', '-240px');
                    $('.content-sidebar').hide();
                    state = "minimized";
                }
            })
            .always(function() {
                $.unblockUI()
            });
        });


        //
//        $('#flat-slider-distance').slider({
//            orientation: 'horizontal',
//            range:       true,
//            values:      [17,67]
//        });
//
//        $('#flat-slider-room').slider({
//            orientation: 'horizontal',
//            range:       false,
//            values:      [17,67]
//        });

//        $(document).scroll(function(){
//            var x= $(document).scrollTop();
//            var $defaultHeight= $('.top-header').outerHeight();
//            if(x< ($defaultHeight + 10))
//                $('.sidebar').removeClass("zero")
//            else
//                $('.sidebar').addClass("zero")
//        })

//
        function initJscroll() {
            $('ul.pagination').hide();
            $(function() {
                $('.infinite-scroll').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="/images/loading.gif" alt="Loading..." />',
                    padding: 0,
                    nextSelector: '.pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll',
                    callback: function() {

                        $("[data-sticky_column]").stick_in_parent({
                            parent: "[data-sticky_parent]",
                            offset_top: 10,
                            force_sticky:!0
                        })
//                        .on('sticky_kit:bottom', function(e) {
//                            $('#sidebar').css({
//                                'position': 'fixed',
//                                'top' : $(this).scrollTop(),
//                                'height': '100%',
//                                'overflow-y': 'scroll',
//                                'overflow-x': 'hidden'
//                            });
//                        });
//                        var simplebar = new SimpleBar($('#sidebar')[0])
                        $('ul.pagination').remove();
                    }
                });
            });
        }
        initJscroll()

        $("select[name=province]").on('change', function() {
            $.ajax({
                url: "{{route('front.getCities')}}",
                data: {
                    province_id: $(this).val()
                },
                success: function(result) {
                    var option = '<option value="0">همه شهرها</option>';
                    $.each(result, function(key, value) {
                        option += '<option value="'+ value.id +'">' + value.name + '</option>';
                    });
                    $("select[name=city]").html(option);
                }
            });
        });
    </script>
@endsection