@extends('front::layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{asset('front/css/persian-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/star-rating-svg.css')}}">
    <link href='{{ asset('backend/plugins/fullcalendar/dist/fullcalendar.css')}}' rel='stylesheet' />
    <style>
        .fc-event .fc-content {
            text-align: center;
        }
        .fc-event .fc-title {
            font-size: 13px;
        }
        .fc-past {
            background: #F5F5F5;
        }
        .fc-past .fc-title {
            display: none;
        }
        .fc-event {
            border-color: transparent !important;
        }
    </style>
@endsection

@section('head')
    <meta name="og:title" content="{{$estate->name}}">
    <meta name="og:site_name" content="الوویلا">
    <meta name="og:image" content="{{asset('front/img/alovila.png')}}">
    <meta name="og:description" content="{{substr(strip_tags($estate->description), 0, 201)}}">

@endsection

@section('title')
{{$estate->name}}
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <div class="mt-5 estate-gallery">
        <div class="owl-carousel owl-theme owl-loaded" id="gallery">
            <div class="item">
                <div class="estate-image">
                    <p class="text-center">
                        <img src="{{asset('uploads/'.$estate->image)}}" alt="{{$estate->name}}">
                    </p>
                </div>
            </div>
            @foreach($estate->gallery as $key => $gallery)
                <div class="item">
                    <div class="estate-image">
                        <p class="text-center">
                            <img src="{{asset('uploads/'.$gallery->image)}}" alt="{{$estate->name}}">
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <section>
        <div class="row clearfix"></div>
        <div class="container">
            <div class="row" >
                <div class="clearfix" data-sticky_parent>
                <div class="col-md-8" id="contentContainer">
                    <div class="pl-5 estate-detail-container">
                        <div class="title-villa">
                            <h1 class="mb-4">{{$estate->name}}</h1>
                        </div>
                        <div class="clearfix">
                            <div class="estate-code">
                                <span class="badge badge-code key">کدآگهی</span>
                                <span class="badge badge-code value">{{$estate->id}}</span>
                            </div>
                            <button @auth onclick="favorite()" @endauth @guest onclick="alert('ابتدا باید وارد شوید')" @endguest data-favorite-id="{{$estate->id}}" class="btn btn-white btn-favorite">
                                @if($estate->favorited)
                                <i class="fa fa-bookmark"></i> حذف از علاقه مندی ها
                                @else
                                <i class="fa fa-bookmark-o"></i> افزودن به علاقه مندی ها
                                @endif
                            </button>

                            @if($estate->rating_count)
                            
                            <div class="estate-rating-box"><div id="estateRating"></div><span id="ratingMsg"> امتیاز {{$estate->rating}} از 5 مجموع {{$estate->rating_count}} رای (تمام رای دهندگان از ملک استفاده کرده اند)</span></div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-6 col-sm-3 mb-4">
                                <div class="desc-villa mt-3">
                                    <span>تعداد اتاق</span>
                                    <img src="{{asset('front/img/bed-purple.svg')}}" alt="تعداد اتاق">
                                    <p>
                                        @if(!$estate->room)
                                            ندارد
                                        @elseif($estate->room < 6)
                                            {{$estate->room}} اتاق
                                        @else
                                           بیشتر از 5
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6 col-sm-3 mb-4">
                                <div class="desc-villa mt-3">
                                    <span>حداکثر ظرفیت</span>
                                    <img src="{{asset('front/img/team-purple.svg')}}" alt="حداکثر ظرفیت">
                                    <p>
                                        {{$estate->capacity}}  نفر
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6 col-sm-3 mb-4">
                                <div class="desc-villa mt-3">
                                    <span>متراژ</span>
                                    <img src="{{asset('front/img/house-size-purple.svg')}}" alt="متراژ">
                                    <p>{{$estate->size}} مترمربع</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6 col-sm-3 mb-4">
                                <div class="desc-villa mt-3">
                                    <span>مکان</span>
                                    <img src="{{asset('front/img/marker-purple.svg')}}" alt="مکان">
                                    <p>{{$estate->cityData->name}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <hr>
                    <div class="row clearfix"></div>
                    <div class="col-description">
                        <h4 class="mb-5">
                            <i class="fa fa-file-text-o"></i>
                    توضیح</h4>
                        {!! $estate->description !!}
                    </div>
                    <hr>
                    @if(count($estate->features))
                        <div class="title-villa">
                            <br/>
                            <h3 class="mb-4">امکانات و شرایط اقامتگاه </h3>
                        </div>
                        <div class="row">
                            @foreach($estate->features as $feature)
                                <div class="col-sm-2 col-xs-6 mb-1 pr-3">
                                    <div class="facility-villa">
                                        <img src="{{'http://alovilla.org/uploads/'.$feature->icon}}" width="30" height="30"><br/>
                                        {{$feature->name}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    @if(count($events))
                        <h4 class="mb-5">
                            <i class="fa fa-calendar"></i>
                            برنامه رزرو این اقامتگاه
                        </h4>
                        <div class="col-sm-12 col-xs-12 col-center">
                            <div id="calendar"></div>
                        </div>
                    @endif
                    <br/>
                        @if(!is_null($estate->lat))
                            <h3>مکان روی نقشه</h3>
                            <div id="map"></div>
                        @endif
                    <div class="clearfix"></div>
                    <p class="mt-2">
                        اشتراک آگهی :
                        <a href="whatsapp://send?text={{$estate->name}}%20%0A%20{{route('front.estate.show', ['estate' => $estate])}}"><i class="fa fa-whatsapp whatsapp-share"></i></a>
                        <a href="https://telegram.me/share/url?text={{$estate->name}}%20%0A%20{{route('front.estate.show', ['estate' => $estate])}}"><i class="fa fa-paper-plane telegram-share"></i></a>
                    </p>
                </div>
                <div class="col-md-4"><!--   id="sidebarContainer" -->
                    <div id="sidebar" class="mb-5" data-sticky_column>
                        <div class="compare">
                            <button onclick="addCompare({{$estate->id}})" class="pr-4 pl-4">
                                <i class="fas fa-plus"></i>
                                <span id="compareText" style="font-size: 13px"> افزودن به لیست مقایسه</span>
                            </button>
                            <a href="{{route('front.estate.compare')}}" class="pr-3 pl-3 relative">لیست مقایسه <span class="count"  id="compareCount">{{$compare_list->count()}}</span></a>
                        </div>
                        <div class="sidebar-villa mb-4">
                            <header class="per p-4 pt-5 pb-5 m-2">
                                <h4 class="inline">قیمت هر شب اقامت</h4>
                                <span class="left"><b>{{number_format($estate->price)}}</b>&nbsp;<mark> تومان</mark></span>
                            </header>
                            <form id="reserveForm" action="" class="resrv-box">
                                {{csrf_field()}}
                                <div class="pr-4 pl-4">
                                    <h4> تاریخ و مدت اقامت</h4>
                                    <div class="row form-inline">
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"> از </span>
                                                    <input type="text" autocomplete="off" placeholder="97/08/01" id="date" name="start_date" onchange="resetReserveBtn()" required class="form-control border-none text-right datepicker">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="form-group input-group">
                                                <span class="input-group-addon"> الی </span>
                                                <input type="text" autocomplete="off" placeholder="97/08/01" id="date" name="end_date" onchange="resetReserveBtn()" required class="form-control border-none text-right datepicker">
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="mt-4"> تعداد افراد</h4>
                                    <input type="text" name="number_of_persons" class="decs-code pr-4 mb-4" onkeyup="this.value=parseArabic(this.value)" placeholder="چند نفر هستید؟" step="1">
                                    <h4> کد تخفیف</h4>
                                    <input type="text" name="code" class="decs-code pr-4 mt-2" placeholder="کد را کاملا با حروف انگلیسی وارد کنید">
                                </div>

                                <div id="invoicePreview"  class="col-xs-12" style="display: none;">
                                    <table class="table borderless mt-5">
                                        <tr>
                                            <td>مبلغ کل : </td>
                                            <td>
                                                <span id="price"></span> تومان
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> تخفیف : </td>
                                            <td>
                                                <span id="discountMsg"></span>
                                            </td>
                                        </tr>
                                        <hr>
                                        <tr class="total-price">
                                            <td>مبلغ پرداختی شما : </td>
                                            <td>
                                                <span id="discountPrice"></span> تومان
                                            </td>
                                        </tr>
                                    </table>


                                    <div class="clearfix"></div>
                                    <p class="text-center">ثبت رزرو به منزله تایید
                                        <a class="col-blue" href="{{route('front.rules')}}" target="_blank">قوانین و مقررات</a>
                                        رزرو میباشد</p>
                                </div>
                                @auth
                                    @if(is_null(Auth::user()->national_code))
                                        <br>
                                        <p class="text-center">برای ثبت رزرو ابتدا باید کد ملی خود را ثبت نمایید</p>
                                        <button type="button" onclick="window.location='{{route('dashboard.profile')}}'" id="reserveBtn" class="submit mt-5">ویرایش پروفایل</button>
                                    @else
                                        <button type="submit" id="reserveBtn" class="submit mt-5">درخواست رزرو این اقامتگاه</button>
                                    @endif
                                @else
                                <button type="button" onclick="window.location='{{route('front.login')}}'" id="reserveBtn" class="submit mt-5">درخواست رزرو این اقامتگاه</button>
                                @endauth
                            </form>
                        </div>

                        <div class="pro-advertise p-4 mb-4">
                            <div class="pro-img">
                                {{--<img src="img/avatar.jpg" class="img-responsive ml-4">--}}
                            </div>
                            <table class="table borderless mb-0">
                                <tr>
                                    <td>مالک/نماینده ی مالک</td>
                                    <td><h4 class="mt-0"><b>{{$estate->user->name}}</b></h4></td>
                                </tr>
                            </table>
                        </div>
                        <div class="gurante p-4">
                            <div class="row">
                                <div class="col-sm-6 text-center">
                                    <div class="gurante-img">
                                        <img src="{{asset('front/img/refund.svg')}}" class="img-responsive">
                                    </div>
                                    <h6><b>گارانتی بازگشت وجه الوویلا</b></h6>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <div class="gurante-img">
                                        <img src="{{asset('front/img/safe-payment.svg')}}" class="img-responsive">
                                    </div>
                                    <h6><b>پرداخت امن</b></h6>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </div>
                @if($other_estates->count())
                <div class="col-md-12">
                    <mark class="mt-5 pr-4 pl-4 mr-4">اقامتگاه های مشابه</mark><br/>
                    <div class="owl-carousel owl-theme more-estate-carousel" id="trip">
                        @foreach($other_estates as $other_estate)
                            <div class="item">
                                @include('front::partials._estate_card', [
                                    'estate' => $other_estate
                                ])
                            </div>
                        @endforeach
                    </div>
                </div>
                @endif

                <div class="col-md-12">
                    <div class="pr-5 pl-5 pt-2 pb-2 mt-5 mb-5 marke-divider city"> نظرات ثبت شده</div>
                    @foreach($comments as $comment)
                        <div class="row comment-box">
                            <div class="col-md-12 mb-4 text-center">
                                <h4 class="text-right"><i class="fa fa-user comment-avatar"></i> {{$comment->user->name}} <i class="fa fa-angle-left"></i> <span> {{\jDate::forge($comment->created_at)->format('%A | %d %B %Y')}}</span></h4>
                                <p class="text-right">{{$comment->description}}</p>
                            </div>
                            {{--<div class="col-md-8 col-md-offset-1 mb-4 text-justify">--}}

                            {{--</div>--}}
                        </div>
                    @endforeach
                    <div class="row clearfix mt-5"></div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('front/js/persian-date.js') }}"></script>
    <script src="{{ asset('front/js/persian-datepicker.js') }}"></script>
    @if(count($events))
    <script src='{{ asset('backend/plugins/fullcalendar/lib/moment/moment.js')}}'></script>
    <script src='{{ asset('backend/plugins/fullcalendar/lib/moment/moment-jalaali.js')}}'></script>
    <script src='{{ asset('backend/plugins/fullcalendar/lib/jquery-ui/jquery-ui.js')}}'></script>
    <script src='{{ asset('backend/plugins/fullcalendar/dist/fullcalendar.js')}}'></script>
    <script src='{{ asset('backend/plugins/fullcalendar/lang/fa.js')}}'></script>
    @endif
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
    <script type="text/javascript" src="{{ asset('front/js/jquery.star-rating-svg.js') }}"></script>
    <script>
        function resetReserveBtn() {
            ("#reserveBtn").removeAttr('onclick');
        }

        $(document).ready(function() {
            $('#sidebarContainer').height($('#contentContainer').outerHeight());
        });

        var reset_scroll;
        var left_offset = $('#sidebar').offset().left;

        $.extend({
            redirectPost: function(location, args)  {
                var form = '';
                $.each( args, function( key, value ) {

                    form += '<input type="hidden" name="'+value.name+'" value="'+value.value+'">';
                    form += '<input type="hidden" name="'+key+'" value="'+value.value+'">';
                });
                $('<form action="'+location+'" method="POST">'+form+'</form>').appendTo('body').submit();
            }
        });

        function submitReserve() {
            $.redirectPost("{{route('front.checkReserve', ['estate' => $estate->id])}}", $("#reserveForm").serializeArray());
        }

        function parseArabic(str) {
            if(str == '') return '';
            var out_num = Number( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
                return d.charCodeAt(0) - 1632; // Convert Arabic numbers
            }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
                return d.charCodeAt(0) - 1776; // Convert Persian numbers
            }) );
            return isNaN(out_num) ? 0 : out_num;
        }

        $("#reserveForm").validate({
            errorClass: "error",
            validClass: "valid-class",
            errorElement: 'div',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            onError : function(){
                $('.input-group.error-class').find('.help-block.form-error').each(function() {
                    $(this).closest('.form-group').addClass('error').append($(this));
                });
            },
            rules: {
                number_of_persons: {
                    required: true,
                    @if($estate->capacity && $estate->capacity < 6)
                    max: {{$estate->capacity}}
                    @endif
                }
            },
            submitHandler: function(form) {
                $('#invoicePreview').slideUp();
                $("#reserveBtn").text('درحال بررسی ...');
                $.ajax({
                    url: '{{route('front.checkReserve', ['estate' => $estate->id])}}',
                    data: $(form).serialize(),
                    method: 'POST',
                    success: function(result) {
                        if(result.error == 1) {
                            $("#reserveBtn").html(result.msg);
                        } else {
                            $('#price').html(numberFormat(result.price));
                            $('#discountPrice').html(numberFormat(result.discount_price));
                            $('#discountMsg').html(numberFormat(result.discount_msg));
                            $("#reserveBtn").html('پرداخت فاکتور و ثبت رزرو');
                            $("#reserveBtn").attr('onclick', 'submitReserve()' );
                            $('#invoicePreview').slideDown();
                        }
                    }
                })
                .error(function() {
                    $("#reserveBtn").html('درخواست رزرو این اقامتگاه');
                });
            }
        });


        @if(count($events))
        var now = "{{$now}}";

        <?php
        echo 'var events = '.json_encode($events);
        ?>

        // if(!events.length) {
        //     console.log(events);
        //     initialCalendar();
        // }

        initialCalendar();
        setFullCalendar();

        {{--console.log("{{$events}}");--}}
        console.log(now)
        function setFullCalendar() {
            $('#calendar').fullCalendar({
                lang: 'fa',
                isJalaali: true,
                selectable: false,
                isRTL: true,
                defaultDate: now,

                eventLimit: true, // allow "more" link when too many events
                events: events,
                droppable: false,
                editable: false,

                allDays:false,

                dayClick: function(date, jsEvent, view, resource) {

                    data = date.format();
                    var selected_date = convertToEn(date.format());

                    $("input[name=selected_date]").val(selected_date);
                    selected_event = events.filter(e => e.start == selected_date);
                    if(selected_event.length){
                        $("input[name=date_price]").val(selected_event[0].price)
                        $("input[name=active_date]").prop("checked",selected_event[0].active)
                    }
                    // $('#calendar').fullCalendar( 'removeEventSource', events );
                    // // initialCalendar();
                    // $('#calendar').fullCalendar( 'addEventSource', events );
                    // date="2018-01-17";
                    // $('#calendar').fullCalendar('renderEvent', {
                    //     title: "34000",
                    //     start: "2018-06-17",
                    //     // allDay: true,
                    //     backgroundColor:"#ff484a",
                    //     resourceEditable: false,
                    //
                    // });
                    // initialCalendar()
                    // $("#calendar").fullCalendar('removeEvents', 999);
                },
                select: function(startDate, endDate, jsEvent, view, resource) {
                    // alert('selected ' + startDate.format() + ' to ' + endDate.format() + ' on resource ' );
                }
            });
        }




        function convertToEn(str) {
            var fa_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            var en_numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];


            fa_numbers.forEach(function (fa_num, key) {
                str = str.replace(new RegExp(fa_num, 'g'), en_numbers[key]);
            });

            return str;
        }

        function initialCalendar(){
            if(!events.length) {

                last_day = moment(now, 'YYYY-MM-D').add(4, 'month').format('YYYY-MM-D');

                last_day = convertToEn(last_day);
                date = now;
                // date = moment(now, 'YYYY-MM-D').add(1, 'day').format('YYYY-MM-D');
                date = convertToEn(date);

                while(date <= last_day){

                    var day_weekend = moment(date).day();
                    events.push({
                            title: 'قیمت:' ,
                            start: date,
                            active:1,
                            reserve:1,
                            price:0,
                            day_weekend : day_weekend,
                            backgroundColor:"#337ab7",
                            resourceEditable: false,
                        }
                    )
                    date = moment(date, 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD');
                    date = convertToEn(date);
                }


            } else {
                events.forEach(function (event,key) {
                    event.title = "قیمت :" + event.price;
                    event.resourceEditable = false;

                    if(event.active == "1") {
                        if (event.reserve == "1") {
                            event.title = " رزرو شده";
                            event.backgroundColor = "#939393 !important";
                        }
                    } else {
                        event.title = "غیر فعال";
                        event.backgroundColor="#ff484a !important"
                    }

                });
            }


            // setFullCalendar();
            $('input[name=calendar]').val(JSON.stringify(events));
        }
        @endif

        function addCompare(id) {
            $.ajax({
                url: '{{route('front.estate.addCompare')}}',
                data: {
                    id: id
                },
                method: 'GET',
                success: function(result) {
                    if(result.status == "added") {
                        $("#compareText").html('به لیست اضافه شد');
                    }
                    if(result.status == "full") {
                        $("#compareText").html('لیست شما پرشده است');
                    }
                    if(result.status == "already_exists") {
                        $("#compareText").html('قبلا در لیست وجود داشته');
                    }
                    $("#compareCount").text(result.count);
                },
                error: function() {
                    alert("مشکلی در ارسال اطلاعات به وجود آمده است!");
                }
            })
        }

        $('.datepicker').persianDatepicker({
            responsive: true,
            initialValue: false,
            observer: true,
            format: 'YYYY/MM/DD',
            autoClose: true
        });

        $("#gallery").owlCarousel({
            center: true,
            items: 3,
            loop: true,
            margin: 0,
            nav:true,
            dots:false,
            lazyLoad:true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }}
        });

        $("#trip").owlCarousel({
//            center: true,
            items: 3,
            loop: true,
            margin: 0,
            nav:true,
            dots:false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }}
        });


        @if(!is_null($estate->lat))
            function estateMap() {
                var myCenter = new google.maps.LatLng({{$estate->lat}},{{$estate->lng}});
                var mapCanvas = document.getElementById("map");
                var mapOptions = {center: myCenter, zoom: 16};
                var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({position:myCenter});
                marker.setMap(map);
            }
        @endif


//        $("[data-sticky_column]").stick_in_parent({
//            parent: "[data-sticky_parent]",
//            spacer: false,
//        })

        $("#estateRating").starRating({
            initialRating: {{$estate->rating}},
            strokeWidth: 10,
            starSize: 20,
            emptyColor: 'lightgray',
            hoverColor: '#7b44ee',
            activeColor: '#7b44ee',
            useGradient: false,
            starShape: 'rounded',
            ratedColor: '#7b44ee',
            useFullStars: true,
            callback: function(currentRating, $el){
                @auth
                $.ajax({
                    url: "{{route('front.estate.rate')}}",
                    data: {
                        estate_id: {{$estate->id}},
                        rate: currentRating
                    },
                    method: 'POST',
                    success: function(result) {
                        $('#ratingMsg').html(result.msg);
                    },
                    error: function() {
                        alert('مشکلی به وجود آمده است، لطفا دوباره امتحان کنید');
                    }
                });
                @else
                    alert('برای امتیاز دادن به این اقامتگاه باید وارد شوید');
                @endauth
            }
        });


        function favorite() {
            $.ajax({
                url: '{{route('front.estate.favorite')}}',
                type: 'POST',
                data: {
                    estate_id: {{$estate->id}}
                },

            })
            .done(function(result) {
                if(result.status == 'favorited') {
                    $("[data-favorite-id={{$estate->id}}]").html('<i class="fa fa-bookmark"></i> حذف از علاقه مندی ها');
                } else {
                    $("[data-favorite-id={{$estate->id}}]").html('<i class="fa fa-bookmark-o"></i> افزودن به علاقه مندی ها');
                }
            })
            .fail(function() {
                alert('مشکلی در ارسال اطلاعات به وجود آمده است.');
            });

        }
    </script>
    @if(!is_null($estate->lat))
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-IgCJVNvcbIOEaC7oQduowDaG3aJzlnM&callback=estateMap"></script>
    @endif
@endsection