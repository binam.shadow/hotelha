@extends('front::layouts.master')

@section('title')
    لیست مقایسه آگهی ها
@endsection

@section('content')
    @include('front::partials._nav_menu')

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @if($estates->count())
                <table class="table table-bordered compare-table mt-5">
                    <tr>
                        <td>تصویر</td>
                        @foreach($estates as $estate)
                            <td class="text-center">
                                <p class="text-left">
                                    <a class="delete-compare" href="{{route('front.estate.removeCompare', [
                                        'id' => $estate->id
                                    ])}}">حذف از لیست</a>
                                </p>
                                <img width="200px" src="{{'http://alovilla.org/uploads/'.$estate->image}}" alt="">
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>عنوان</td>
                        @foreach($estates as $estate)
                            <td>
                                <a class="col-blue" href="{{route('front.estate.show', ['estate' => $estate])}}">{{$estate->name}}</a>
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>قیمت </td>
                        @foreach($estates as $estate)
                            <td>
                                {{number_format($estate->price)}} تومان 
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>تعداد اتاق</td>
                        @foreach($estates as $estate)
                            <td>
                                {{number_format($estate->room)}}
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>حداکثر ظرفیت</td>
                        @foreach($estates as $estate)
                            <td>
                                {{$estate->capacity}} نفر
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>متراژ</td>
                        @foreach($estates as $estate)
                            <td>
                                {{$estate->size}} مترمربع
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>مکان</td>
                        @foreach($estates as $estate)
                            <td>
                                {{$estate->cityData->name}}
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>توضیحات</td>
                        @foreach($estates as $estate)
                            <td>
                                {!! $estate->description !!}
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>امکانات</td>
                        @foreach($estates as $estate)
                            <td>
                                <ul>
                                @foreach($estate->features as $feature)
                                    <li>{{$feature->name}}</li>
                                    {{--<div class="col-xs-2 mb-1 pr-0">--}}
                                        {{--<div class="facility-villa">--}}
{{--                                            <img src="{{'http://alovilla.org/uploads/'.$feature->icon}}" width="30" height="30"><br/>--}}
                                            {{--{{$feature->name}}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                @endforeach
                                </ul>
                            </td>
                        @endforeach
                    </tr>
                </table>
                @else

                    <div class="my-5 ">
                        <div class="clearfix mt-5"></div>
                        <h2 class="text-center">لیست مقایسه شما خالی است</h2>
                        <p class="text-center">از طریق
                            <a class="delete-compare" href="{{route('front.estate.list')}}">     این لینک </a>
                            میتوانید به مشاهده اقامتگاه های سایت بپردازید</p>
                        <div class="clearfix mb-5"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection