@extends('front::layouts.master')

@section('title')
  قوانین
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="rule">
        <div class="container">
            <div class="row p-4">
                <div class="col-md-12">
                    <h4>شرایط و قوانین الوویلا</h4>
                    <p>شرایط و قوانین الوویلا به شرح زیر میباشد:</p>
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                @foreach($rules as $key => $rule)
                                    <li @if(!$key) class="active" @endif><a href="#tab1default{{$key}}" data-toggle="tab" class="p-3"><i class="fas fa-{{$rule->icon}}"></i> {{$rule->title}} </a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                @foreach($rules as $key => $rule)
                                    <div class="tab-pane fade @if(!$key) in active @endif" id="tab1default{{$key}}">
                                        {!! nl2br($rule->description) !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </section>
@endsection