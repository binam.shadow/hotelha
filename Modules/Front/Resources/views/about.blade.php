@extends('front::layouts.master')

@section('title')
درباره ما
@endsection

@section('content')
@include('front::partials._nav_menu')
<section class="about-us">
    <div class="container">
        <div class="row p-4">
            <div class="col-md-12">
                <h4>درباره الوویلا</h4>
                <div>
                    {!! $about->description !!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection