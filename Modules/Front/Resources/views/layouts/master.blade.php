<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('front/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/materialize.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/chosen.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/owl-carousel.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/selectric.css')}}">
    <!--   <link rel="stylesheet" href="{{asset('front/css/docs.theme.min.css')}}"> -->
    <!--     <link rel="stylesheet" href="{{asset('front/css/owl.theme.default.min.css')}}"> -->


    @stack('stack_styles')
    @yield('styles')
    <title>@yield('title')</title>
    @yield('head')
</head>
<body>


@yield('content')
<!-- End City -->
<!-- Footer -->
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2 right col-xs-12">
                <h4>لینک های مفید</h4>
                <ul class="list-styled">
                    <li><a href="#"> درباره ما</a></li>
                    <li><a href="#">تماس با ما / ثبت شکایات</a></li>
                    <li><a href="#"> سوالات متداول</a></li>
                    <li><a href="#"> قوانین سایت </a></li>
                </ul>
            </div>

            <div class="col-md-2 right col-xs-12">
                <h4>تماس</h4>
                <ul class="list-styled phone">
                    <li>09195272251</li>
                </ul>
            </div>
            <div class="col-md-1 right col-xs-12">
                <a href="{{route('front.index')}}">
{{--                    <img src="{{ asset('front/img/logo.png')}}" class="img-reponsive center-img">--}}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="social-network text-center">
                    <a href="#"><img src="{{asset('front/img/aparat.svg')}}" class="aparat-footer" alt="alovilla aparat"></a>
                    <a href="#"><i class="fab fa-instagram p-3 ml-2"></i></a>
                    <a href="#"><i class="fab fa-telegram-plane p-3 ml-2"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Footer -->
<!-- Optional JavaScript -->
{{--<script src="{{ asset('front/js/jquery.min.js') }}"></script>--}}
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('front/js/bootstrap.js') }}"></script>
<script src="{{ asset('front/js/jquery.sticky-kit.min.js') }}"></script>
<script src="{{ asset('front/js/category-filter.js') }}"></script>
<script src="{{ asset('front/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('front/js/owl-carousel.js') }}"></script>
{{--<script src="{{ asset('front/js/bootstrap-notify.min.js') }}"></script>--}}
<script>
    function numberFormat(num) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}'
        }
    });
    $(document).ready(function() {
        $("img").on("contextmenu",function(){
            if($(this).closest(".item-hotel").length==0)
                return false;

        });
    });
</script>
@stack('stack_scripts')
@yield('scripts')

</body>
</html>