@extends('front::layouts.master')

@section('content')
    @include('front::partials._nav_menu')
    <section class="clientarea">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="welcome"><i class="fas fa-user"></i> سلام
                        {{!is_null($user->name) ? $user->name . ' عزیز ' : '' }}
                        ،به پنل کاربری الوویلا خوش آمدید </p>
                </div>

                <div class="col-sm-12 col-md-2 mb-4 px-0">
                    <div class="li-profile pt-4 pb-4">
                        <ul class="nav tabs pr-4 pl-4">
                            <li class=""><a href="{{route('dashboard.profile')}}"> پروفایل من</a></li>
                            <li class=""><a href="{{route('dashboard.editPassword')}}">تغییر رمز عبور</a></li>
                            <li class=""><a href="{{route('dashboard.wishlist')}}">لیست علاقه مندی ها</a></li>
                            <li class=""><a href="{{route('dashboard.wallet', ['type' => 'user'])}}">گردش حساب میهمان</a></li>
                            <li class=""><a style="margin-bottom: 15px;border-bottom: none;" href="{{route('dashboard.reservation')}}"> رزروهای من</a></li>

                            <li class=""><a href="{{route('dashboard.wallet', ['type' => 'owner'])}}">گردش حساب مالک</a></li>
                            <li class=""><a href="{{route('dashboard.ad.list')}}">آگهی های من</a></li>
                            <li class=""><a href="{{route('dashboard.ad.create')}}"> ثبت آگهی (رایگان)</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-12 col-md-10">
                    @yield('panel_content')
                </div>
                <div class="row mb-5"></div>
            </div>
        </div>
    </section>
@endsection
