@extends('front::layouts.master')

@section('title')
    چرا الوویلا؟
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="rule">
        <div class="container">
            <div class="row p-4">
                <div class="col-md-12">
                    <h4> چرا الوویلا؟</h4>
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                @foreach($guides as $key => $guide)
                                <li @if(!$key) class="active" @endif><a href="#tab1default{{$key}}" data-toggle="tab" class="p-3"><i class="fas fa-{{$guide->icon}}"></i> {{$guide->title}} </a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                @foreach($guides as $key => $guide)
                                <div class="tab-pane fade @if(!$key) in active @endif" id="tab1default{{$key}}">
                                    {!! nl2br($guide->description) !!}
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </section>
@endsection