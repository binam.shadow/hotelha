@extends('front::layouts.master')

@section('title')
    سوالات متداول
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section>
        <div class="container">
            <div class="row p-4">

                <div class="col-md-7">
                    <h4 class="question">سوالات متداول</h4>
                    <p class="mb-4">پاسخ سوالات خود را از این بخش میتوانید مشاهده نمایید.</p>

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        @foreach($faq as $key => $f)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading{{$key}}">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                                            <i class="fas fa-angle-double-left"></i> {{$f->title}}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$key}}" class="panel-collapse collapse @if(!$key) in @endif" role="tabpanel" aria-labelledby="heading{{$key}}">
                                    <div class="panel-body">{{$f->description}}</div>
                                </div>
                            </div>
                        @endforeach
                    </div><!-- /.panel-group -->

                </div>
                <div class="col-md-5">
                    <p></p>
                    <p></p>
                    <p><img src="{{ asset('front/img/faq.jpg') }}" class="mt-5" style="width: 100%;"></p>
                </div><!-- /.col-md-offset-3 col-md-6 -->

            </div></div>
    </section>
@endsection