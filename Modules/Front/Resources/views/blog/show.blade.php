@extends('front::layouts.master')

@section('title')
   {{$blog->name}}
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="about-us">
        <div class="container">
            <div class="row p-4">

                <div class="col-md-3">
                    <h4>سایر مطالب </h4>
                    @foreach($blogs as $random_blog)
                        <div>
                            <a class="col-blue" href="{{route('front.blog.show', ['blog' => $random_blog])}}">{{$random_blog->name}}</a>
                            <p>تاریخ
                                {{jDate::forge($random_blog->created_at)->format('%d %B %Y')}}
                            </p>
                        </div>
                        <hr>
                    @endforeach
                </div>
                <div class="col-md-9 blog-details-box">
                    <img src="{{ asset('uploads/' . $blog->image) }}" class="img-responsive w-100"><br/>
                    <hr>
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="h3 mt-0">
                                <a class="col-blue" href="{{route('front.blog.show', ['blog' => $blog])}}">{{$blog->name}}</a>
                            </h1>
                        </div>
                        <div class="col-sm-4">
                            <p class="text-left text-muted">
                                تاریخ
                              {{jDate::forge($blog->created_at)->format('%d %B %Y')}}
                                @auth
                                <a href="#like" onclick="like()">
                                    <span id="likeLabel" class="pr-4 pl-4 pt-2 pb-2">
                                        {{$blog->likes_count}}
                                        @if($blog->isLiked())
                                        <i class="fa fa-heart"></i>
                                        @else
                                        <i class="fa fa-heart-o"></i>
                                        @endif
                                    </span>
                                </a>
                                @else
                                <a href="{{route('front.login')}}">
                                    <span class="pr-4 pl-4 pt-2 pb-2"><i class="far fa-heart"></i></span>
                                </a>
                                @endauth
                            </p>

                        </div>
                    </div>

                    <p>

                    </p>
                    <hr>
                    <div class="clearfix">
                        {!! $blog->description !!}
                    </div>
                    <div class="row clearfix"></div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        function like() {
            $.ajax({
                url: '{{route('front.blog.like', ['blog' => $blog])}}',
                success: function(result) {
                    var label = result.count;
                    console.log(result.is_liked);
                    if(result.is_liked)
                        label += ' <i class="fa fa-heart"></i>';
                    else
                        label += ' <i class="fa fa-heart-o"></i>';
                    $('#likeLabel').html(label);
                }
            });
        }
    </script>
@endsection