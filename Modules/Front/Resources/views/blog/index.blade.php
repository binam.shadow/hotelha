@extends('front::layouts.master')

@section('title')
 لیست مطالب مجله سفر
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="bg-gray">
        <div class="container">
            <div id="dynamicContent">
                <h3>مجله سفر</h3>
                <div class="infinite-scroll">
                    @foreach($blogs as $blog)
                        <div class="col-md-4">
                            <div class="blog-box">
                                @include('front::partials._blog_card')
                            </div>
                        </div>
                    @endforeach
                    {{ $blogs->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection


@section('scripts')
    <script src="{{ asset('front/js/bootstrap-slider.js') }}"></script>
    <script src="{{ asset('front/js/jquery.jscroll.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/block-ui/block-ui.min.js') }}"></script>
    <script>
        var state = "minimized";
        //Check if navbar is expanded or minimized and handle
        $('#navbar-toggle').click(function() {
            if (state == "expanded") {
                $('.sidebar').css('margin-right', '-240px');
                $('.content-sidebar').hide();
                state = "minimized";
            } else {
                if (state == "minimized") {
                    $('.sidebar').css('margin-right', '0px');
                    $('.content-sidebar').show();
                    state = "expanded";
                }
            }
        })

        var priceSlider = $('#priceSlider').slider({});
        $('#priceSlider').on('slide', function(data_range) {
            $("#minPriceLabel").html(numberFormat(data_range.value[0]));
            $("input[name=min_price]").val(data_range.value[0]);

            $("#maxPriceLabel").html(numberFormat(data_range.value[1]));
            $("input[name=max_price]").val(data_range.value[1]);
        });


        $('#sizeSlider').slider({});
        $('#sizeSlider').on('slide', function(data_range) {
            $("#minSizeLabel").html(numberFormat(data_range.value[0]));
            $("input[name=min_size]").val(data_range.value[0]);

            $("#maxSizeLabel").html(numberFormat(data_range.value[1]));
            $("input[name=max_size]").val(data_range.value[1]);

        });

        $('#roomSlider').slider({});
        $('#roomSlider').on('slide', function(data) {
            var label = data.value;
            if(data.value == 6)
                label = 'بیشتر از 5';

            $("input[name=room]").val(data.value);
            $("#roomLabel").html(label);
        });

        $('#searchBtn').on('click', function() {
            $.blockUI({message: '<h1>لطفا صبر کنید...</h1>'});
            $.ajax({
                url: "{{route('front.estate.search')}}",
                data: $("#searchForm").serialize(),
                success: function(result) {
                    $('#dynamicContent').html(result);
                    initJscroll()
                    $('.sidebar').css('margin-right', '-240px');
                    $('.content-sidebar').hide();
                    state = "minimized";
                }
            })
            .always(function() {
                $.unblockUI()
            });
        });


        //
//        $('#flat-slider-distance').slider({
//            orientation: 'horizontal',
//            range:       true,
//            values:      [17,67]
//        });
//
//        $('#flat-slider-room').slider({
//            orientation: 'horizontal',
//            range:       false,
//            values:      [17,67]
//        });

        $(document).scroll(function(){
            var x= $(document).scrollTop();
            var $defaultHeight= $('.top-header').outerHeight();
            if(x< ($defaultHeight + 10))
                $('.sidebar').removeClass("zero")
            else
                $('.sidebar').addClass("zero")
        })

        function initJscroll() {
            $('ul.pagination').hide();
            $(function() {
                $('.infinite-scroll').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="/images/loading.gif" alt="Loading..." />',
                    padding: 0,
                    nextSelector: '.pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll',
                    callback: function() {
                        $('ul.pagination').remove();
                    }
                });
            });
        }
        initJscroll()

        $("select[name=province]").on('change', function() {
            $.ajax({
                url: "{{route('front.getCities')}}",
                data: {
                    province_id: $(this).val()
                },
                success: function(result) {
                    var option = '<option value="0">همه شهرها</option>';
                    $.each(result, function(key, value) {
                        option += '<option value="'+ value.id +'">' + value.name + '</option>';
                    });
                    $("select[name=city]").html(option);
                }
            });
        });
    </script>
@endsection