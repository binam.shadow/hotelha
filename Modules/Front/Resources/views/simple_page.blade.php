@extends('front::layouts.master')

@section('title')
   {{$data['title']}}
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="about-us">
        <div class="container">
            <div class="row p-4">
                <div class="col-md-12">
                    <h4>{{$data['title']}}</h4>
                    <div>
                        {!! $data['description'] !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection