@extends('front::layouts.master')

@section('title')
هتلیها
@endsection
@section('styles')

        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
    .file-footer-caption, .file-thumbnail-footer{
        display: none;
    }
    .col-center {
        margin: 0 auto;
        float: none;
    }
</style>
<link rel="stylesheet" href="{{ asset('dp/persian-datepicker.css') }}"/>
@endsection
@section('content')
    @include('front::partials._full_header', [
        'header_type' => 1
    ])
    <div class="home-page">
    <!-- end banner section -->
    <!-- div Service -->
    <section class="service">
        <div class="container">

            <div class="row">
                <a href="#">
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                        <img src="{{ asset('front/img/support.svg')}}" class="img-responsive home-icons center-img mb-4">
                        <h4 class="col-black"><b>پشتیبانی تا پایان سفر</b></h4>
                    </div>
                </a>
                <a href="#">
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                        <img src="{{ asset('front/img/best-price.svg')}}" class="img-responsive home-icons center-img mb-4">
                        <h4 class="col-black"><b>تضمین بهترین قیمت</b></h4>
                    </div>
                </a>
                <a href="#">
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                        <img src="{{ asset('front/img/24h.svg')}}" class="img-responsive home-icons center-img mb-4">
                        <h4 class="col-black"><b>رزرو 24 ساعته</b></h4>
                    </div>
                </a>
                <a href="#">
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                        <img src="{{ asset('front/img/refund.svg')}}" class="img-responsive home-icons center-img mb-4">
                        <h4 class="col-black"><b>امکان لغو رزرو و ضمانت بازگشت وجه</b></h4>
                    </div>
                </a>
            </div>

        </div>
    </section>
    <!-- end sevice -->
    <!-- Hotel -->
    <section class="home-tags-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="category-filter pb-5">
                        <button class="btn btn-outline-primary category-button active ml-3" data-filter="new">هتل های برگزیده </button>

                        <button class="btn btn-outline-primary sss" data-filter="new">هتل داخلی</button>
                        <button class="btn btn-outline-primary category-button" data-filter="new">هتل خارجی</button>
                    </div>
                </div>
            </div>


            <div class="row" id="aaa">

                <div class="filter new">
                    @foreach($hotels as $estate )
                        @include('front::partials._estate_card')
                    @endforeach
                </div>
            </div>



            {{--<div id="customizeDashboard" class="roundBorder greyGradient clearfix">--}}

                {{--<div class="row" id="aaa">--}}

                    {{--<div class="filter new">--}}
                        {{--@foreach($hotels as $estate )--}}
                            {{--@include('front::partials._estate_card')--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}


                {{--<div class="row" id="aa">--}}

                    {{--<div class="filter new">--}}
                        {{--@foreach($hhs as $estate )--}}
                            {{--@include('front::partials._estate_card')--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="fl widgetColumns">--}}
                    {{--<ul>--}}
                        {{--<li><a href="#" class="monitoringDesc">Show group 1</a></li>--}}
                        {{--<li><a href="#" class="monitoringDesc2">Show group 2</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}


                {{--<div class="widgetGroup">--}}
                    {{--<h3 class="subFontSmall">Summary</h3>--}}
                    {{--<p>Place holder div and text</p>--}}
                {{--</div>--}}

                {{--<div class="row" id="aa">--}}

                    {{--<div class="filter new">--}}
                        {{--@foreach($hhs as $estate )--}}
                            {{--@include('front::partials._estate_card')--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<!--First Widget Content-->--}}
                {{--<div class="widgetGroupnew" id="newGroup" style="display:none">--}}
                    {{--<h3>NEW GROUP 1</h3>--}}

                    {{--<p>New Group content</p>--}}

                {{--</div>--}}

                {{--<!--Second group-->--}}
                {{--<div id="newGroup2" style="display:none">--}}
                    {{--<h3>NEW GROUP 2!!!</h3>--}}
                    {{--<p>New Group content</p>--}}
                {{--</div>--}}



            {{--</div>--}}


            <div class="col-sm-4 col-center">
                <a class="btn btn-info btn-block btn-show-all py-3" href="#"><span class="text-warning"></span>مشاهده همه هتل ها</a>
            </div>
        </div>
    </section>
    {{--<!-- end hotel -->--}}
    {{--<!-- Download app -->--}}

    {{--<section class="bg-gray" id="application">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<!-- <mark class="pr-5 pl-5 pt-2 pb-2 mb-5 shape"><i class="fas fa-arrow-left"></i></mark> -->--}}
                {{--<div class="col-md-5">--}}
                    {{--<h2 class="alovila"><b>الوویلایی شو!</b></h2>--}}
                    {{--<p>همین الان شماره تلفن خودت رو اینجا وارد کن تا مراحل ثبت نام الوویلایی خودت رو شروع کنی</p>--}}
                    {{--<form action="" class="ltr">--}}
                    {{--<div class="form-reserve mt-4 mb-4">--}}
                    {{--<div class="form-reserve-r">--}}
                    {{--<div class="form-reserve-c">--}}
                    {{--<input type="text" name="phone" placeholder="09123456789" class="main-register pr-5 pl-5 pt-2 pb-2">--}}
                    {{--</div>--}}
                    {{--<div class="form-reserve-c calender-i">--}}
                    {{--<i class="fas fa-mobile-alt"></i>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<input type="submit" value="ثبت نام" class="submit-main pr-5 pl-5 mt-2">--}}
                    {{--</form>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-6">--}}
                            {{--<a href="#">--}}
                                {{--<img src="{{asset('front/img/sibapp.svg')}}" class="store-btn" alt="دانلود از سیب اپ">--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-6">--}}
                            {{--<a href="{{route('front.ios')}}">--}}
                                {{--<img src="{{asset('front/img/ios-direct-link.png')}}" class="store-btn" alt="دانلود مستقیم">--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-6">--}}
                            {{--<a href="#">--}}
                                {{--<img src="{{asset('front/img/cafebazaarbtn.svg')}}" class="store-btn" alt="دانلود از کافه بازار">--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-6">--}}
                            {{--<a href="#">--}}
                                {{--<img src="{{asset('front/img/android-direct-link.png')}}" class="store-btn" alt="دانلود از گوگل پلی">--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-7">--}}
                    {{--<img src="{{ asset('front/img/infographic.png')}}" class="img-responsive">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</section>--}}
    {{--<!-- End Download app -->--}}
    {{--<!-- trip -->--}}
    {{--<section class="reviwe">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<a href="{{route('front.blog.index')}}"><mark class="pr-5 pl-5 pt-2 pb-2 mb-5 mark-divider"> مجله سفر</mark></a>--}}
                {{--<div class="owl-carousel owl-theme" id="tripMag">--}}
                   {{--@foreach($blogs as $blog)--}}
                       {{--@include('front::partials._blog_card')--}}
                   {{--@endforeach--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<!-- End trip -->--}}
    {{--<!-- register -->--}}
    {{--<!-- End register -->--}}
    {{--<section class="popular">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<mark class="pr-5 pl-5 pt-2 pb-2 mb-5 mark-divider"> شهرهای پرطرفدار</mark>--}}
                {{--<div class="owl-carousel owl-theme" id="city">--}}
                    {{--@foreach($popular_cities  as $popular_city)--}}
                        {{--<a href="{{route('front.estate.list')}}?city={{$popular_city->extra_data->city_id}}">--}}
                            {{--<div class="item">--}}
                                {{--<div class="img-city">--}}
                                    {{--<img src="{{ asset('uploads/' . $popular_city->image)}}" class="img-reponsive mb-2">--}}
                                {{--</div>--}}
                                {{--<div class="name-city p-2">--}}
                                    {{--<span>{{$popular_city->title}}</span><br>--}}
                                    {{--<span>{{$popular_city->description}}</span>--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</section>--}}
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('dp/persian-date.js ') }}"></script>
    <script src="{{ asset('dp/persian-datepicker.js') }}"></script>
    
<script src="{{ asset('front/js/jquery.selectric.js') }}"></script>
<script>

    $(".chosen").chosen({rtl: true});

    $(function() {
        $('.selectric').selectric();
    });

    $("#tripMag").owlCarousel({
        items :3,
        loop: true,
        margin: 10,
        nav:true,
        dots:false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }}
    });


    $("#city").owlCarousel({
        items :4,
        loop: true,
        margin: 10,
        nav:true,
        dots:false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }}
    });

    $('.category-button').categoryFilter();


    $('.normal-example').persianDatepicker({
        responsive: true,
        initialValue: false,
        observer: true,
        format: 'YYYY/MM/DD',
        autoClose: true,
    });


</script>


@endsection
