@extends('front::layouts.master')

@section('title')
    پرداخت موفق
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="about-us">
        <div class="container">
            <div class="row p-4">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <p>پرداخت با موفقیت انجام شد</p>
                    </div>
                    <br>
                    @if(isset($data->user))
                        <p>میهمان گرامی رزرو شما با موفقیت در سیستم ثبت شد. لطفا اطلاعات رزرو خود را ذخیره کرده و تا پایان مسافرت نزد خود نگهدارید.</p>
                    @endif
                    <table class="table table-bordered">
                        <tr>
                            <td>مبلغ</td>
                            <td>{{number_format($data->price)}} ریال </td>
                        </tr>
                        <tr>
                            <td>تاریخ</td>
                            <td>{{jDate::forge(\Carbon\Carbon::now())->format('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td>زمان</td>
                            <td>{{jDate::forge(\Carbon\Carbon::now())->format('time')}}</td>
                        </tr>
                        <tr>
                            <td>شماره پیگیری تراکنش</td>
                            <td>{{$data->resNum}}</td>
                        </tr>
                        @if(isset($data->user))
                            <tr>
                                <th>نام مالک :</th>
                                <td>{{$data->owner->name}}</td>
                            </tr>
                            <tr>
                                <th>شماره مالک :</th>
                                <td>{{$data->owner->phone}}</td>
                            </tr>
                            <tr>
                                <th>کد ملک:</th>
                                <td>{{$data->estate_id}}</td>
                            </tr>

                            <tr>
                                <th>کد واچر:</th>
                                <td>{{$data->reserve_code}}</td>
                            </tr>
                            <tr>
                                <th>روزهای رزرو شده:‌</th>
                                <td>
                                    @foreach($data->dates as $date)
                                        {{$date}}<br>
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection