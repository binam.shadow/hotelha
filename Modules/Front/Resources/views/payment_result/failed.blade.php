@extends('front::layouts.master')

@section('title')
    پرداخت نا موفق
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="about-us">
        <div class="container">
            <div class="row p-4">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <p>
                            <b>پرداخت ناموفق</b> [{{$data->description}}]
                        </p>
                    </div>
                    <br>
                    <p>درصورتی که مبلغی از حساب شما کسر شده باشد در کمتر از 12 ساعت به حساب شما برگشت داده خواهد شد</p>
                    <table class="table table-bordered">
                        <tr>
                            <td>تاریخ</td>
                            <td>{{jDate::forge(\Carbon\Carbon::now())->format('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td>زمان</td>
                            <td>{{jDate::forge(\Carbon\Carbon::now())->format('time')}}</td>
                        </tr>
                        <tr>
                            <td>شماره پیگیری تراکنش</td>
                            <td>{{$data->resNum}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection