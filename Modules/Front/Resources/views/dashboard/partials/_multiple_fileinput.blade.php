@php $field_name = isset($field_name) ? $field_name : 'gallery' @endphp
@php $field_type = isset($field_type) ? $field_type : 'image' @endphp

@pushonce('stack_styles')
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}">
    <style>
        .btn-file {
            float: left;
        }
        .btn-file span{
            color: #FFF;
        }
    </style>
@endpushonce

@pushonce('stack_scripts')
    <script src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('backend/plugins/bootstrap-fileinput/themes/fa/theme.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bootstrap-fileinput/js/locales/fa.js') }}"></script>
@endpushonce


@push('stack_scripts')
<script type="text/javascript">
$("#{{$field_name}}").fileinput({
    language: 'fa',
    rtl: true,
    uploadAsync: false,
    uploadUrl: '/',
    allowedFileTypes: ['{{$field_type}}'],
    overwriteInitial: false,
    maxFileSize: 2048,
    showPreview: true,
    showCaption: false,
    showRemove: true,
    showUpload: false,
    multiple: true,
    maxFilesNum: 5,
    removeIcon: '<i class="fa fa-trash"></i>',
    removeTitle: 'Remove file',
    layoutTemplates: {actions: '<div class="file-actions">\n' +
        '    <div class="file-footer-buttons">\n' +
        '        {delete}' +
        '    </div>\n' +
        '    <div class="clearfix"></div>\n' +
        '</div>',
        actionDelete: '<button type="button" class="kv-file-remove {removeClass}" title="{removeTitle}"{dataUrl}{dataKey}><i class="fa fa-trash"></i></button>\n',
        btnBrowse: '<div tabindex="500" class="{css} btn-purple"{status}>انتخاب تصاویر</div>',
    },
    @if(isset($gallery))
    initialPreview: [
        @foreach($gallery as $media)
            "{{ asset('uploads/'. $media->image)}}",
        @endforeach
    ],
    initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
    initialPreviewFileType: '{{$field_type}}',
    initialPreviewConfig: [
        @foreach($gallery as $media)
        {
            {{--filetype: "{{$field_type}}/{{$media->extension}}",--}}
            url: '{{  route('dashboard.deleteImage') }}',
            key: '{{ $media->id }}'
        },
        @endforeach
    ],
    @endif
});
</script>
@endpush