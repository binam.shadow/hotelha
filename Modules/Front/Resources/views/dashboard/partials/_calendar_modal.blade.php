<div class="modal fade" id="calendarModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">ثبت قیمت</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <label class="form-label">قیمت</label>
                        <input class="form-control" name="date_price" value="" required=""
                               aria-required="true" type="text">
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-sm-3">
                        <div class="switch">
                            <label><input name="active_date" checked="" type="checkbox"><span class="lever switch-col-blue"></span>فعال باشد؟</label>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="selected_date" value="">
                <button type="submit" id="add-price" class="btn btn-primary btn-lg pull-right waves-effect" data-dismiss="modal">ثبت</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">@lang('order::default.close')</button>
            </div>
        </div>
    </div>
</div>