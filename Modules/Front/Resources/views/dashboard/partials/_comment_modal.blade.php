<div class="modal fade" id="commentModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ارسال دیدگاه</h4>
            </div>
            <div class="modal-body">
                <div class="row form-page">
                    <div class="col-xs-12">
                        <div class="alert alert-success" id="successSend" style="display: none;">
                            دیدگاه شما با موفقیت ثبت شد و پس از تایید نمایش داده خواهد شد.
                        </div>
                    </div>
                    <form id="commentForm" method="post" action="{{route('front.sendComment')}}">
                        <input type="hidden" id="commentEstateId" name="estate_id">
                        {{csrf_field()}}
                        <div class="col-md-12">
                            <label>دیدگاه شما</label>
                            <textarea rows="5" name="description" class="form-control mb-2" placeholder="نظر خود را بنویسید" required></textarea>
                        </div>
                        <div class="col-md-12">
                            <input id="sendCommentBtn" type="submit" value="ارسال نظر" class="submit-reviwe btn-block pr-5 pl-5 mt-5">
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="selected_date" value="">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">@lang('order::default.close')</button>
            </div>
        </div>
    </div>
</div>

@pushonce('stack_scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>

<script>
    function sendComment(id) {
        $('#commentEstateId').val(id)
        $('#commentModal').modal('show');


        $("#commentForm").validate({
            submitHandler: function(form) {
                $("#sendCommentBtn").html('درحال ارسال ...');
                $.ajax({
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    method: 'POST',
                    success: function(result) {
                        if(result.status == 'success') {
                            $('[name=description]').val('');
                            $('[name=description]').html('');
                            $("#commentForm").fadeOut(1500);
                            $("#successSend").fadeIn(1500);
                        }
                    },
                    error: function() {
                        alert("مشکلی در ارسال اطلاعات به وجود آمده است!");
                    }
                }).always(function() {
                    $("#sendCommentBtn").html('ارسال نظر');
                })
                return false;
            }
        });
    }
</script>
@endpushonce