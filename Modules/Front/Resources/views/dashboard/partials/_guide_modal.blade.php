<!-- modal guid -->
<div class="modal fade" id="guid" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel"> راهنمای مدیریت جدول روزها</h3>
            </div>
            <div class="modal-body">
                <h4>راهنمای نمادها و رنگ ها</h4>
                <p> در جدول روزهای ماه هر روز با یک باکس(مستطیل) مشخص شده است و در هر مستطیل اطلاعاتی شمامل، روزهته(شنبه، یکشنبه و ...)، قیمت اجاره روزانهبه توان، و تاریخ آن روز وجود داد.
                    <br/>
                    مستطیل های خاکستری: این مستطیل ها روزهایی از ماه هستند که گذشته اند.<br/>
                </p>
                <h4>راهنمای تغییر قیمت</h4>
                <p>دقت کنید که نیاز نیست قیمت تک تک روزها را تغییر دهید یا تعیین کنید، تمام روزها بصورت اتوماتیک به قیمت کلی که تعیین کرده اید تعیین قیمت می شوند، فقط اگر روزهای خاصی را ( مثل ایامی از سال که مسافرت زیاد است یا ...)  برای افزایش یا تخفیف و کاهش قیمت مدنظر دارید با لمس همان روز در پنجره باز شده قیمت جدید را تعیین کنید و سپس ذخیره تغییرات را بزنید.</p>

            </div>
        </div>
    </div>
</div>