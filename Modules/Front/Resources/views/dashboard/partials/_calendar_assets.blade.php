@pushonce('stack_styles')
<link href='{{ asset('backend/plugins/fullcalendar/dist/fullcalendar.css')}}' rel='stylesheet' />
<link href='{{ asset('backend/plugins/fullcalendar/dist/fullcalendar.print.css')}}' rel='stylesheet' media='print' />
<style>
    .fc-event{
        border: none;
        padding: 0px 5px 2px 5px;
    }
    .fc-title {
        color: #fff;
        font-size: 13px;
    }
    .fc-past {
        background: #f6f6f6;
    }
    .purple-shadow{
        background-color: #aa2fed !important;
        box-shadow: 1px 1px 10px #c150ff !important;
    }
</style>
@endpushonce

@pushonce('stack_scripts')
<script src='{{ asset('backend/plugins/fullcalendar/lib/moment/moment.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/lib/moment/moment-jalaali.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/lib/jquery-ui/jquery-ui.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/dist/fullcalendar.js')}}'></script>
<script src='{{ asset('backend/plugins/fullcalendar/lang/fa.js')}}'></script>
@endpushonce