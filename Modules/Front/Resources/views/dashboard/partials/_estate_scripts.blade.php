
@pushonce('stack_styles')
<link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
@endpushonce
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>

<script>
    function parseArabic(str) {
        if(str == '') return '';
        var out_num = Number( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
            return d.charCodeAt(0) - 1632; // Convert Arabic numbers
        }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
            return d.charCodeAt(0) - 1776; // Convert Persian numbers
        }) );
        return isNaN(out_num) ? 0 : out_num;
    }
    $(function () {

        $('#features').select2({
            dir: 'rtl'
        });

        $.validator.messages.max = 'عدد وارد شده معتبر نیست';

        $('#estateForm').validate({
            errorElement: 'div',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                price: {
                    required: true,
                    max: 3000000
                },
                friday_price: {
                    max: 3000000
                },
                rules: {
                    required: true
                },
                distancetosea: {
                    max: 3000000
                },
                size: {
                    max: 3000000
                },
                capacity: {
                    max: 3000000
                }
            }
        });

        $('input[name=apartment]').on('change', function() {
            if (this.value == 'apartment') {
                $('input[name=forest]').prop('checked', false);
                $('input[name=shore]').prop('checked', false);
                $('input[name=forest]').attr('disabled', true);
                $('input[name=shore]').attr('disabled', true);
            }
            else {
                $('input[name=forest]').attr('disabled', false);
                $('input[name=shore]').attr('disabled', false);
            }
        });

        $('input[name=apartment]:first-child').trigger('change');

        $("select[name=province]").on('change', function() {
            $.ajax({
                url: "{{route('front.getCities')}}",
                data: {
                    province_id: $(this).val()
                },
                success: function(result) {
                    var option = '';
                    $.each(result, function(key, value) {
                        option += '<option value="'+ value.id +'">' + value.name + '</option>';
                    });
                    $("select[name=city]").html(option);
                }
            });
        });

    });
</script>