@php $field_name = isset($field_name) ? $field_name : 'avatar' @endphp
<div class="kv-avatar">
	<input id="{{$field_name}}" name="{{$field_name}}" type="file">
</div>

@pushonce('stack_style')
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}">
@endpushonce

@pushonce('stack_scripts')
    <script src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('backend/plugins/bootstrap-fileinput/themes/fa/theme.js') }}"></script>
    <script src="{{ asset('backend/plugins/bootstrap-fileinput/js/locales/fa.js') }}"></script>
@endpushonce

@push('stack_scripts')
    <script type="text/javascript">
        $("#{{$field_name}}").fileinput({
			@if(isset($old_image) && !is_null($old_image->image))
				uploadUrl: "/", // server upload action
			    deleteUrl: "/",
			    showUpload: false, // hide upload button
			    showRemove: true, // hide remove button
				initialPreview: [
                    "{{ asset('uploads/'. $old_image->image)}}",
				],
				initialPreviewFileType: 'image',
				initialPreviewAsData: true,
				initialPreviewShowDelete: true,
				initialPreviewConfig: [
			        {
			            width: '160px',
			            url: "{{  route('dashboard.deleteImage') }}",
			            key: "{{ $old_image->id }}",
			            extra: {
			                feature_image: 1
						}
			        }
			    ],
			@endif
            language: 'fa',
            theme: "fa",
            rtl: true,
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            showBrowse: false,
            browseOnZoneClick: true,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fa fa-trash"></i>',
            removeIcon: '<i class="fa fa-trash"></i>',
            removeTitle:  'لغو تغییرات',
            elErrorContainer: '#kv-avatar-errors-2',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="{{asset('/backend/plugins/bootstrap-fileinput/img/default.png')}}" alt="نمایه" width="185"><h6 class="text-muted">برای انتخاب تصویر کلیک کنید</h6>',
            layoutTemplates: {
                main2: '{preview} {remove}',
                actionDelete: '<button type="button" class="kv-file-remove {removeClass}" title="{removeTitle}"{dataUrl}{dataKey}><i class="fa fa-trash"></i></button>\n',

            },
            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"]
        }).on('filedeleted', function(event, key, jqXHR, data) {
		    $("#{{$field_name}}").fileinput('destroy');
			$("#{{$field_name}}").fileinput({
	            language: 'fa',
	            rtl: true,
	            overwriteInitial: true,
	            maxFileSize: 1500,
	            showClose: false,
	            showCaption: false,
	            showBrowse: false,
	            browseOnZoneClick: true,
	            browseLabel: '',
	            removeLabel: '',
	            browseIcon: '<i class="material-icons">create_new_folder</i>',
	            removeIcon: '<i class="material-icons">delete</i>',
	            removeTitle:  'لغو تغییرات',
	            elErrorContainer: '#kv-avatar-errors-2',
	            msgErrorClass: 'alert alert-block alert-danger',
	            defaultPreviewContent: '<img src="{{asset('/backend/plugins/bootstrap-fileinput/img/default.png')}}" alt="نمایه" width="185"><h6 class="text-muted">برای انتخاب تصویر کلیک کنید</h6>',
	            layoutTemplates: {main2: '{preview} {remove}'},
	            allowedFileExtensions: ["jpg", "png", "gif"]
	        });
		});
    </script>
@endpush
