@php
if(!isset($lat)) $lat = "";
if(!isset($lng)) $lng = "";
@endphp



<style>
    .btn-map-search{
        padding: 2px 5px;
        width: 65px;
        border-radius: 3px;
    }
    .btn-map-search i{
        font-size: 23px !important;
    }
</style>

<div class="row">
    <input class="form-control" name="lat" id="lat" value="{{ $lat }}" required="" aria-required="true" type="hidden">
    <input class="form-control" name="lng" id="lng" value="{{ $lng }}" required="" aria-required="true" type="hidden">

    <div id="mp" class="col-md-12 form-group form-float">
        <div class="form-group">
            <div class="col-md-12">
                <section id="place-on-map">
                    <label for="address-map" style="padding-top:10px"> پس از جستجوی محل
                        پوینتر را کشیده و بر روی مکان مورد نظر رها کنید.</label>
                    <div class="">
                        <input class="form-control" style="width:100%" id="geocomplete" type="text"
                               placeholder="آدرس را وارد کنید" value="تهران"/>
                    </div>
                    <div class="map_canvas" style=" height: 350px; width: 100%;"></div>
                    <br/>
                    <div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@push('stack_scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-IgCJVNvcbIOEaC7oQduowDaG3aJzlnM&libraries=places&v=3.exp"></script>
<script src="{{asset('js/jquery.geocomplete.js')}}"></script>

<script type="text/javascript">
$(function () {
    
    
    $("#geocomplete").geocomplete({
        map: ".map_canvas",
        mapOptions: {
            zoom: 17
        },
        markerOptions: {
            draggable: true
        }
    });

    $("#geocomplete").bind("geocode:dragged", function (event, latLng) {

        $("input#lat").val(latLng.lat());
        $("input#lng").val(latLng.lng());

    });
    $("#find").click(function () {
        $("#geocomplete").trigger("geocode");

    }).click();
    
    @if($lat != '' && $lng != '')
    var lat_and_long = "{{$lat}}, {{$lng}}";
    $("#geocomplete").geocomplete("find", lat_and_long);
    @endif
});
</script>
@endpush