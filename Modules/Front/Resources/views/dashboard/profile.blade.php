@extends('front::layouts.dashboard')

@section('title')
    پروفایل کاربری
@endsection

@section('panel_content')
    <div class="tab-content p-4">
        <h4 class="p-4"> <i class="fas fa-user-edit"></i> اطلاعات شخص حقیقی</h4>
        @if (Session::has('msg'))
            <div class="alert alert-success">{{ Session::get('msg') }}</div>
        @endif
        <form id="profileForm" method="post" action="{{route('dashboard.profile')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6 right col-sm-12 col-xs-12">
                    <label>نام و نام خانوادگی: </label>
                    <input type="text" value="{{$user->name}}" name="name">
                    <p class="text-danger">{{$errors->first('name')}}</p>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 right">
                    <label>ایمیل: </label>
                    <input name="email" type="text" value="{{$user->email}}">
                    <p class="text-danger">{{$errors->first('email')}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 right col-sm-12 col-xs-12">
                    <label>کد ملی : </label>
                    <input type="text" value="{{old('national_code', $user->national_code)}}" name="national_code">
                    <p class="text-danger">{{$errors->first('national_code')}}</p>
                </div>
                <div class="col-md-6 right col-sm-12 col-xs-12">
                    <label>شماره کارت : </label>
                    <input type="text" value="{{$user->card_number}}" name="card_number">
                    <p class="text-danger">{{$errors->first('card_number')}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <label>کدمعرف من: </label>
                    <input type="text" value="{{$user->recommend_code}}"readonly>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12 right">
                    <label>کدمعرفی من به دوستان: </label>
                    <input type="text" value="{{$user->phone}}" readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 right">
                    <label>جنسیت: </label>

                    <select name="sex" class="form-control nav disabled">
                        <option disabled selected>انتخاب کنید</option>
                        <option value="female" @if($user->sex == 'female') selected @endif>مونث</option>
                        <option value="male" @if($user->sex == 'male') selected @endif>مذکر</option>
                    </select>
                    <p class="text-danger">{{$errors->first('sex')}}</p>
                </div>
            </div>
            <input type="submit" value="ویرایش مشخصات" class="submit-reviwe pr-5 pl-5 mt-5">
        </form>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>

    <script>
        function convertNumbers2English(string) {
            return string.replace(/[\u0660-\u0669]/g, function (c) {
                return c.charCodeAt(0) - 0x0660;
            }).replace(/[\u06f0-\u06f9]/g, function (c) {
                return c.charCodeAt(0) - 0x06f0;
            });
        }
        jQuery.validator.addMethod("valid_national_code", function(value, element) {
            value = convertNumbers2English(value);
            var L=value.length;

            if(L<8 || parseInt(value,10)==0) return false;
            value=('0000'+value).substr(L+4-10);
            if(parseInt(value.substr(3,6),10)==0) return false;
            var c=parseInt(value.substr(9,1),10);
            var s=0;
            for(var i=0;i<9;i++)
                s+=parseInt(value.substr(i,1),10)*(10-i);
            s=s%11;
            return (s<2 && c==s) || (s>=2 && c==(11-s));
            return true;
        }, "کد ملی وارد شده معتبر نیست");

        $("#profileForm").validate({
            errorClass: "error",
            rules: {
                name: {
                  required: true,
                },
                national_code: {
                    required: true,
                    valid_national_code: true
                },
                sex: {
                    required: true
                },
                email: {
                    email: true
                },
                number_of_persons: {
                    number: true,
                }
            }
        });
    </script>
@endsection