@extends('front::layouts.dashboard')

@section('title')
   تاریخچه رزرو [ {{$estate->name}} ]
@endsection

@section('panel_content')
    <div class="tab-content p-4">
        <h4 class="p-4"> <i class="fas fa-archive"></i> تاریخچه رزرو [ {{$estate->name}} ]</h4>
        <div style="overflow-x:auto">
            <div class="t-reservation text-center">
                <div class="r-reservation header-table">
                    <div class="td-reservation">
                        <span> کدرزرو</span>
                    </div>
                    <div class="td-reservation">
                        <span> تاریخ ثبت</span>
                    </div>
                    <div class="td-reservation">
                        <span> تاریخ شروع</span>
                    </div>
                    <div class="td-reservation">
                        <span> تاریخ پایان</span>
                    </div>
                    <div class="td-reservation">
                        <span> مجموع مبلغ (ریال)</span>
                    </div>
                    <div class="td-reservation">
                        <span> تغییر وضعیت</span>
                    </div>
                </div>
                @foreach($reserves as $reserve)
                    <div class="r-reservation">
                        <div class="td-reservation">
                            <span> A{{$reserve->id}}</span>
                        </div>
                        {{--<div class="td-reservation">--}}
                            {{--<span>{{$reserve->estate->name}}</span>--}}
                        {{--</div>--}}
                        <div class="td-reservation">
                            <span>{{jDate::forge($reserve->created_at)->format('%d %B %Y')}}</span>
                        </div>
                        <div class="td-reservation">
                            <span>{{jDate::forge($reserve->dates()->first()->date)->format('%d %B %Y')}}</span>
                        </div>
                        <div class="td-reservation">
                            <span>{{jDate::forge($reserve->dates()->orderBy('id', 'desc')->first()->date)->format('%d %B %Y')}}</span>
                        </div>
                        <div class="td-reservation">
                            <span>{{number_format($reserve->discount_price)}}</span>
                        </div>
                        <div class="td-reservation">
                            @if(!$reserve->status)
                                <a href="" class="cancel">درحال بررسی </a>
                            @elseif($reserve->status == 2)
                                <a href="" class="cancel">کنسل شده توسط کاربر </a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection