@extends('front::layouts.dashboard')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}">
@endsection

@section('title')
 ویرایش آگهی
@endsection

@section('panel_content')
    <div class="tab-content p-4" id="register">
        <div class="row p-4">
            <h4 class="p-4 mb-5"><i class="fas fa-plus-square"></i> ویرایش آگهی</h4>
            <p>تمام مراحل ثبت اقامتگاه و انتشار آن در وبسایت, کاملا رایگان می باشد.</p>
            <p class="guid">جهت اطلاع از معانی نماد و رنگ های جدول روی <a data-toggle="modal" data-target="#guid"> مدیریت راهنما جدول </a> کلیک کنید </p>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form id="estateForm" action="{{route('dashboard.ad.edit', ['estate' => $estate])}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 mt-4">
                    <label>نام استان</label>
                    <select name="province" class="form-control disabled">
                        <option disabled> انتخاب کنید</option>
                        @foreach($provinces as $province)
                            <option value="{{$province->id}}" @if($province->id == $estate->province) selected @endif>{{$province->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 mt-4">
                    <label>نام شهر</label>
                    <select name="city" class="form-control nav disabled">
                        <option disabled>انتخاب کنید</option>
                        @foreach($cities as $city)
                            <option value="{{$city->id}}" @if($city->id == $estate->city) selected @endif>{{$city->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6  mt-4">
                    <label>نام روستا</label>
                    <input class="p-4" type="text" name="village_name" placeholder="نام روستا" value="{{$estate->village_name}}">
                </div>
                <div class="col-md-6 mt-4">
                    <label>عنوان اقامتگاه</label>
                    <input class="p-4" type="text" name="name" placeholder="عنوان اقامتگاه" value="{{$estate->name}}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mt-3">
                    <label>اجاره بهاء روزانه (تومان)</label>
                    <input class="p-4" type="text" name="price" placeholder="اجاره بهاء روزانه (تومان)" onkeyup="this.value=parseArabic(this.value)" value="{{$estate->price}}">
                </div>

                <div class="col-md-6 mt-3">
                    <label>اجاره بهاء در روزهای پنجشنبه و جمعه (تومان)</label>
                    <input class="p-4" type="text" name="friday_price" onkeyup="this.value=parseArabic(this.value)" placeholder="0">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mt-4">
                    <label>فاصله اقامتگاه از دریا (متر)</label>
                    <input  class="p-4" type="text" name="distancetosea" placeholder="0" onkeyup="this.value=parseArabic(this.value)" value="{{$estate->distancetosea}}">
                </div>
                <div class="col-md-6 mt-4">
                    <label>متراژ کل (مترمربع)</label>
                    <input class="p-4" type="text" name="size" placeholder="0" onkeyup="this.value=parseArabic(this.value)" value="{{$estate->size}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mt-4">
                    <label>تعداد اتاق خواب</label>
                    <select class="form-control" name="room">
                        @for($i = 0;$i <= 6;$i++)
                            <option value="{{$i}}" @if($i == $estate->room) selected @endif>
                                @if(!$i)    ندارد
                                @elseif($i == 6) بیشتر از 5
                                @else {{$i}}
                                @endif
                            </option>
                        @endfor
                    </select>
                </div>

                <div class="col-md-6 mt-4">
                    <label>حداکثر ظرفیت</label>
                    <input class="form-control p-4" name="capacity" type="text" placeholder="0" onkeyup="this.value=parseArabic(this.value)" value="{{$estate->capacity}}">
                </div>

                <div class="col-md-12 mt-4">
                    <label>توضیحات</label>
                    <textarea class="form-control p-4" name="description" type="text" placeholder="توضیحات">{!! strip_tags($estate->description) !!}</textarea>
                </div>
                <hr>
            </div>


                <div class="col-md-12 mt-5 mb-4">
                    <div id="caleandar">
                    </div>

                </div>
                <hr>

                <div class="clearfix"></div>
                <div class="demo-radio-button">
                    <div>
                        <div>
                            <div class="col-sm-3">
                                <input name="apartment" type="radio" id="apartment" value="apartment" @if($estate->apartment == 'apartment') checked="" @endif class="with-gap radio-col-pink" />
                                <label for="apartment">@lang('estate::default.apartment')</label>
                            </div>
                            <div class="col-sm-3">
                                <input name="apartment" type="radio" id="villa" value="villa" @if($estate->apartment == 'villa') checked="" @endif class="with-gap radio-col-pink" />
                                <label for="villa">@lang('estate::default.villa')</label>
                            </div>
                            <div class="col-sm-3">
                                <input name="apartment" type="radio" id="suite" value="suite" @if($estate->apartment == 'suite') checked="" @endif class="with-gap radio-col-pink" />
                                <label for="suite">@lang('estate::default.suite')</label>
                            </div>
                            <div class="col-sm-3">
                                <input name="apartment" type="radio" id="rural" value="rural" @if($estate->apartment == 'rural') checked="" @endif class="with-gap radio-col-pink" />
                                <label for="rural">@lang('estate::default.rural')</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>


                <div class="col-md-12">
                    <h4 class="mt-4 type"> امکانات اقامتگاه</h4>
                    <div class="form-group form-float">
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="forest" @if($estate->forest) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.forest')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="jac" @if($estate->jac) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.jac')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="furnished" @if($estate->furnished) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.furnished')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="shore" @if($estate->shore) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.shore')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="pool" @if($estate->pool) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.pool')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="exclusive" @if($estate->exclusive) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.exclusive')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="parking" @if($estate->parking) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.parking')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="switch">
                                <label><input name="elevator" @if($estate->elevator) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span>@lang('estate::default.elevator')</label>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group form-float">
                                <label class="form-label">امکانات <span class="col-red">*</span></label>
                                <select name="features[]" id="features" class="form-control" multiple>
                                    @foreach($features as $feature)
                                        <option value="{{$feature->id}}" @if($estate->features->contains($feature)) selected @endif>{{$feature->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <hr>
            <div class="col-md-12">
                <h3>رزرو:</h3>
                <p class="text-right">برای ویرایش قیمت یک روز خاص روی آن کلیک کنید</p>
                <input type="hidden" name="calendar">
                <div id="calendar">
                </div>

            </div>

            <div class="col-md-12">
                <h4 class="mt-5 type">آپلود تصویر اصلی</h4>
                <div class="col-sm-4 col-center text-center">
                    @include('front::dashboard.partials._single_fileinput', [
                        'field_name' => 'image',
                        'old_image' => $estate
                    ])
                </div>
            </div>

            <input type="hidden" name="lat" id="lat" value="{{$estate->lat}}">
            <input type="hidden" name="lng" id="lng" value="{{$estate->lng}}">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="mt-5 type">آپلود گالری تصاویر </h4>
                    <div>
                        <input class="product_images" type="file" name="images[]" id="gallery" multiple>
                        @include('front::dashboard.partials._multiple_fileinput', [
                            'field_name' => 'gallery',
                            'gallery'   => $gallery
                        ])
                    </div>
                </div>
                <div class="col-md-12">
                    @include('front::dashboard.partials._googlemap', [
                        'lat' => $estate->lat,
                        'lng' => $estate->lng
                    ])
                </div>
            </div>
        </div>


        <div class="col-xs-12">
            <p>ثبت اقامتگاه به منزله تایید <a class="col-blue" target="_blank" href="{{route('front.rules')}}">قوانین و مقررات</a> الوویلا میباشد</p>
        </div>
        <input type="submit" value="ثبت اطلاعات وآگهی" class="submit-reviwe register-btn pr-5 pl-5 mt-5">
        </form>
    </div>

    @include('front::dashboard.partials._calendar_modal')
    @include('front::dashboard.partials._guide_modal')
@endsection

@section('scripts')
    @include('front::dashboard.partials._estate_scripts')
    @include('front::dashboard.partials._calendar_assets')

    <script>
        var now = "{{$now}}";

        <?php
        echo 'var events = '.json_encode($events);
        ?>

        // if(!events.length) {
        //     console.log(events);
        //     initialCalendar();
        // }

        initialCalendar();
        setFullCalendar();

        {{--console.log("{{$events}}");--}}
        console.log(now)
        function setFullCalendar() {
            $('#calendar').fullCalendar({
                lang: 'fa',
                isJalaali: true,
                selectable: true,
                isRTL: true,
                defaultDate: now,

                eventLimit: true, // allow "more" link when too many events
                events: events,
                droppable: false,
                editable: false,

                allDays:false,

                dayClick: function(date, jsEvent, view, resource) {

                    data = date.format();
                    var selected_date = convertToEn(date.format());

                    $("input[name=selected_date]").val(selected_date);
                    selected_event = events.filter(e => e.start == selected_date);
                    if(selected_event.length){
                        $("input[name=date_price]").val(selected_event[0].price)
                        $("input[name=active_date]").prop("checked",selected_event[0].active)
                    }


                    $("#calendarModal").modal("show");
                },
                select: function(startDate, endDate, jsEvent, view, resource) {

                }
            });
        }




        function convertToEn(str) {
            var fa_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            var en_numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];


            fa_numbers.forEach(function (fa_num, key) {
                str = str.replace(new RegExp(fa_num, 'g'), en_numbers[key]);
            });

            return str;
        }

        function initialCalendar(){
            if(!events.length) {

                last_day = moment(now, 'YYYY-MM-D').add(4, 'month').format('YYYY-MM-D');
                last_day = convertToEn(last_day);
                date = now;
                // date = moment(now, 'YYYY-MM-D').add(1, 'day').format('YYYY-MM-D');
                date = convertToEn(date);

                while(date <= last_day){

                    var day_weekend = moment(date).day();
                    events.push({
                            title: 'قیمت:' ,
                            start: date,
                            active:1,
                            reserve:0,
                            price:0,
                            day_weekend : day_weekend,
                            backgroundColor:"#337ab7",
                            resourceEditable: false,
                        }
                    )
                    date = moment(date, 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD');
                    date = convertToEn(date);
                }


            } else {
                events.forEach(function (event,key) {
                    event.title = "قیمت :" + event.price;
                    event.resourceEditable = false;

                    if(event.active == "1") {
                        if (event.reserve == "1") {
                            event.title = "قیمت :" + event.price + " رزرو شده";
                            event.backgroundColor = "#15b70f";
                        }
                    } else {
                        event.title = "غیر فعال";
                        event.backgroundColor="#ff484a !important"
                    }

                });
            }


            // setFullCalendar();
            $('input[name=calendar]').val(JSON.stringify(events));


        }

        $("input[name=price]").on("change",function (e) {
            var price = convertToEn($(this).val());
            $('#calendar').fullCalendar( 'removeEventSource', events );

            events.forEach(function (event,key) {
                if(event.reserve != "1") {
                    event.price = price;
                    event.title = "قیمت :"+ price;
                    event.backgroundColor="#337ab7";
                    event.active = 1;
                }
            });

            $('#calendar').fullCalendar( 'addEventSource', events );
            $('input[name=calendar]').val(JSON.stringify(events));
        });

        $("input[name=friday_price]").on("change",function (e) {
            var price = convertToEn($(this).val());
            $('#calendar').fullCalendar( 'removeEventSource', events );

            events.forEach(function (event,key) {
                if(event.reserve != "1") {
                    if (event.day_weekend == 4 || event.day_weekend == 5) {
                        event.price = price;
                        event.title = "قیمت :" + price;
                        event.backgroundColor = "#337ab7";
                        event.active = 1;
                    }
                }

            });

            //
            $('#calendar').fullCalendar( 'addEventSource', events );
            $('input[name=calendar]').val(JSON.stringify(events));
        });

        $("#add-price").on("click",function (event) {
            var price = convertToEn($("input[name=date_price]").val());

            if(price == "")
                price = 0;

            var selected_date = $("input[name=selected_date]").val();
            var active = $("input[name=active_date]").prop("checked");

            events.forEach(function (event,key) {
                if(event.start == selected_date && event.reserve == "0"){

                    if(active) {
                        event.price = price;
                        event.title = "قیمت :"+ price;
                        event.backgroundColor="#337ab7";
                        event.active = 1;
                    } else {
                        event.price = 0;
                        event.title = "غیر فعال";
                        event.active = 0;
                        event.backgroundColor="#ff484a !important"
                    }

                }

            });

            $('#calendar').fullCalendar( 'removeEventSource', events );
            $('#calendar').fullCalendar( 'addEventSource', events );
            $('input[name=calendar]').val(JSON.stringify(events));
        });

    </script>
@endsection