@extends('front::layouts.dashboard')

@section('title')
    پروفایل کاربری
@endsection

@section('panel_content')
    <div class="tab-content p-4">
        <h4 class="p-4"> <i class="fas fa-user-edit"></i> ثبت آگهی</h4>
        <div>
            <h4>لطفا ابتدا شماره کارت ملی خود را از طریق صفحه

                <a class="col-blue" href="{{route('dashboard.profile')}}">ویرایش پروفایل</a>
            ثبت کنید.
            </h4>
        </div>
    </div>
@endsection