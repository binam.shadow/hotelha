@extends('front::layouts.dashboard')

@section('title')
 ثبت آگهی
@endsection



@section('panel_content')
    <div class="tab-content p-4" id="register">
        <div class="row p-4">
            <h4 class="p-4 mb-5"><i class="fas fa-plus-square"></i> ثبت آگهی</h4>
            <p>تمام مراحل ثبت اقامتگاه و انتشار آن در وبسایت, کاملا رایگان می باشد.</p>
            <p class="guid">جهت اطلاع از معانی نماد و رنگ های جدول روی <a data-toggle="modal" data-target="#guid"> مدیریت راهنما جدول </a> کلیک کنید </p>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
           <form id="estateForm" method="post" action="{{route('dashboard.ad.create')}}" enctype="multipart/form-data">
               {{csrf_field()}}
            <div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 mt-4">
                        <label>نام استان</label>
                        <select name="province" class="form-control disabled">
                            @foreach($provinces as $province)
                                <option value="{{$province->id}}">{{$province->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 mt-4">
                        <label>نام شهر</label>
                        <select name="city" class="form-control nav disabled">
                            @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6  mt-4">
                        <label>نام روستا</label>
                        <input class="p-4" name="village_name" value="{{old('village_name')}}" type="text" placeholder="گرمدره">
                    </div>
                    <div class="col-md-6 mt-4">
                        <label>عنوان اقامتگاه</label>
                        <input class="p-4" type="text" name="name" value="{{old('name')}}" placeholder="ویلا اجاره ای کوهستان" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt-3">
                        <label>اجاره بهاء روزانه (تومان)</label>
                        <input class="p-4" name="price" type="text" value="{{old('price')}}" onkeyup="this.value=parseArabic(this.value)" required>
                    </div>

                    <div class="col-md-6 mt-3">
                        <label>اجاره بهاء در روزهای پنجشنبه و جمعه (تومان)</label>
                        <input class="p-4" name="friday_price" value="{{old('friday_price')}}" onkeyup="this.value=parseArabic(this.value)" type="text" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mt-4">
                        <label>فاصله اقامتگاه از دریا (متر)</label>
                        <input class="p-4" name="distancetosea" value="{{old('distancetosea')}}" onkeyup="this.value=parseArabic(this.value)" type="text">
                    </div>
                    <div class="col-md-6  mt-4">
                        <label>متراژ کل (مترمربع)</label>
                        <input class="p-4" name="size" value="{{old('size')}}" onkeyup="this.value=parseArabic(this.value)" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6  mt-4">
                        <label>تعداد اتاق خواب</label>
                        <select class="form-control" name="room">
                            @for($i = 0;$i <= 6;$i++)
                                <option value="{{$i}}">
                                    @if(!$i)    ندارد
                                    @elseif($i == 6) بیشتر از 5
                                    @else {{$i}}
                                    @endif
                                </option>
                            @endfor
                        </select>
                    </div>

                    <div class="col-md-6 mt-4">
                        <label>حداکثر ظرفیت (نفر)</label>
                        <input class="form-control p-4" name="capacity" value="{{old('capacity')}}" onkeyup="this.value=parseArabic(this.value)" type="text" placeholder="0">
                    </div>

                    <div class="col-md-12 mt-4">
                        <label>توضیحات</label>
                        <textarea class="form-control p-4" name="description" type="text" placeholder="توضیحات">{{old('description')}}</textarea>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mt-4 type">نوع اقامتگاه</h4>
                        <div class="row">
                            <div>
                                <div class="col-sm-3">
                                    <input name="apartment" id="apartment" value="apartment" class="with-gap radio-col-pink" checked="" type="radio">
                                    <label for="apartment">آپارتمان</label>
                                </div>
                                <div class="col-sm-3">
                                    <input name="apartment" id="villa" value="villa" class="with-gap radio-col-pink" type="radio">
                                    <label for="villa">ویلایی</label>
                                </div>
                                <div class="col-sm-3">
                                    <input name="apartment" id="suite" value="suite" class="with-gap radio-col-pink" type="radio">
                                    <label for="suite">سوئیت</label>
                                </div>
                                <div class="col-sm-3">
                                    <input name="apartment" id="rural" value="rural" class="with-gap radio-col-pink" type="radio">
                                    <label for="rural">خانه روستایی</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="form-group form-float">
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="forest" disabled="disabled" type="checkbox"><span class="lever switch-col-blue"></span>جنگلی                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="jac" type="checkbox"><span class="lever switch-col-blue"></span>جکوزی                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="furnished" type="checkbox"><span class="lever switch-col-blue"></span>مبله                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="shore" disabled="disabled" type="checkbox"><span class="lever switch-col-blue"></span>ساحلی                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="pool" type="checkbox"><span class="lever switch-col-blue"></span>استخر                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="exclusive" type="checkbox"><span class="lever switch-col-blue"></span>دربست                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="parking" type="checkbox"><span class="lever switch-col-blue"></span>پارکینگ                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch">
                                        <label><input name="elevator" type="checkbox"><span class="lever switch-col-blue"></span>آسانسور                                    </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">امکانات <span class="col-red">*</span></label>
                                    <select name="features[]" id="features" class="form-control" multiple>
                                        @foreach($features as $feature)
                                            <option value="{{$feature->id}}">{{$feature->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="col-md-12">
                    <h3>رزرو:</h3>
                    <p class="text-right">برای ویرایش قیمت یک روز خاص روی آن کلیک کنید</p>
                    <input type="hidden" name="calendar">
                    <div id="calendar">
                    </div>

                </div>

                <div class="col-md-12">
                    <h4 class="mt-5 type">آپلود تصویر اصلی </h4>
                    <div class="col-sm-4 col-center text-center">
                        @include('front::dashboard.partials._single_fileinput', [
                            'field_name' => 'image'
                        ])
                    </div>
                </div>

                <div class="col-md-12">
                    <h4 class="mt-5 type">آپلود گالری تصاویر </h4>
                    <input type="file" name="gallery[]" id="gallery" multiple>
                    @include('front::dashboard.partials._multiple_fileinput')
                </div>
                @include('front::dashboard.partials._googlemap', [
                    'lat' => 35.6892,
                    'lng' => 51.3890
                ])
            </div>

           <div class="col-xs-12">
               <p class="text-right">ثبت اقامتگاه به منزله تایید <a class="col-blue" target="_blank" href="{{route('front.rules')}}">قوانین و مقررات</a> الوویلا میباشد</p>
           </div>
            <button type="submit" class="submit-reviwe register-btn pr-5 pl-5 mt-5">ثبت اطلاعات وآگهی</button>
        </form>
    </div>


    @include('front::dashboard.partials._calendar_modal')
    @include('front::dashboard.partials._guide_modal')
@endsection

@section('scripts')
    @include('front::dashboard.partials._estate_scripts')
    @include('front::dashboard.partials._calendar_assets')


    <script>
        var events =  [];
        var now = "{{$now}}";
        $(document).ready(function() {

            $('#calendar').fullCalendar({
                lang: 'fa',
                isJalaali: true,
                selectable: true,
                isRTL: true,
                defaultDate: now,
                eventLimit: true,
                events: initialCalendar(),
                droppable: false,
                editable: false,
                allDays:false,
                dayClick: function(date, jsEvent, view, resource) {

                    data = date.format();
                    var selected_date = convertToEn(date.format());

                    $("input[name=selected_date]").val(selected_date);
                    selected_event = events.filter(e => e.start == selected_date);
                    if(selected_event.length){
                        $("input[name=date_price]").val(selected_event[0].price)
                        $("input[name=active_date]").prop("checked", selected_event[0].active)
                    }

                    $("#calendarModal").modal("show");
                },
                select: function(startDate, endDate, jsEvent, view, resource) {

                }
            });


            function convertToEn(str) {
                var fa_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                var en_numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                fa_numbers.forEach(function (fa_num, key) {
                    str = str.replace(new RegExp(fa_num, 'g'), en_numbers[key]);
                });
                return str;
            }

            function initialCalendar(){
                last_day = moment(now, 'YYYY-MM-D').add(4, 'month').format('YYYY-MM-D');
                last_day = convertToEn(last_day);
                date = now;
                // date = moment(now, 'YYYY-MM-D').add(1, 'day').format('YYYY-MM-D');
                date = convertToEn(date);

                while(date <= last_day){

                    var day_weekend = moment(date).day();
                    events.push({
                            title: 'قیمت:' ,
                            start: date,
                            active:1,
                            reserve:0,
                            price:0,
                            day_weekend : day_weekend,
                            backgroundColor:"#337ab7",
                            resourceEditable: false,
                        }
                    )
                    date = moment(date, 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD');
                    date = convertToEn(date);
                }
//             $('input[name=calendar]').val(JSON.stringify(events));
                return events;
            }

            $("input[name=price]").on("change",function (e) {
                var price = $(this).val();
                $('#calendar').fullCalendar('removeEventSource', events );

                events.forEach(function (event,key) {
                    event.price = price;
                    event.title = "قیمت :"+ price;
                    event.backgroundColor="#337ab7";
                    event.active = 1;
                });

                $('#calendar').fullCalendar( 'addEventSource', events );
                $('input[name=calendar]').val(JSON.stringify(events));
            });

            $("input[name=friday_price]").on("change",function (e) {
                var price = $(this).val();
                $('#calendar').fullCalendar( 'removeEventSource', events );

                events.forEach(function (event,key) {

                    if(event.day_weekend == 4 || event.day_weekend == 5) {
                        event.price = price;
                        event.title = "قیمت :"+ price;
                        event.backgroundColor="#337ab7";
                        event.active = 1;
                    }

                });

                //
                $('#calendar').fullCalendar( 'addEventSource', events );
                $('input[name=calendar]').val(JSON.stringify(events));
            });

            $("#add-price").on("click",function (event) {
                var price = $("input[name=date_price]").val();

                if(price == "")
                    price = 0;

                var selected_date = $("input[name=selected_date]").val();
                var active = $("input[name=active_date]").prop("checked");

                events.forEach(function (event,key) {
                    if(event.start == selected_date ){
                        if(active) {
                            event.price = price;
                            event.title = "قیمت :"+ price;
                            event.backgroundColor="#337ab7";
                            event.active = 1;
                        } else {
                            event.price = 0;
                            event.title = "غیر فعال";
                            event.active = 0;
                            event.backgroundColor="#ff484a"
                        }
                    }
                });

                $('#calendar').fullCalendar( 'removeEventSource', events );
                $('#calendar').fullCalendar( 'addEventSource', events );
                $('input[name=calendar]').val(JSON.stringify(events));
            });

            $("input[name=price]").trigger('change');
            $("input[name=friday_price]").trigger('change');
        });

    </script>
@endsection