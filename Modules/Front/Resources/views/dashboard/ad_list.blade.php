@extends('front::layouts.dashboard')

@section('title')
    لیست آگهی ها
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection


@section('panel_content')
    <div class="tab-content p-4">
        <div class="row p-4">
            <h4 class="p-4 mb-5"> <i class="fas fa-archive"></i> آگهی های من</h4>
            @if (Session::has('msg'))
                <div class="alert alert-success">{{ Session::get('msg') }}</div>
            @endif
            @foreach($estates as $estate)
                <div class="col-sm-6 col-md-6 col-lg-6 filter north-villa mb-5" id="ad{{$estate->id}}">
                    <div class="item-hotel p-2">
                        <div class="img-hotel">
                            <img src="{{asset('uploads/'.$estate->image)}}" class="img-responsive" style="width: 100%;">
                            @if($estate->active)
                            <span class="pr-5 pl-5 pt-2 pb-2">تایید شده</span>
                            @else
                            <span class="pr-5 pl-5 pt-2 pb-2">تایید نشده</span>
                            @endif
                        </div>
                        <div class="desc-hotel">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12 text-center pt-2">
                                    <span>قیمت </span><br>
                                    <i class="fas fa-money-bill"></i><br>
                                    <span>{{number_format($estate->price)}}</span>
                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-12 text-center pt-2">
                                    <span>عنوان </span><br>
                                    <i class="fas fa-building"></i><br>
                                    <span>{{$estate->name}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <a class="submit-edit submit-special py-3 mt-4" href="#specialAd" onclick="specialAd({{$estate->id}})">ویژه کردن آگهی</a>
                        </div>
                        <div class="col-xs-6">
                            <a class="submit-edit p-3 mt-4" href="{{route('dashboard.ad.edit', ['estate' => $estate])}}">ویرایش آگهی</a>
                        </div>
                        <div class="col-xs-6">
                            <a class="submit-edit submit-delete p-3 mt-4" href="#deleteAd" onclick="showConfirmDelete({{$estate->id}})">حذف آگهی</a>
                         </div>
                        <div class="col-xs-6">
                            <a class="submit-edit p-3 mt-4" href="{{route('dashboard.ad.reserves', ['estate' => $estate])}}">تاریخچه رزرو</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div id="specialModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ویژه کردن آگهی</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('dashboard.ad.special')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="id">
                        <div class="row">
                            @foreach($plans as $plan)
                                <div class="clearfix col-sm-12 text-right">
                                    <p class="text-right">
                                        <input name="plan" type="radio" id="plan{{$plan->id}}" value="{{$plan->id}}" checked="" class="with-gap radio-col-pink" />
                                        <label for="plan{{$plan->id}}"><img src="{{'http://alovilla.org/uploads/'.$plan->image}}" width="30px" alt="plan"> {{$plan->name}} [ قیمت : {{number_format($plan->price / 10)}} تومان ]</label>
                                    </p>
                                    <div class="mb-3">{{$plan->description}}</div>
                                    <hr>
                                </div>
                            @endforeach
                            <div class="col-xs-12">
                                <button class="btn btn-block btn-purple">ثبت و پرداخت</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        function showConfirmDelete(id){
            swal({
                title: "@lang('product::default.are_you_sure')",
                text: "@lang('product::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('product::default.cancel')",
                confirmButtonText: "@lang('product::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "{{ route('dashboard.ad.destroy') }}/"+id,
                        processData: false,
                        contentType: false,
                        type: 'GET',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('product::default.success_delete')", "@lang('product::default.success_delete_msg')", "success");
                                $("#ad"+id).fadeOut();
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }

        function specialAd(id) {
            $('input[name=id]').val(id);
            $('#specialModal').modal('show');
        }
    </script>

@endsection