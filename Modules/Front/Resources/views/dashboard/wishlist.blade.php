@extends('front::layouts.dashboard')

@section('title')
  لیست علاقه مندی ها
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection


@section('panel_content')
    <div class="tab-content p-4">
        <div class="row p-4">
            <h4 class="p-4 mb-5"> <i class="fas fa-archive"></i>  لیست علاقه مندی ها</h4>
            @if (Session::has('msg'))
                <div class="alert alert-success">{{ Session::get('msg') }}</div>
            @endif
        </div>

        <div class="row">
            @foreach($estates as $estate)
                @include('front::partials._estate_card', ['col_lg' => 'col-lg-4'])
            @endforeach
        </div>
    </div>
@endsection