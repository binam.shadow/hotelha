@extends('front::layouts.dashboard')

@section('title')
    @if($type == 'owner')
        تاریخچه رزرو [ {{$estate->name}} ]
    @else
    رزروهای من
    @endif
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
    <style>
        .setting-column{
            overflow: visible !important;
        }
        .actions-group .dropdown-menu{
            right: -120px;
            min-width: 220px;
        }
        .actions-group .dropdown-menu .divider {
            margin: 1px;
        }
        .table-responsive{
            overflow: visible;
        }
        /*** Customize Advance DataTable ***/
        .table-responsive{
            overflow-x: initial !important;
        }
        .actions-group .dropdown-menu {
            top: auto !important;
        }
        .filter-search{
            padding-right: 5px !important;
        }
        .dataTable .form-control{
            width: 100%;
            box-shadow: none;
        }
        .dataTable .select2-container{
            margin-bottom: 0px;
        }
        .dataTables_wrapper .bootstrap-select .dropdown-toggle{
            display: block !important;
            border-bottom: 1px solid #CECECE !important;
            padding-right: 5px !important;
        }
        .dataTables_wrapper .bootstrap-select .btn:not(.btn-link):not(.btn-circle) span.bs-caret {
            left: 20px;
        }
        .dataTables_processing {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 100%;
            height: 40px;
            margin-left: -50%;
            margin-top: -25px;
            padding-top: 20px;

            text-align: center;
            font-size: 1.2em;

            background-color: white;
        }
        div.slider {
            display: none;
        }
        table.dataTable tbody td.no-padding {
            padding: 0;
        }
        .dataTables_wrapper tr.selected {
            background: #DEDEDE !important;
        }
        .dataTables_wrapper tr.selected td:nth-child(1){
            border-right: 4px solid #0067CC;
        }
        .dataTables_length{
            float: right !important;
            margin-bottom: 15px;
        }

        .dataTableLayout {
            table-layout:fixed;
            width:100%;
        }
        .dropdown-menu{
            margin-right: -35px;
        }
        .dataTables_paginate {
            text-align: left !important;
        }
        div.slider {
            display: none;
        }

        table.dataTable tbody td.no-padding {
            padding: 0;
        }
        .info-icon {
            font-size: 15px !important;
            background: linear-gradient(to bottom, #7944ee, #f860f0);
            padding: 5px 10px;
            border-radius: 50%;
            color: #FFF !important;
        }
        .reserve-more-info{
            font-size: 12px !important;
        }
    </style>
@endsection

@section('panel_content')
<div class="tab-content p-4">
    @if($type == 'owner')
    <h4 class="p-4"> <i class="fas fa-archive"></i> تاریخچه رزرو [ {{$estate->name}} ]</h4>
    @else
    <h4 class="p-4"> <i class="fas fa-archive"></i> رزروهای من</h4>
    @endif
    <div>
        <div>
            <div class="table-responsive">
                <table class="table" id="reserveList">
                    <thead>
                        <th></th>
                        <th>شماره واچر</th>
                        <th>نام اقامتگاه</th>
                        <th>تاریخ ثبت</th>
                        <th> مجموع مبلغ (تومان)</th>
                        <th>وضعیت</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @include('adminbsb.layouts.datatable_files')
    @include('front::dashboard.partials._comment_modal')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>
        function format ( d ) {
            // `d` is the original data object for the row
            return '<div class="slider">' +
                ' <table class="table table-bordered" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                    '<tr>'+
                        '<td>تاریخ شروع:</td>'+
                        '<td>'+d.from_date+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>تاریخ پایان:</td>'+
                        '<td>'+d.to_date+'</td>'+
                    '</tr>'+
                    @if($type == 'user')
                    '<tr>'+
                        '<td>نام مالک:</td>'+
                        '<td>'+d.estate.user.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>شماره مالک:</td>'+
                        '<td>'+d.estate.user.phone+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td colspan="2"><button class="submit-edit submit-special p-3" onclick="sendComment('+d.estate.id+')">دیدگاه شما در مورد این اقامتگاه</button></td>'+
                    '</tr>'+
                    @else
                    '<tr>'+
                        '<td>نام میمان:</td>'+
                        '<td>'+d.user.name+'</td>'+
                    '</tr>'+
                    @endif
                '</table>' +
                '</div>';
        }
        var dataTable = $('#reserveList').DataTable({
            ajax: {
                @if($type == 'owner')
                url: '{{ route('dashboard.reservation.dataList') }}?type={{$type}}&id={{$estate->id}}'
                @else
                url: '{{ route('dashboard.reservation.dataList') }}?type={{$type}}'
                @endif
            },
            order: [[ 0, "desc" ]],
            responsive: true,
            serverSide: true,
            processing: true,
            searching: false,
            columns: [
                {
                    "class": 'details-control text-center',
                    "orderable": false,
                    data: 'id',
                    render:  function(data, type, row) {
                        return '<a class="reserve-more-info" href="#reserveInfo">' +
                            '<i class="fa fa-info info-icon"></i><p class="text-center">مشاهده جزییات</p></a>';
                    }
                },
                {data: 'code'},
                {
                    data: 'estate.name',
                    name:'estate.name',
                    render: function(data, type, row) {
                        return data +'<br>'+ ' کد آگهی: ' + row.estate.id;
                    },
                    "orderable": false
                },
                {data: 'reserve_date', name:'reserve_date'},
                {
                    data: 'discount_price',
                    render: function(data, type, row) {
                        return numberFormat(data);
                    }
                },
            ],
            columnDefs: [
                {
                    targets: 5,
                    render: function(data, type, row) {
                        var btns = row.status_label;
                        if(row.status == 0)
                            btns = '<button class="btn btn-sm btn-c-danger" onclick="showConfirmCancel('+row.id+')">کنسل کردن</button>';

                        return btns;
                    }
                }
            ],
            buttons: [
                'copyHtml5', 'excelHtml5', 'print'
            ],
            language:{
                url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
            }
        });

        var detailRows = [];

        $('#reserveList tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = dataTable.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                $('div.slider', row.child()).slideUp( function () {
                    row.child.hide();
                    tr.removeClass('shown');
                } );
            }
            else {
                // Open this row
                row.child( format(row.data()), 'no-padding' ).show();
                tr.addClass('shown');

                $('div.slider', row.child()).slideDown();
            }
        });

        function showConfirmCancel(id) {
            swal({
                @if($type == 'user')
                title: "میهمان ارجمند",
                text: "۱.اگر بیشتر از ۷۲ ساعت به موعد رزرو باقی مانده باشد ۱۰٪ کارمزد از مبلغ پرداختی کسر و بقیه مبلغ عودت داده میشود.\n" +
                "۲.اگر کمتر از ۷۲ ساعت به موعد رزرو باقی مانده باشد اجاره شب اول کسر میشود و مابقی عودت داده میشود.\n" +
                "۳.اگر از ساعت صفر بامداد موعد رزرو گذشته باشد هیچ  مبلغی عودت داده نمیشود و اقامتگاه برای شما خالی میماند.",
                @else
                title: "مالک محترم",
                text: "لغو رزرو برای شما نمره منفی در بر دارد و در صورت تکرار باعث حذف شما از سامانه می شود",
                @endif
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('product::default.cancel')",
                confirmButtonText: "@lang('product::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: '{{route('dashboard.reservation.changeStatus')}}',
                        method: 'GET',
                        data: {
                            id: id,
                            user_type: '{{$type}}'
                        },
                        success: function(result) {
                            if(!result.error) {
                                swal(result.msg, "", "success");
                                dataTable.ajax.reload(null, false)
                            } else {
                                swal(result.msg, "", "success");
                                dataTable.ajax.reload(null, false)
                            }
                        }
                    });
            });
        }
    </script>
@endsection