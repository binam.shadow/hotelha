@extends('front::layouts.dashboard')

@section('title')
    لیست آگهی ها
@endsection

@section('panel_content')
    <div class="tab-content p-4">
        <div class="row  p-4">
            <h4 class="p-4 mb-5"> <i class="fas fa-archive"></i> آگهی های من</h4>
            <div class="col-sm-6 col-md-4 col-lg-4 filter north-villa mb-5">
                <div class="item-hotel p-2">
                    <div class="img-hotel">
                        <img src="img/villa2.jpg" class="img-responsive">
                        <span class="pr-5 pl-5 pt-2 pb-2">منتظر تایید</span>
                    </div>
                    <div class="desc-hotel">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 text-center pt-2">
                                <span>قیمت </span><br>
                                <i class="fas fa-money-bill"></i><br>
                                <span>200.000</span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 text-center pt-2">
                                <span>عنوان </span><br>
                                <i class="fas fa-building"></i><br>
                                <span> سوییت دنا</span>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="submit-edit p-3 mt-4" href="edit.html">ویرایش آگهی</a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 filter north-villa mb-5">
                <div class="item-hotel p-2">
                    <div class="img-hotel">
                        <img src="img/villa3.jpg" class="img-responsive">
                        <span class="pr-5 pl-5 pt-2 pb-2">تایید شده</span>
                    </div>
                    <div class="desc-hotel">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 text-center pt-2">
                                <span>قیمت </span><br>
                                <i class="fas fa-money-bill"></i><br>
                                <span>200.000</span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 text-center pt-2">
                                <span>عنوان </span><br>
                                <i class="fas fa-building"></i><br>
                                <span> سوییت دنا</span>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="submit-edit p-3 mt-4" href="edit.html">ویرایش آگهی</a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 filter north-villa mb-5">
                <div class="item-hotel p-2">
                    <div class="img-hotel">
                        <img src="img/villa4.jpg" class="img-responsive">
                        <span class="pr-5 pl-5 pt-2 pb-2">تایید شده</span>
                    </div>
                    <div class="desc-hotel">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 text-center pt-2">
                                <span>قیمت </span><br>
                                <i class="fas fa-money-bill"></i><br>
                                <span>200.000</span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 text-center pt-2">
                                <span>عنوان </span><br>
                                <i class="fas fa-building"></i><br>
                                <span> سوییت دنا</span>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="submit-edit p-3 mt-4" href="edit.html">ویرایش آگهی</a>
            </div>
        </div>
    </div>
@endsection