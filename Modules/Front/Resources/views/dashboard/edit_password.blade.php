@extends('front::layouts.dashboard')

@section('title')
   تغییر رمز عبور
@endsection

@section('panel_content')
    <div class="tab-content p-4 mb-5">
        <h4 class="p-4"> <i class="fas fa-user-edit"></i>   تغییر رمز عبور</h4>
        @if (Session::has('msg'))
            <div class="alert alert-success">{{ Session::get('msg') }}</div>
        @endif
        <form id="passwordForm" method="post" action="{{route('dashboard.editPassword')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6 right col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>رمزعبور فعلی: </label>
                        <input type="text" name="current_password" placeholder="رمزعبور فعلی">
                        <p class="text-danger">{{$errors->first('current_password')}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>رمزعبور جدید: </label>
                        <input id="password" name="password" type="text" placeholder="رمزعبور جدید">
                        <p class="text-danger">{{$errors->first('password')}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>تکرار رمزعبور جدید: </label>
                        <input name="password_confirmation" type="text" placeholder="تکرار رمزعبور جدید">
                        <p class="text-danger">{{$errors->first('password_confirmation')}}</p>
                    </div>
                </div>
            </div>
            <input type="submit" value="ویرایش رمز عبور" class="submit-reviwe pr-5 pl-5 mt-5">
        </form>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>

    <script>
        $("#passwordForm").validate({
            errorClass: "error",
            rules: {
                current_password: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#password"
                },
            }
        });
    </script>
@endsection