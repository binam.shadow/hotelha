@extends('front::layouts.dashboard')

@section('title')
گردش حساب
@endsection

@section('panel_content')
    <div class="tab-content p-4">
        <div class="row p-4">
            <h4 class="p-4"> <i class="fas fa-money-bill"></i> گردش حساب
                <span class="p-4"> موجودی حساب: <b>{{number_format($price)}} تومان</b></span> </h4>

            <div style="overflow-x:auto">
                <div class="t-reservation text-center">
                    <div class="r-reservation header-table">
                        <div class="td-reservation">
                            <span>تاریخ تراکنش</span>
                        </div>
                        <div class="td-reservation">
                            <span>بابت</span>
                        </div>
                        <div class="td-reservation">
                            <span>شماره پیگیری</span>
                        </div>
                        <div class="td-reservation">
                            <span>کدواچر</span>
                        </div>
                        <div class="td-reservation">
                            <span>مبلغ پرداخت شده</span>
                        </div>
                        @if($type == 'owner')
                        <div class="td-reservation">
                            <span>وضعیت </span>
                        </div>
                        @endif
                        <div class="td-reservation">
                            <span>نوع تراکنش</span>
                        </div>
                    </div>
                    @foreach($wallets as $key => $wallet)
                        <div class="r-reservation">
                            <div class="td-reservation">
                                <span>{{jDate::forge($wallet->created_at)->format('%d %B %Y H:i:s')}}</span>
                            </div>
                            <div class="td-reservation">
                                <span>{{$wallet->description}}</span>
                            </div>
                            <div class="td-reservation">
                                <span>{{$wallet->tracking_code}}</span>
                            </div>
                            <div class="td-reservation">
                                <span>{{$wallet->code}}</span>
                            </div>
                            <div class="td-reservation">
                                <span> {{number_format($wallet->price)}} تومان </span>
                            </div>
                            @if($type == 'owner')
                            <div class="td-reservation">
                                <span>
                                    @if($wallet->status)
                                        تسویه شده
                                    @else
                                        در انتظار تسویه
                                    @endif
                                </span>
                            </div>
                            @endif
                            <div class="td-reservation">
                                @if($wallet->is_plus)
                                <span class="text-success">به کیف پول اضافه شده</span>
                                @else
                                <span class="text-danger">از کیف پول کم شده</span>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                {{$wallets->links()}}
            </div>
        </div>
    </div>
@endsection