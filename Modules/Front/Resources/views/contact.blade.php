@extends('front::layouts.master')

@section('title')
    تماس با ما
@endsection

@section('content')
    @include('front::partials._nav_menu')
    <section class="contact-us py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 form-contact p-5">
                    <h3 class="text-center">راه های ارتباط با ما</h3><br/>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <i class="fab fa-telegram-plane border-contact"></i>
                            <span class="font">@alovilla_admin</span>
                        </div>

                        <div class="col-md-4 text-center">
                            <i class="fas fa-envelope border-contact"></i>
                            <span class="font">info@alovilla.ir</span>
                        </div>
                        <div class="col-md-4 text-center">
                            <i class="fas fa-phone border-contact"></i>
                            <span>۰۲۱-۲۸۴۲۱۴۲۰</span>
                        </div>

                    </div>
                    <br/>
                    <p class="text-center mb-5">هر گونه نظر, پیشنهاد یا انتقاد شما می تواند ما را در ارائه خدمات بهتر یاری نماید.</p>
                    <div class="alert alert-success" id="successSend" style="display: none;">
                       اطلاعات با موفقیت ارسال شد، پس از بررسی با شما تماس خواهیم گرفت.
                    </div>
                    <form id="contactForm" method="post">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 right">
                                <input name="title" type="text" placeholder="نام و نام خانوادگی" required class="form-control pr-3">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input name="phone" type="text" placeholder="موبایل" required class="form-control text-left pr-3">
                            </div>
                            <div class="col-md-12">
                                <textarea name="description" class="form-control pr-3" type="text" required placeholder="متن پیام"></textarea>
                                <button id="contactBtn" type="submit" class="submit send mt-5 pr-5 pl-5">ارسال اطلاعات</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>

    <script>

        $.validator.addMethod("phone", function(value, element) {
            isPhone = (this.optional(element) || /09\d{9}/.test(value)) && this.getLength($.trim(value), element) <= 12 && this.getLength($.trim(value), element) >= 11 ;
            return isPhone;
        }, "لطفا شماره تلفن صحیح وارد نمایید!" );


        $("#contactForm").validate({
            rules: {
                phone: {
                    phone: true
                }
            },
            submitHandler: function(form) {
                $("#contactBtn").text('درحال ارسال ...');
                $.ajax({
                    url: '{{route('front.contact')}}',
                    data: $(form).serialize(),
                    method: 'POST',
                    success: function(result) {
                        if(result.error == 1) {
                            $("#contactBtn").html(result.msg);
                        } else {
                            $("#contactForm").slideUp();
                            $("#successSend").slideDown();
                        }
                    }
                })
                .error(function() {
                    $("#contactBtn").html('ارسال اطلاعات');
                });
            }
        });
    </script>
@endsection