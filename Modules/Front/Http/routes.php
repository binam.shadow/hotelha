<?php

Route::group([
    'middleware' => ['web', 'numbers_to_english'],
    'namespace' => 'Modules\Front\Http\Controllers'
], function() {


    Route::get('/', [
        'as'    => 'front.index',
        'uses'  => 'FrontController@index'
    ]);
    
    Route::get('/about', [
        'as'    => 'front.about',
        'uses'  => 'FrontController@about'
    ]);
    
    Route::get('/contact', [
        'as'    => 'front.contact',
        'uses'  => 'FrontController@contact'
    ]);
    
    Route::post('/contact', [
        'as'    => 'front.contact',
        'uses'  => 'FrontController@storeContact'
    ]);
    
    Route::get('/guide', [
        'as'    => 'front.guide',
        'uses'  => 'FrontController@guide'
    ]);
    
    Route::get('/permissions', [
        'as'    => 'front.permissions',
        'uses'  => 'FrontController@permissions'
    ]);
    
    Route::get('/faq', [
        'as'    => 'front.faq',
        'uses'  => 'FrontController@faq'
    ]);
    
    Route::get('/rules', [
        'as'    => 'front.rules',
        'uses'  => 'FrontController@rules'
    ]);
    
    Route::get('/report', [
        'as'    => 'front.report',
        'uses'  => 'FrontController@report'
    ]);
    
    Route::get('/jobs', [
        'as'    => 'front.jobs',
        'uses'  => 'FrontController@jobs'
    ]);
    Route::post('/jobs', [
        'as'    => 'front.jobs',
        'uses'  => 'FrontController@storeJobs'
    ]);
    
    Route::get('/become-guest', [
        'as'    => 'front.becomeGuest',
        'uses'  => 'FrontController@becomeGuest'
    ]);
    
    Route::get('/how-to-reserve', [
        'as'    => 'front.reservingWay',
        'uses'  => 'FrontController@reservingWay'
    ]);
    
    Route::get('/how-to-reserve', [
        'as'    => 'front.reservingWay',
        'uses'  => 'FrontController@reservingWay'
    ]);
    
    Route::get('/reject-guesting-rule', [
        'as'    => 'front.rejectGuestingRule',
        'uses'  => 'FrontController@rejectGuestingRule'
    ]);
    
    
    Route::get('/guaranteed-payment', [
        'as'    => 'front.guaranteedPayment',
        'uses'  => 'FrontController@guaranteedPayment'
    ]);
    
    Route::get('/become-host', [
        'as'    => 'front.becomeHost',
        'uses'  => 'FrontController@becomeHost'
    ]);
    
    Route::get('/hosting-standards', [
        'as'    => 'front.hostingStandards',
        'uses'  => 'FrontController@hostingStandards'
    ]);
    
    Route::get('/reject-hosting-rule', [
        'as'    => 'front.rejectHostingRule',
        'uses'  => 'FrontController@rejectHostingRule'
    ]);
    
    Route::get('app/ios', [
        'as'    => 'front.ios',
        'uses'  => 'FrontController@ios'
    ]);
    
    Route::get('app/android', [
        'as'    => 'front.android',
        'uses'  => 'FrontController@android'
    ]);
    
    Route::get('/blog', [
        'as'    => 'front.blog.index',
        'uses'  => 'BlogController@index'
    ]);
    
    Route::get('/blog/{blog}/show', [
        'as'    => 'front.blog.show',
        'uses'  => 'BlogController@show'
    ]);
    
    Route::get('/blog/{blog}/like', [
        'as'    => 'front.blog.like',
        'uses'  => 'BlogController@like'
    ]);
    
    Route::get('/estates/list', [
        'as'    => 'front.estate.list',
        'uses'  => 'EstateController@index'
    ]);
    
    Route::get('/estates/search', [
        'as'    => 'front.estate.search',
        'uses'  => 'EstateController@search'
    ]);
    
    Route::get('/estates/show/{estate}', [
        'as'    => 'front.estate.show',
        'uses'  => 'EstateController@show'
    ]);
    
    Route::get('/estates/addCompare', [
        'as'    => 'front.estate.addCompare',
        'uses'  => 'EstateController@addCompare'
    ]);
    Route::get('/estates/removeCompare', [
        'as'    => 'front.estate.removeCompare',
        'uses'  => 'EstateController@removeCompare'
    ]);
    
    Route::get('/estates/compare', [
        'as'    => 'front.estate.compare',
        'uses'  => 'EstateController@compare'
    ]);
    
    
    Route::group([
        'prefix'    => 'auth',
        'namespace' => 'Auth'
    ], function () {
        Route::get('/signup', [
            'as'    => 'front.signup',
            'uses'  => 'SignupController@index'
        ]);
        Route::post('/signup', [
            'as'    => 'front.signup',
            'uses'  => 'SignupController@signup'
        ]);
    
        Route::get('/login', [
            'as'    => 'front.login',
            'uses'  => 'LoginController@index'
        ]);
        Route::post('/login', [
            'as'    => 'front.login',
            'uses'  => 'LoginController@login'
        ]);
    
        Route::get('/logout', [
            'as'    => 'front.logout',
            'uses'  => 'LoginController@logout'
        ]);
        
        Route::get('/forget', [
            'as'    => 'front.forget',
            'uses'  => 'ForgetController@index'
        ]);
        Route::post('/forget', [
            'as'    => 'front.forget',
            'uses'  => 'ForgetController@forget'
        ]);
    });
    
    
    Route::get('/getCities', [
        'as'    => 'front.getCities',
        'uses'  => 'EstateController@getCities'
    ]);
    
    Route::group([
        'middleware'=> ['front_auth']
    ], function () {
        
        Route::post('/sendComment', [
            'as' => 'front.sendComment',
            'uses' => 'EstateController@sendComment'
        ]);
    
        Route::post('/estates/rate', [
            'as'    => 'front.estate.rate',
            'uses'  => 'EstateController@rate'
        ]);
    
        Route::post('/estates/favorite', [
            'as'    => 'front.estate.favorite',
            'uses'  => 'EstateController@favorite'
        ]);
    });
    
    Route::group([
        'middleware'=> ['front_auth'],
        'prefix'    => 'dashboard',
        'namespace' => 'Dashboard'
    ], function () {
        
        Route::get('/', [
            'as'    => 'dashboard.index',
            'uses'  => 'DashboardController@index'
        ]);
    
        Route::get('/wishlist', [
            'as'    => 'dashboard.wishlist',
            'uses'  => 'DashboardController@wishlist'
        ]);
    
        Route::get('/profile', [
            'as'    => 'dashboard.profile',
            'uses'  => 'ProfileController@edit'
        ]);
        Route::post('/profile', [
            'as'    => 'dashboard.profile',
            'uses'  => 'ProfileController@update'
        ]);
    
        Route::get('/editPassword', [
            'as'    => 'dashboard.editPassword',
            'uses'  => 'ProfileController@editPassword'
        ]);
        Route::post('/editPassword', [
            'as'    => 'dashboard.editPassword',
            'uses'  => 'ProfileController@updatePassword'
        ]);
    
        Route::get('/reservation', [
            'as'    => 'dashboard.reservation',
            'uses'  => 'ReservationController@index'
        ]);
    
        Route::get('/reservation/dataList', [
            'as'    => 'dashboard.reservation.dataList',
            'uses'  => 'ReservationController@dataList'
        ]);
    
        Route::get('/reservation/changeStatus', [
            'as'    => 'dashboard.reservation.changeStatus',
            'uses'  => 'ReservationController@changeStatus'
        ]);
        
        Route::post('/reservation/check/{estate}', [
            'as'    => 'front.checkReserve',
            'uses'  => 'ReservationController@check'
        ]);
    
        Route::get('/ads', [
            'as'    => 'dashboard.ad.list',
            'uses'  => 'AdController@index'
        ]);
        Route::get('/ads/create', [
            'as'    => 'dashboard.ad.create',
            'uses'  => 'AdController@create'
        ]);
        Route::post('/ads/create', [
            'as'    => 'dashboard.ad.create',
            'uses'  => 'AdController@store'
        ]);
    
    
        Route::get('/ads/{estate}/edit', [
            'as'    => 'dashboard.ad.edit',
            'uses'  => 'AdController@edit'
        ]);
        Route::post('/ads/{estate}/edit', [
            'as'    => 'dashboard.ad.edit',
            'uses'  => 'AdController@update'
        ]);
    
        Route::get('/ads/delete/{estate?}', [
            'as'    => 'dashboard.ad.destroy',
            'uses'  => 'AdController@destroy'
        ]);
    
        Route::get('/ads/reserves/{estate?}', [
            'as'    => 'dashboard.ad.reserves',
            'uses'  => 'AdController@reserves'
        ]);
    
        Route::post('/ads/special', [
            'as'    => 'dashboard.ad.special',
            'uses'  => 'AdController@special'
        ]);
    
    
        Route::get('/wallet/{type}', [
            'as'    => 'dashboard.wallet',
            'uses'  => 'WalletController@index'
        ]);
    
        Route::post('/deleteImage', [
            'as'    => 'dashboard.deleteImage',
            'uses'  => 'AdController@deleteImage'
        ]);
    
    
        Route::any('/payment/callback', [
            'as'    => 'front.callBack',
            'uses'  => 'PaymentController@callBack'
        ]);
        
    });
});