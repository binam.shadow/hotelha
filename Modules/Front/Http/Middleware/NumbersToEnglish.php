<?php

namespace Modules\Front\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Classes\PersianTools;

class NumbersToEnglish
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        foreach ($request->all() as $key => $req) {
            $request[$key] = PersianTools::numberConvertor($request[$key]);
        }
        
        return $next($request);
    }
}
