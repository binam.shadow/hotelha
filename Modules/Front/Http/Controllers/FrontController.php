<?php

namespace Modules\Front\Http\Controllers;

use App\Province;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Blog\Entities\Blog;

use Modules\Message\Entities\Message;
use Modules\Setting\Entities\Slider;

use App\Classes\Notification;
use Modules\Hotel\Entities\Hotel;

class FrontController extends Controller {


    public function index() {

     $hotels = Hotel::where('active',1)->get();
        
        $hhs = Hotel::all();
        return view('front::index', compact('hotels','hhs'));
    }

    public function about() {
        $about = Setting::where('key', 'about')->first()->value;

        $about = json_decode($about);

        date_default_timezone_set("Asia/Tehran");

//        // Send data
//        $SendData = [
//            'Message' => 'تست پیامک سفید',
//            'MobileNumbers' => ['09373934074'],
//            'CanContinueInCaseOfError' => true
//        ];
//
//        $SendByMobileNumbers = new SendByMobileNumbers();
//        dd($SendByMobileNumbers->SendByMobileNumbers($SendData));
        return view('front::about', compact('about'));
    }

    public function contact() {
        return view('front::contact');
    }

    public function storeContact(Request $request) {
        $this->validate($request, [
            'title' => 'required|max:255',
            'phone' => 'required|digits:11|numeric|regex:/(^09)/',
            'description' => 'required '
        ]);

        $message = [];
        $message['title'] = $request->title;
        $message['phone'] = $request->phone;
        $message['description'] = $request->description;
        $message['type'] = 'contactus';
        Message::create($message);

        // push notification to Admin Panel
//        $content = \Lang::get('message::default.new_received_message');
//        $notification = new Notification();
//        $notification->sendWebNotification($content, url('admin/message/receivedList'));

        return [
            'error' => 0
        ];
    }

    public function guide() {

        $description = Setting::where('key', 'guide')->first()->value;

        $guides = json_decode($description);

        return view('front::guide', compact('guides'));
    }

    public function faq() {
        $faq = json_decode(Setting::where('key', 'faq')->first()->value);

        return view('front::faq', compact('faq'));
    }

    public function rules() {
        $description = Setting::where('key', 'site_rules')->first()->value;

        $rules = json_decode($description);
        return view('front::rules', compact('rules'));
    }


    public function becomeGuest() {

        $description = Setting::where('key', 'become_guest')->first()->value;

        $data = [
            'title' => ' چگونه مهمان شوم',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }

    public function jobs() {

        $description = Setting::where('key', 'jobs')->first()->value;

        $data = [
            'title' => ' فرصت های شغلی',
            'description' => $description
        ];

        return view('front::jobs', compact('data'));
    }

    public function storeJobs(Request $request) {
        $this->validate($request, [
            'title' => 'required|max:255',
            'phone' => 'required|digits:11|numeric|regex:/(^09)/',
            'description' => 'required '
        ]);

        $message = [];
        $message['title'] = $request->title;
        $message['phone'] = $request->phone;
        $message['description'] = $request->description;
        $message['type'] = 'jobs';
        Message::create($message);

        // push notification to Admin Panel
        $content = \Lang::get('message::default.new_received_message');
        $notification = new Notification();
        $notification->sendWebNotification($content, url('admin/message/receivedList'));

        return [
            'error' => 0
        ];
    }

    public function reservingWay() {

        $description = Setting::where('key', 'reserving_way')->first()->value;

        $data = [
            'title' => ' چگونه رزرو کنم',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }

    public function rejectGuestingRule() {

        $description = Setting::where('key', 'reject_guesting_rule')->first()->value;

        $data = [
            'title' => ' مقررات لغو رزرو',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }

    public function guaranteedPayment() {

        $description = Setting::where('key', 'guaranteed_payment')->first()->value;

        $data = [
            'title' => ' ضمانت بازگشت وجه',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }

    public function becomeHost() {

        $description = Setting::where('key', 'become_host')->first()->value;

        $data = [
            'title' => ' چگونه میزبان شوم',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }

    public function hostingStandards() {

        $description = Setting::where('key', 'hosting_standards')->first()->value;

        $data = [
            'title' => ' استانداردهای میزبانی',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }

    public function rejectHostingRule() {

        $description = Setting::where('key', 'reject_hosting_rule')->first()->value;

        $data = [
            'title' => ' مقررات لغو رزرو',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }

    public function ios() {
        $description = Setting::where('key', 'ios_guide')->first()->value;

        return view('front::ios', compact('description'));
    }

    public function permissions() {

        $description = Setting::where('key', 'application_permissions')->first()->value;

        $data = [
            'title' => 'دسترسی های اپلیکیشن',
            'description' => $description
        ];

        return view('front::simple_page', compact('data'));
    }
}
