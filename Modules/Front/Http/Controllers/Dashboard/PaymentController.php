<?php

namespace Modules\Front\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;


use Auth;
use Modules\Estate\Entities\Calendar;
use Modules\Estate\Entities\Estate;
use Modules\Payment\Entities\Transaction;
use Modules\Payment\Http\Classes\Gateway;
use App\User;
use Modules\Reserve\Entities\DiscountCode;
use Modules\Reserve\Entities\Reserve;
use Modules\Reserve\Entities\ReserveDate;
use Modules\Reserve\Notifications\Reservation;
use Modules\Reserve\Http\classes\ReserveHelper;
use Modules\Wallet\Entities\Wallet;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Carbon\Carbon;
use App\Classes\Smsir;

class PaymentController extends Controller {
    
    
    public function callback(Request $request) {
        try {
            $order = Order::where('tracking_code', $request->ResNum)->first();
            
            if ($order->type == "plan") {
                $gateway = \Gateway::verify();
            
                // update Status in Database Transactions
                Transaction::where('id', $request->ResNum)->update(['status' => 'SUCCEED']);
            
            
                $order->update([
                    'status' => 'succeed'
                ]);
            
                $plan = Plan::find($order->plan_id);
                $product_id = OrderItem::where('order_id', $order->id)->first()->product_id;
                $product = Estate::find($product_id);
            
            
                if (!is_null($plan->action_types)) {
                    $plan->action_types = explode(',', $plan->action_types);
                
                    if (in_array('ladder', $plan->action_types)) {
                        $product->updated_at = \Carbon\Carbon::now();
                        $product->save();
                    }
                    if (in_array('special', $plan->action_types)) {
                        $product->timestamps = false;
                        $product->special = 1;
                        $product->save();
                    }
                }
                
                $data = (object)[
                    //            'cardNum'   => $gateway->cardNumber(),
                    'cardNum'   => '11111',
                    'price'     => $order->total_price,
                    'resNum'    => $request->ResNum,
                ];
    
                return view('front::payment_result.success', compact('data'));
            }
            else {
                $reserve_parameters = json_decode($order->reserve_parameters,true);
                $estate = Estate::find($reserve_parameters['estate_id']);
    
                $dates = $reserve_parameters["dates"];
    
                $calendar = collect(json_decode($estate->calendar()->first()->calendar, true));
    
    
                $dates = $calendar->whereIn('start', $dates);
            
            
                $user = User::find($order->user_id);
                $discount_code = null;
                if(isset($reserve_parameters["code"])) {
                    $discount_code = DiscountCode::with(["users" => function ($query) use ($user) {
                        $query->where("users.id", $user->id);
                    }])
                        ->where(["code" => $reserve_parameters["code"], "active" => 1])
                        ->where("number_of_use", ">", 0)
                        ->first();
                
                    if(!is_null($discount_code) && $discount_code->users()->count())
                        $discount_code = null;
                }
            
                $sum_price = 0;
                $discount_price = 0;
                $reserve_dates = [];
                foreach ($dates as $date){
                    array_push($reserve_dates,$date["start"]);
                    $sum_price += $date["price"];
                    if(!is_null($discount_code)){
                        $discount_price += (int)(((100 - $discount_code->precent)/100)*$date["price"]);
                    } else {
                        $discount_price += $date["price"];
                    }
                
                }
            
                if(!is_null($discount_code)) {
                    $discount_code->number_of_use = $discount_code->number_of_use - 1;
                    $discount_code->save();
                    $discount_code->users()->attach([$user->id]);
                }
            
            
                $exist_reserve = Reserve::with(["dates" => function($query) use($reserve_dates){
                    $query->whereIn("date",$reserve_dates);
                }])
                    ->whereIn('status', [0, 1])
                    ->where(["product_id" => $reserve_parameters["estate_id"]])
                    ->first();
            
                if(!is_null($exist_reserve) && count($exist_reserve->dates)){
                    $data = (object)[
                        'description' => 'خطایی رخ داده است',
                        'resNum'    => $request->ResNum,
                    ];
                    return view('front::payment_result.failed', compact('data'));
                }
                if($order->type == "reserve-with-wallet"){
                    if(($user->use_price + $order->price) < $discount_price ){
                        $data = (object)[
                            'description' => 'مقدار کیف پول شما کافی نیست',
                            'resNum'    => $request->ResNum,
                        ];
                        return view('front::payment_result.failed', compact('data'));
                    }
                }
    
    
                $gateway = \Gateway::verify();
            
                $estate = Estate::find($reserve_parameters["estate_id"]);
            
                $code = strtoupper(str_random(10));
                while(!is_null(Reserve::where('code', $code)->first()))
                    $code = strtoupper(str_random(10));
            
                $reserve = Reserve::create([
                    "product_id" => $reserve_parameters["estate_id"],
                    "user_id" => $user->id,
                    "sum_price" => $sum_price,
                    "discount_price" => $discount_price,
                    "owner_price" => $discount_price,
                    "admin_price" => 0,
                    "user_price" => 0,
                    "is_paid" => 1,
                    "owner_id" => $estate->user_id,
                    "code" => $code
                ]);
                
            
                $owner = User::find($estate->user_id);
                $owner->last_reserved_at  = Carbon::now();
                $owner->save();
            
                $fa_reserve_dates = [];
                foreach ($dates as $date){
                    $discount_price = $date["price"];
                    if(!is_null($discount_code)){
                        $discount_price = (int)(((100 - $discount_code->precent)/100)*$date["price"]);
                    }
                
                    $fa_reserve_date = \jDate::forge($date["start"])->format('date');
                    $fa_reserve_dates[] = $fa_reserve_date ;
                
                    ReserveDate::create([
                        "reserve_id" => $reserve->id,
                        "price" => $sum_price,
                        "discount_price" => $discount_price,
                        "date" => $date["start"]
                    ]);
                }
            
            
                $calendar = Calendar::where("product_id",$reserve_parameters["estate_id"])->first();
                $calendar_dates = json_decode($calendar->calendar);
            
                $calendar_new_dates = [];
                $dates_length = count($dates);
            
                foreach ($calendar_dates as $calendar_date) {
                    foreach ($dates as $key => $date){
                        if($date["start"] == $calendar_date->start){
                            $calendar_date->reserve = 1;
                        }
                    }
                    array_push($calendar_new_dates,$calendar_date);
                }
            
                $calendar->calendar = json_encode($calendar_new_dates, JSON_UNESCAPED_UNICODE);
                $calendar->save();
            
            
//                $wallet_code = strtoupper(str_random(10));
//                while(!is_null(Wallet::where('code', $wallet_code)->first()))
//                    $wallet_code = strtoupper(str_random(10));
            
            
                $wallet = Wallet::create([
                    'type'        => 'reserve',
                    'type_id'     => $reserve->id,
                    'description' => 'رزرو ویلا '.$estate->name,
                    'price'       => $order->total_price,
                    'user_id'     => $order->user_id,
                    'is_plus'     => 1,
                    'is_used'     => 0,
                    'factor_id'   => $order->id,
                    'tracking_code' => $order->tracking_code,
                    'estate_id' =>  $reserve->product_id,
                    'code' => $code,
                ]);
            
                $user = User::find($order->user_id);
                $user->wallet_price = $user->wallet_price + $order->total_price;
                $user->save();
            
                if($order->type == "reserve-with-wallet"){
                    $user->use_price = $user->use_price - $order->total_price;
                    $user->save();
                }
            }
        
            $user = User::find($order->user_id);
            $destination_nums = [$user->phone];
        
            $order->reserve_id = $reserve->id;
            $order->save();
    
            ReserveHelper::sendSms($owner, $user, $reserve, $order, $fa_reserve_dates, $request);
            ReserveHelper::sendEmail($owner, $user, $reserve, $order, $fa_reserve_dates, $request);
            
        
            $data = (object)[
                'cardNum'   => '11111',
                'price'     => $order->total_price,
                'resNum'    => $request->ResNum,
                'user' => $user,
                'owner' => $owner,
                "estate_id" => $estate->id,
                "reserve_code" => $reserve->code,
                "dates" => $fa_reserve_dates,
            ];
        
            return view('front::payment_result.success', compact('data'));
        } catch (\Larabookir\Gateway\Exceptions\RetryException $e) {

            return $request->all();
            Transaction::where('id' , $request->ResNum)->update(['status' => 'FAILED']);
            Order::where('tracking_code', $request->ResNum)->update([
                'status' => 'failed'
            ]);

            $data = (object)[
                'description' => $e->getMessage(),
                'resNum'    => $request->ResNum,
            ];
            return view('front::payment_result.failed', compact('data'));

        } catch (\Exception $e) {

            Transaction::where('id' , $request->ResNum)->update(['status' => 'FAILED']);
            Order::where('tracking_code', $request->ResNum)->update([
                'status' => 'failed'
            ]);

            $data = (object)[
                'description' => $e->getMessage(),
                'resNum'    => $request->ResNum,
            ];
            return view('front::payment_result.failed', compact('data'));

        }
//
    }
}
