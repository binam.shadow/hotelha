<?php

namespace Modules\Front\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller {
    
    
    public function edit() {
    
        $user = Auth::user();
        
        return view('front::dashboard.profile', compact('user'));
    }
    
    public function update(Request $request) {
        
        $this->validate($request, [
            'name' => 'required|max:255',
            'sex' => 'required|in:male,female',
            'national_code' => [
                'required',
                'numeric',
                'digits:10',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[0-9]{10}$/', $value))
                        return $fail('کد ملی وارد شده معتبر نیست');
                    for($i = 0; $i < 10; $i++)
                        if(preg_match('/^'.$i.'{10}$/',$value))
                            return $fail('کد ملی وارد شده معتبر نیست');
                    for($i = 0, $sum = 0; $i < 9; $i++)
                        $sum += (( 10 - $i) * intval(substr($value, $i,1)));
                    $ret = $sum % 11;
                    $parity = intval(substr($value, 9,1));
                    if(($ret < 2 && $ret == $parity) || ($ret >= 2 && $ret == 11 - $parity))
                        return true;
                    return $fail('کد ملی وارد شده معتبر نیست');
                }
            ],
            'card_number' => 'nullable|digits:16',
            'email' => 'email'
        ], [
            'sex.*' => 'وارد کردن جنسیت ضروری است',
        ], [
            'national_code' => 'کد ملی',
            'card_number' => 'شماره کارت'
        ]);
        
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->sex = $request->sex;
        $user->national_code = $request->national_code;
        $user->card_number = $request->card_number;
        $user->save();
        
        return back()
                ->with('msg', 'پروفایل شما با موفقیت ویرایش شد');
    }
    
    public function editPassword() {
        
        return view('front::dashboard.edit_password');
    }
    
    public function updatePassword(Request $request) {
        $this->validate($request, [
            'current_password' => 'required',
            'password'  => 'required|min:6|confirmed',
        ]);
    
        $hash_check = Hash::check($request->current_password, Auth::user()->password);
    
        if(!$hash_check)
            return back()->withErrors([
                'current_password' => 'رمز عبور فعلی صحیح نیست'
            ]);
        
        Auth::user()->update([
            'password' => bcrypt($request->password)
        ]);
    
        return back()->with('msg', 'رمزعبور شما با موفقیت به روز رسانی شد');
    }
}
