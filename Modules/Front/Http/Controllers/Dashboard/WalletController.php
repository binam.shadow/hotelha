<?php

namespace Modules\Front\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Modules\Wallet\Entities\Wallet;

class WalletController extends Controller {
    
    public function index($type, Request $request) {
        if($type != 'user' && $type != 'owner') abort(404);
        
        $user = Auth::user();
        
        $wallets = Wallet::where('user_id', $user->id);
        
        if($request->type == 'user') {
            $wallets->where('type', '!=', 'owner');
            $price = $user->use_price;
        } else {
            $wallets->where('type', '=', 'owner');
            $price = $user->owner_price;
        }
        
    
        $wallets = $wallets->orderBy('created_at', 'desc')
                ->paginate(15);
        
        
        
        return view('front::dashboard.wallet', compact('wallets', 'user', 'price', 'type'));
    }
}
