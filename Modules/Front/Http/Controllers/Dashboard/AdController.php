<?php

namespace Modules\Front\Http\Controllers\Dashboard;

use App\City;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Modules\Estate\Entities\Estate;
use Modules\Estate\Entities\Feature;
use Modules\Estate\Entities\Calendar;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Modules\Product\Entities\ProductGallery;
use App\Classes\PersianTools;
use App\Classes\FileManager;
use Carbon\Carbon;
use Image;
use Modules\Payment\Http\Classes\Gateway;
use Modules\Reserve\Entities\Reserve;

class AdController extends Controller {
    
    public function index() {
        
        $estates = Estate::where([
            'user_id' => Auth::id()
        ])->get();
        
        $plans = Plan::get();
        
        return view('front::dashboard.ad_list', compact('estates', 'plans'));
    }

    
    public function create() {
        
        if(is_null(Auth::user()->national_code)) return view('front::dashboard.national_code');
        
        $provinces = Province::get();
        $cities = City::where('province_id', $provinces->first()->id)->get();
        $features = Feature::get();
        $now = Carbon::now()->format('Y-m-d');
        
        return view('front::dashboard.ad_create', compact('provinces', 'cities', 'features', 'now'));
    }
    
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required|numeric|max:2147483647',
            'province' => 'required|exists:provinces,id',
            'city' => 'required|exists:cities,id',
            'image' => 'image|max:2048',
            'gallery.*' => 'image|max:2048'
        ]);
        
        $estate = $request->except('ad_type');
        $estate['user_id'] = Auth::id();
        $estate['category_id'] = 1;
        $estate['forest'] = 0;
        $estate['jac'] = 0;
        $estate['furnished'] = 0;
        $estate['shore'] = 0;
        $estate['pool'] = 0;
        $estate['exclusive'] = 0;
        $estate['parking'] = 0;
        $estate['elevator'] = 0;
        $estate['active'] = 0;
        if ($request->forest) {
            $estate['forest'] = 1;
        }
        if ($request->jac) {
            $estate['jac'] = 1;
        }
        if ($request->furnished) {
            $estate['furnished'] = 1;
        }
        if ($request->shore) {
            $estate['shore'] = 1;
        }
        if ($request->pool) {
            $estate['pool'] = 1;
        }
        if ($request->exclusive) {
            $estate['exclusive'] = 1;
        }
        if ($request->parking) {
            $estate['parking'] = 1;
        }
        if ($request->elevator) {
            $estate['elevator'] = 1;
        }
        if($request->special){
            $estate['special'] = 1;
            $estate['sell_price'] = Setting::where('key', 'special_cost')->first()->value;
            $estate['is_paid'] = 1;
        }
        else {
            $estate['sell_price'] = 0;
        }
        $estate['active'] = 0;
    
        $estate['image'] = $request->image;
        $estate['user_id'] = \Auth::user()->id;
        $estate['price'] = PersianTools::numberConvertor($request->price);
        $estate['size'] = ($request->size) ? PersianTools::numberConvertor($request->size) : null;
        $estate['capacity'] = ($request->capacity) ? PersianTools::numberConvertor($request->capacity) : null;
        $estate['distancetosea'] = ($request->distancetosea) ? PersianTools::numberConvertor($request->distancetosea) : null;
        $estate['room'] = ($request->room) ? PersianTools::numberConvertor($request->room) : null;
        $estate['village_name'] = ($request->village_name) ? PersianTools::arabicToPersian($request->village_name) : null;
    
        if ($request->agreed_price) {
            $estate['price'] = -1;
        }
        $estate_id = Estate::create($estate);
    
        $estate_id->features()->attach($request->features);
    
        $calendar = Calendar::create(["product_id"=> $estate_id->id, "calendar" => $request->calendar]);
    
        if ($request->gallery) {
            foreach ($request->gallery as $image) {
                if($image) {
                    $image_path = FileManager::uploader('images/estates', $image);
                    $gallery_id = ProductGallery::create(['product_id' => $estate_id->id, 'image' => $image_path]);
//				$img = Image::make(public_path('uploads/' . $image_path));
//				$img->insert(public_path('images/watermark.png'), 'bottom-right', 5, 5)->save();
                    $img = Image::make(public_path('uploads/' . $image_path));
                    $img->fit(500)->save();
                }
            }
        }
        
        if($request->image) {
            $image_path = FileManager::uploader('images/estates', $request->image);
            Estate::where('id', $estate_id->id)->update(['image' => $image_path]);
//				$img = Image::make(public_path('uploads/' . $image_path));
//				$img->insert(public_path('images/watermark.png'), 'bottom-right', 5, 5)->save();
            $img = Image::make(public_path('uploads/' . $image_path));
            $img->fit(500)->save();
        }
        
        return redirect()->route('dashboard.ad.list')
                    ->with('msg', 'اطلاعات شما با موفقیت ذخیره شد و پس از تایید مدیر نمایش داده خواهد شد');
    }
    
    public function edit(Estate $estate) {
        
        if($estate->user_id != Auth::id()) abort(404);
        
        $gallery = ProductGallery::where('product_id', $estate->id)->get();
        $provinces = Province::get();
        $cities = City::where('province_id', $estate->province)->get();
        
        $calendar = Calendar::where('product_id' , $estate->id)->first();
        $events = [];
        $now = Carbon::now()->format('Y-m-d');
        if($calendar) {
            $events = $calendar->calendar;
            $now = json_decode($events)[0]->start;
            $events = json_decode($events, true);
        }
        
        foreach ($events as $key => $event) {
            $events[$key]['className'] = 'purple-shadow';
        }
//        return $events;
    
        $features = Feature::get();
        
        return view('front::dashboard.ad_edit', compact('estate', 'gallery', 'provinces', 'cities', 'now', 'events', 'features'));
    }
    
    
    public function update(Estate $estate, Request $request) {
        if($estate->user_id != Auth::id()) abort(404);
    
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required|numeric|max:2147483647',
            'province' => 'required|exists:provinces,id',
            'city' => 'required|exists:cities,id',
            'image' => 'image|max:2048',
            'gallery.*' => 'image|max:2048'
        ]);
        
        $estate->name = $request->name;
        $estate->description = $request->description;
        if($request->lat & $request->lng) {
            $estate->lat = $request->lat;
            $estate->lng = $request->lng;
        }
        $estate->price = PersianTools::numberConvertor($request->price);
        $estate->province = $request->province;
        $estate->city = $request->city;
        $estate->village_name = ($request->village_name) ? PersianTools::arabicToPersian($request->village_name) : null;
        $estate->size = ($request->size) ? PersianTools::numberConvertor($request->size) : null;
        $estate->capacity = ($request->capacity) ? PersianTools::numberConvertor($request->capacity) : null;
        $estate->room = ($request->room) ? PersianTools::numberConvertor($request->room) : null;
        $estate->distancetosea = ($request->distancetosea) ? PersianTools::numberConvertor($request->distancetosea) : null;
        $estate->apartment = $request->apartment;
        if ($request->forest) {
            $estate->forest = 1;
        }else{
            $estate->forest = 0;
        }
        if ($request->jac) {
            $estate->jac = 1;
        }else{
            $estate->jac = 0;
        }
        if ($request->furnished) {
            $estate->furnished = 1;
        }else{
            $estate->furnished = 0;
        }
        if ($request->shore) {
            $estate->shore = 1;
        }else{
            $estate->shore = 0;
        }
        if ($request->pool) {
            $estate->pool = 1;
        }else{
            $estate->pool = 0;
        }
        if ($request->exclusive) {
            $estate->exclusive = 1;
        }else{
            $estate->exclusive = 0;
        }
        if ($request->parking) {
            $estate->parking = 1;
        }else{
            $estate->parking = 0;
        }
        if ($request->elevator) {
            $estate->elevator = 1;
        }else{
            $estate->elevator = 0;
        }
        if ($request->special) {
            if(!$estate->special) {
                $estate->special = 1;
                $estate->sell_price = Setting::where('key', 'special_cost')->first()->value;
                $estate->is_paid = 1;
            }
        } else {
            if($estate->special) {
                $estate->special = 0;
                $estate->sell_price = 0;
                $estate->is_paid = 0;
            }
        }
        
        $estate->active = 0;
        $estate->verified = 0;
    
        if ($request->agreed_price) {
            $estate->price = -1;
        }
        $estate->save();
    
        $estate->features()->sync($request->features);
    
        if ($request->gallery) {
            foreach ($request->gallery as $image) {
                if($image) {
                    $image_path = FileManager::uploader('images/estates', $image);
                    $gallery_id = ProductGallery::create(['product_id' => $estate->id, 'image' => $image_path]);
//				$img = Image::make(public_path('uploads/' . $image_path));
//				$img->insert(public_path('images/watermark.png'), 'bottom-right', 5, 5)->save();
                    $img = Image::make(public_path('uploads/' . $image_path));
                    $img->fit(500)->save();
                }
            }
        }
    
        if($request->image) {
            $image = $request->image;
            $image_path = FileManager::uploader('images/estates', $image);
            Estate::where('id', $estate->id)->update(['image' => $image_path]);
//				$img = Image::make(public_path('uploads/' . $image_path));
//				$img->insert(public_path('images/watermark.png'), 'bottom-right', 5, 5)->save();
            $img = Image::make(public_path('uploads/' . $image_path));
            $img->fit(500)->save();
        }
    
        $calender = Calendar::where("product_id", $estate->id)->first();
    
        if($calender){
            $calender->calendar = $request->calendar;
            $calender->save();
        } else {
            Calendar::create(["product_id" => $estate->id, "calendar" => $request->calendar]);
        }
    
    
        return redirect()->route('dashboard.ad.list')
            ->with('msg', 'اطلاعات شما با موفقیت ذخیره شد و پس از تایید مدیر نمایش داده خواهد شد');
    }
    
    public function destroy(Estate $estate = null) {
        if(is_null($estate) || $estate->user_id != Auth::id()) abort(404);
    
        $galleries = ProductGallery::where('product_id', $estate->id)->get();
        foreach($galleries as $gallery){
            FileManager::delete($gallery->image);
        }
        
        FileManager::delete($estate->image);
        $estate->forceDelete();
        return ['status' => 'success'];
    }
    
    public function galleryDelete(Request $request) {
    
        ProductGallery::find($request->key)->delete();
        
        return ['status' => 'success'];
    }
    
    public function deleteImage(Request $request) {
        $id = $request->key;
        
        if($request->feature_image) {
            $estate = Estate::find($id);
            FileManager::delete($estate->image);
            $estate->image = null;
            $estate->save();
        } else {
            $gallery = ProductGallery::find($id);
            $estate = Estate::find($gallery->product_id);
    
            if($estate->user_id != Auth::id()) abort(404);
    
            FileManager::delete($gallery->image);
            $gallery->delete();
        }
        
        return [
            'status' => 'success'
        ];
    }
    
    public function special(Request $request) {
        $user = Auth::user();
        
        $this->validate($request, [
            'id' => 'required|exists:products,id',
            'plan' => 'required|exists:plans,id'
        ]);
    
        $plan = Plan::find($request->plan);
        $estate = Estate::find($request->id);
    
        $token = str_random(32);
    
        $order = Order::create([
            'user_id'       => $user->id,
            'user_name'     => $user->name,
            'total_price'   => $plan->price,
            'status'        => 'init',
            'token'         => $token,
            'plan_id'       => $plan->id,
            'plan_data'     => $plan->toJson()
        ]);
    
        OrderItem::create([
            'order_id'      => $order->id,
            'product_id'    => $estate->id,
            'product_name'  => $estate->name,
            'count'         => 1,
            'price'         => $plan->price
        ]);
    
        try {
            //$tracking_code = $this->getTimeId();
            $gateway = Gateway::saman();
            $gateway->setCallback(route('front.callBack'));
//            $gateway->price(1000)->ready();
            $gateway->price($order->total_price)->ready();
        
            $order->update([
                'tracking_code' => $gateway->transactionId(),
            ]);
        
            return $gateway->redirect();
        
        } catch (Exception $e) {
        
            echo $e->getMessage();
        }
    }
    
    public function reserves(Estate $estate) {
        if($estate->user_id != Auth::id()) abort(404);
        
        $type = 'owner';
        
        return view('front::dashboard.reservation', compact('estate','type', 'reserves'));
    }
}
