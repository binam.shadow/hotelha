<?php

namespace Modules\Front\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;


use App\Classes\PersianTools;
use Illuminate\Support\Facades\Auth;
use Modules\Estate\Entities\Calendar;
use Modules\Estate\Entities\Estate;
use Modules\Reserve\Entities\DiscountCode;
use Modules\Reserve\Entities\Reserve;
use Modules\Reserve\Entities\ReserveDate;
use Modules\Wallet\Entities\Wallet;
use Modules\Plan\Entities\Order;
use Modules\Payment\Entities\Transaction;
use Modules\Payment\Http\Classes\Gateway;

use DataTables;
use Morilog\Jalali\jDate;
use App\User;
use Carbon\Carbon;
use Modules\Reserve\Http\classes\ReserveHelper;

class ReservationController extends Controller {
    
    
    public function index() {
        $type = 'user';
        return view('front::dashboard.reservation', compact('type'));
    }
    
    public function check(Estate $estate, Request $request) {
        $user = Auth::user();
        
        $calendar = $estate->calendar()->first();
        if(is_null($calendar)) return ['error' => 1, 'msg' => 'امکان رزرو این اقامتگاه وجود ندارد'];
        
        $calendar = collect(json_decode($calendar->calendar, true));
        
        // convert persian numbers to english
        $start_date = PersianTools::numberConvertor($request->start_date);
        $end_date = PersianTools::numberConvertor($request->end_date);
        
        // convert jalali date toGregorian
        $start_date = \Morilog\Jalali\jDateTime::createCarbonFromFormat('Y/m/d', $start_date)->toDateString();
        $end_date = \Morilog\Jalali\jDateTime::createCarbonFromFormat('Y/m/d', $end_date)->toDateString();
        
        
        $discount_code = null;
        if($request->code) {
            $discount_code = DiscountCode::with(["users" => function ($query) use ($user) {
                $query->where("users.id", $user->id);
            }])
                ->where(["code" => $request->code, "active" => 1])
                ->where("number_of_use", ">", 0)
                ->first();
        }
        
        $dates = new \DatePeriod(
            new \DateTime($start_date),
            new \DateInterval('P1D'),
            (new \DateTime($end_date))->modify('+1 day')
        );
        
        
        $sum_price = 0;
        $discount_price = 0;
        $reserve_dates = [];
        foreach ($dates as $key => $date) {
            $date = $calendar->where('start', '=', $date->format('Y-m-d'))
                            ->where('reserve', '=', 0)
                            ->where('active', '=', 1)
                            ->first();
            if(is_null($date))
                return [
                    'error' => 1,
                    'msg'   => 'متاسفانه در تاریخ وارد شده امکان رزرو وجود ندارد!'
                ];
            
            array_push($reserve_dates, $date['start']);
            $sum_price += $date['price'];
            if(!is_null($discount_code)){
                $discount_price += (int)(((100 - $discount_code->precent)/100)*$date['price']);
            } else {
                $discount_price += $date['price'];
            }
            
        }
        
        if(!count($reserve_dates))
            return [
                'error' => 1,
                'msg' => 'تاریخ وارد شده معتبر نمیباشد'
            ];
    
    
        $discount_code = null;
        $discount_msg = 'وارد نشده';
        if($request->code) {
            $discount_msg = 'نامعتبر';
            
            $discount_code = DiscountCode::with(["users" => function ($query) use ($user) {
                $query->where("users.id", $user->id);
            }])
                ->where(["code" => $request->code, "active" => 1])
                ->where("number_of_use", ">", 0)
                ->first();
        }
    
        $sum_price = 0;
        $discount_price = 0;
        $reserve_dates = [];
        foreach ($dates as $date) {
            $date = $calendar->where('start', '=', $date->format('Y-m-d'))->first();
            
            array_push($reserve_dates,$date["start"]);
            $sum_price += $date["price"];
            if(!is_null($discount_code)){
                $discount_price += (int)(((100 - $discount_code->precent)/100)*$date["price"]);
                $discount_msg = $discount_code->precent . ' درصد ';
            } else {
                $discount_price += $date["price"];
            }
        }
    
    
        $exist_reserve = Reserve::with(["dates" => function($query) use($reserve_dates){
                $query->whereIn("date", $reserve_dates);
            }])
            ->whereIn('status', [0, 1])
            ->where(["product_id" => $estate->id])
            ->first();
            
        if(!is_null($exist_reserve) && count($exist_reserve->dates)){
            return $data = ["error" => 1, "msg" => "در تاریخ های فوق ویلا رزرو شده است"] ;
        }
        
        if($request->ajax())
            return [
                'error' => 0,
                'price' => $sum_price,
                'discount_price' => $discount_price,
                'discount_msg' => $discount_msg
            ];
    
        $token = str_random(32);
        
        $reserve_type = 'reserve-with-wallet';
        if($user->use_price < $discount_price)
            $reserve_type = 'reserve';
        
        $reserve_parameters = [
            'code'  => $request->code,
            'dates' => $reserve_dates,
            'estate_id' => $estate->id
        ];
        
        $order = Order::create([
            'user_id'       => $user->id,
            'user_name'     => $user->name,
            'total_price'   => $discount_price,
            'status'        => 'init',
            'token'         => $token,
            'type'          => $reserve_type,
            'reserve_parameters'       => json_encode($reserve_parameters)
        ]);
    
    
        try {
            //$tracking_code = $this->getTimeId();
            $gateway = Gateway::saman();
            $gateway->setCallback(route('front.callBack'));
//            $gateway->price(1000)->ready();
            $gateway->price(($order->total_price * 10))->ready();
        
            $order->update([
                'tracking_code' => $gateway->transactionId(),
            ]);
        
            return $gateway->redirect();
        
        } catch (Exception $e) {
        
            echo $e->getMessage();
        }
    }
    
    public function dataList(Request $request) {
        $this->validate($request, [
            'type' => 'required|in:user,owner'
        ]);
        
        $reserves = Reserve::with(['estate' => function($query) {
            $query->with('user');
        }, 'dates', 'user']);
        if($request->type == 'user')
            $reserves->where('user_id', Auth::id());
        else {
            $reserves->where([
                'owner_id' => Auth::id(),
                'product_id' => $request->id
            ]);
        }
    
        return DataTables::of($reserves->orderBy('created_at', 'desc'))
                ->addColumn('reserve_date', function ($item) {
                    return $item->created_at;
//                    return jDate::forge($item->created_at)->format('%B %d، %Y');
                })
                ->addColumn('from_date', function ($item) {
                    try {
                        return jDate::forge($item->dates()->first()->date)->format('%d %B %Y');
                    } catch (\Exception $e) {
                        return '';
                    }
                })
                ->addColumn('to_date', function ($item) {
                    try {
                        return jDate::forge($item->dates->reverse()->first()->date)->format('%d %B %Y');
                    } catch (\Exception $e) {
                        return '';
                    }
                })
                ->addColumn('status_label', function ($item) {
                    $label = 'درحال بررسی';
                    switch ($item->status) {
                        case 1:
                            $label = 'تحویل داده شد';
                            break;
                        case 2:
                            $label = 'کنسل شده توسط مالک';
                            break;
                        case 3:
                            $label = 'کنسل شده توسط کاربر';
                    }
                    return $label;
                })
                ->make();
    }
    
    public function changeStatus(Request $request) {
        $this->validate($request, [
            'id' => 'exists:reserves,id',
            'user_type' => 'in:user,owner'
        ]);
        $user = \Auth::user();
        
        $status = 0;
        $reserve = Reserve::with(["estate", "dates"])->find($request->id);
        
        $owner =  User::find($reserve->estate->user_id);
        $reservatore = User::find($reserve->user_id);
        
        // check request and detect status
        if($request->user_type == 'user' && $reservatore->id == $user->id)
            $status = 3;
        elseif($request->user_type == 'owner' && $owner->id == $user->id)
            $status = 2;
        else
            abort(404);
        
        if($reserve->status) {
            return ["error" => 1 , "msg" => "شما قادر به انجام این عمل نیستید."];
        }
        
        ReserveHelper::changeStatusReserve($reserve,$reservatore,$owner,$status);
    
        $data = ["error" => 0 , "msg" => "رزرو ویلا با موفقیت کنسل شد."];
        return $data;
    }
}

