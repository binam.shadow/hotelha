<?php

namespace Modules\Front\Http\Controllers\Dashboard;

use App\Classes\Smsir;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Estate\Entities\Estate;
use Modules\Product\Entities\Wishlist;

use Modules\Reserve\Notifications\Reservation;

class DashboardController extends Controller {
   
    public function index() {
        
        return redirect()->route('dashboard.profile');
        return view('front::dashboard.index');
    }
    
    public function wishlist() {
        $wishlists = Wishlist::where('user_id', Auth::id())->get()->pluck('product_id');
    
        $estates = Estate::whereIn('id', $wishlists)->paginate(10);
        
        return view('front::dashboard.wishlist', compact('estates'));
        
    }
}
