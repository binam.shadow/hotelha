<?php

namespace Modules\Front\Http\Controllers;

use App\City;
use App\Http\Controllers\Controller;
use App\Province;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Lcobucci\JWT\Claim\Validatable;
use Modules\Estate\Entities\Estate;
use Modules\Estate\Entities\Calendar;
use Modules\Product\Entities\ProductGallery;
use Modules\Product\Entities\ProductRating;
use Modules\Product\Entities\Wishlist;

class EstateController extends Controller {
    
    public function index(Request $request) {
        $this->validate($request, [
            'province' => 'nullable|exists:provinces,id'
        ]);
        
        $provinces = Province::get();
        
        $cities = City::get();
        
        $estates = Estate::orderBy('updated_at', 'desc')
            ->where([
                'active' => 1,
                'verified' => 1
            ]);
        
        if($request->province)
            $estates->where('province', $request->province);
        
        if($request->city)
            $estates->where('city', $request->city);
    
        $estates = $estates->paginate(9);
        
        
        return view('front::estate.list', compact('estates', 'provinces', 'cities'));
    }
    
    public function search(Request $request) {
//        Validatable::
        
        $estates = Estate::orderBy('updated_at', 'desc')
            ->where([
                'active' => 1,
                'verified' => 1
            ]);
        
        
        if($request->province)
            $estates->where('province', $request->province);
    
        if($request->city)
            $estates->where('city', $request->city);
    
        if($request->apartment)
            $estates->whereIn('apartment', $request->apartment);
    
        if($request->min_size && $request->max_size)
            $estates->whereBetween('size', [$request->min_size, $request->max_size]);
        
        if($request->room)
            $estates->where('room', $request->room);
        
        if($request->min_price && $request->max_price)
            $estates->whereBetween('price', [$request->min_price, $request->max_price]);
    
        if($request->min_capacity && $request->max_capacity)
            $estates->whereBetween('capacity', [$request->min_capacity, $request->max_capacity]);
    
//        return $estates->limit(4)->get();
        if($request->forest)
            $estates->where('forest', 1);
    
        if($request->jac)
            $estates->where('jac', 1);
    
        if($request->furnished)
            $estates->where('furnished', 1);
        
        if($request->shore)
            $estates->where('shore', 1);
    
        if($request->pool)
            $estates->where('pool', 1);
    
        if($request->exclusive)
            $estates->where('exclusive', 1);
    
        if($request->parking)
            $estates->where('parking', 1);
    
        if($request->elevator)
            $estates->where('elevator', 1);
        
        $estates = $estates->paginate(35);
        
        return view('front::partials._estates_ajax_list', compact('estates'));
    }

    public function show(Estate $estate, Request $request) {
        $estate->active && $estate->verified ? : abort(404);
        
        $estate->load(['gallery', 'features']);
//        return $estate;
        
        $comments = $estate->comments()
                        ->where('active', 1)
                        ->with('user')
                        ->paginate(10);
        
        $other_estates = Estate::with(['cityData', 'features'])
                            ->where([
                                'active' => 1,
                                'verified' => 1,
                                'city' => $estate->city
                            ])
                            ->where('id', '!=', $estate->id)
                            ->orderBy('updated_at', 'desc')
                            ->limit(10)
                            ->get();
        $estate->rating = 0;
        $estate->rating_count = ProductRating::where('product_id', $estate->id)->count();
    
        if($estate->rating_count)
            $estate->rating = round(ProductRating::where('product_id', $estate->id)->avg('rate'), 1);
        
    
        $compare_list = collect([]);
        if(!is_null(Cookie::get('compare_list')))
            $compare_list = collect(Cookie::get('compare_list'));
    
        $calendar = Calendar::where('product_id' , $estate->id)->first();
        $events = [];
        $now = Carbon::now()->format('Y-m-d');
        if($calendar) {
            $events = $calendar->calendar;
            if(isset(json_decode($events)[0]))
                $now = json_decode($events)[0]->start;
//            return [$calendar];
            $events = json_decode($events);
        }
        
        foreach ($events as $event) {
//            $event->color = 'yellow';
            if($event->reserve) {
                $event->title = 'رزرو شده';
                $event->backgroundColor = '#b3b3b3';
                $event->color = '#b3b3b3';
            } else {
                $event->title =  number_format((int)$event->price) . 'ت';
                $event->backgroundColor = '#9a62e4';
//                $event->backgroundColor = '#fff';
                $event->color =  '#9a62e4';
            }
        }
    
        $estate->favorited = 0;
        if(Auth::check() && !is_null(Wishlist::where(['user_id' => Auth::id(), 'product_id' => $estate->id])->first()))
            $estate->favorited = 1;
        
        return view('front::estate.show', compact(
            'estate',  'compare_list', 'other_estates', 'comments',
                    'events', 'now'));
    }
    
    public function sendComment(Request $request) {
        $this->validate($request, [
            'estate_id' => 'exists:products,id',
            'description' => 'required'
        ]);
        
        $estate = Estate::find($request->estate_id);
    
        $estate->comments()->create([
            'user_id' => Auth::id(),
            'description' => $request->description
        ]);
        
        return [
            'status' => 'success'
        ];
    }
    
    public function addCompare(Request $request) {
    
        $this->validate($request, [
            'id' => 'exists:products,id',
        ]);
        
        $id = $request->id;
        $current_action = 'added';
        
        $compare_list = collect([]);
        if(!is_null(Cookie::get('compare_list')))
            $compare_list = collect(Cookie::get('compare_list'));
        
        
        // if compare list is full
        if($compare_list->count() == 4)
            $current_action = 'full';
        
        // if this id not exists => add
        elseif($compare_list->search($id) === false) {
            $compare_list->push($id);
            Cookie::queue('compare_list', $compare_list, 60*60*12);
        } else {
            $current_action = 'already_exists';
        }
    
        return [
            'status' => $current_action,
            'count' => $compare_list->count(),
            'list'  => $compare_list
        ];
    }
    
    public function removeCompare(Request $request) {
        $this->validate($request, [
            'id' => 'exists:products,id',
        ]);
    
        $id = $request->id;
    
        $compare_list = collect([]);
        if(!is_null(Cookie::get('compare_list')))
            $compare_list = collect(Cookie::get('compare_list'));
    
        $compare_list = $compare_list->reject(function ($value, $key) use ($id) {
            return $value == $id;
        });
    
        Cookie::queue('compare_list', $compare_list, 60*60*12);
    
//        return [
//            'status' => 'removed',
//            'count' => $compare_list->count(),
//            'list'  => $compare_list
//        ];
        return back();
    }
    
    
    public function compare() {
        $compare_list = collect([]);
        if(!is_null(Cookie::get('compare_list')))
            $compare_list = collect(Cookie::get('compare_list'));
        
        $estates = Estate::with('features')->whereIn('id', $compare_list)->where('active', 1)->get();
        
        return view('front::estate.compare', compact('estates'));
    }
    
    
    public function getCities(Request $request) {
        $this->validate($request, [
            'province_id' => 'exists:provinces,id'
        ]);
        
        return City::where('province_id', $request->province_id)->get();
    }
    
    public function rate(Request $request) {
        $this->validate($request, [
            'estate_id' => 'required|numeric|exists:products,id',
            'rate'  => 'required|min:1|max:5'
        ]);
    
        $user = Auth::user();
        $id = $request->estate_id;
        ProductRating::where(['user_id' => $user->id, 'product_id' => $id])->delete();
        ProductRating::create([
            'user_id' => $user->id,
            'product_id' => $id,
            'rate' => $request->rate
        ]);
        return [
            'error' => 0,
            'msg' => 'امتیاز با موفقیت ثبت شد',
            
        ];
    }
    
    public function favorite(Request $request) {
        $this->validate($request, [
            'estate_id' => 'required|exists:products,id'
        ]);
    
        $user = Auth::user();
        $id = $request->estate_id;
        $data = Wishlist::where(['user_id' => $user->id, 'product_id' => $id])->first();
        if(is_null($data)) {
            Wishlist::create([
                'product_id' => $id,
                'user_id' => $user->id
            ]);
            
            return [
                'error' => 0,
                'status' => 'favorited',
                'msg'   => 'به علاقه مندی های اضافه شد'
            ];
        }
        $data->delete();
        return [
            'error' => 0,
            'status' => 'unfavorited',
            'msg'   => 'از لیست علاقه مندی ها حذف شد'
        ];
    }
}
