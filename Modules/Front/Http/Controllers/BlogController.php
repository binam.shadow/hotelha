<?php

namespace Modules\Front\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Modules\Blog\Entities\Blog;

class BlogController extends Controller {
    
    public function index() {
    
        $blogs = Blog::where([
            'active' => 1,
            'type' => 'blog'
        ])->limit(6)->orderBy('updated_at', 'desc')->paginate(10);
        
        return view('front::blog.index', compact( 'blogs'));
    }
    
    public function show(Blog $blog) {
    
        $blogs = Blog::where('active', 1)->limit(6)->inRandomOrder()->get();
        
        return view('front::blog.show', compact('blog', 'blogs'));
    }
    
    public function like(Blog $blog) {
        if(!Auth::check()) abort(403);
        
        $is_liked = 0;
        
        if($blog->isLiked())
            $blog->unLike();
        else {
            $is_liked = 1;
            $blog->like();
        }
        
        return [
            'is_liked' => $is_liked,
            'count' => $blog->likes_count
        ];
    }
}
