<?php

namespace Modules\Front\Http\Controllers\Auth;

use App\Classes\Smsir;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\User;

class ForgetController extends Controller {
    
    
    public function index() {
        
        return view('front::auth.forget');
    }
    
    public function forget(Request $request) {
        
        $this->validate($request, [
            'phone' => 'required|digits:11|numeric|regex:/(^09)/|exists:users,phone'
        ]);
        
        $phone = $request->phone;
        $tmp_password = rand(1000, 9999);
        
        $user = User::where('phone', $phone)->first();
        $user->password = bcrypt($tmp_password);
        $user->save();
        
        Smsir::sendVerification($tmp_password, $request->phone);
        
        return redirect()
            ->route('front.login')
            ->with('msg', 'رمز عبور موقت به شماره شما ارسال شد');
    }
}
