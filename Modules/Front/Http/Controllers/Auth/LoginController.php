<?php

namespace Modules\Front\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Auth;

class LoginController extends Controller {
    
    public function index() {
        return view('front::auth.login');
    }
    
    public function login(Request $request) {
        $this->validate($request, [
            'phone' => 'required|digits:11|numeric|regex:/(^09)/|exists:users,phone',
            'password' => 'required|max:255'
        ]);
    
        $credentials = [
            'phone' => $request->phone,
            'password' => $request->password,
            'active'    => 1
        ];
        
        if(Auth::attempt($credentials)) {
            $user = Auth::user();
            $user->verified = 1;
            $user->save();
            return redirect()->route('dashboard.index');
        }
        
        return redirect()->back()->withErrors([
            'شماره تلفن یا رمز عبور وارد شده صحیح نیست'
        ]);
        
    }
    
    public function logout() {
        
        Auth::logout();
        
        return redirect()->route('front.index');
    }

}
