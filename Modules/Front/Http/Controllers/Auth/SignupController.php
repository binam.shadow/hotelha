<?php

namespace Modules\Front\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Classes\Smsir;

class SignupController extends Controller {
    
    public function index() {
        
        return view('front::auth.signup');
    }
    
    public function signup(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'phone' => 'required|digits:11|numeric|regex:/(^09)/|unique:users,phone',
            'recommend_code' => 'nullable|max:255|exists:users,phone',
            'national_code' => [
                'required',
                'numeric',
                'digits:10',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[0-9]{10}$/', $value))
                        return $fail('کد ملی وارد شده معتبر نیست');
                    for($i = 0; $i < 10; $i++)
                        if(preg_match('/^'.$i.'{10}$/',$value))
                            return $fail('کد ملی وارد شده معتبر نیست');
                    for($i = 0, $sum = 0; $i < 9; $i++)
                        $sum += (( 10 - $i) * intval(substr($value, $i,1)));
                    $ret = $sum % 11;
                    $parity = intval(substr($value, 9,1));
                    if(($ret < 2 && $ret == $parity) || ($ret >= 2 && $ret == 11 - $parity))
                        return true;
                    return $fail('کد ملی وارد شده معتبر نیست');
                }
            ],
        ], [
            'phone.digits' => 'شماره تلفن باید 11 رقم باشد',
            'recommend_code.exists' => 'کد معرفی وارد شده معتبر نیست'
        ]);
    
        $code = str_random(10);
        while(count(User::where('code', $code)->first()))
            $code = str_random(10);
    
        $recommend_code = null;
        if($request->recommend_code)
            $recommend_code = User::where('phone', $request->recommend_code)->first()->code;
        
        $activation_code = rand(1000,9999);
        
        $user = User::create([
            'name'     => $request->name,
            'phone'     => $request->phone,
            'national_code' => $request->national_code,
            'code'      => $code,
            'recommend_code' => $recommend_code,
            'password'  => bcrypt($activation_code),
            'active'    => 1,
            'verified'  => 0
        ]);
        $role = Role::where('name', 'user')->first();
        $user->roles()->save($role);
    
        Smsir::sendVerification($activation_code, $request->phone);
        
        return redirect()
                ->route('front.login')
                ->with('msg', 'ثبت نام شما با موفقیت انجام شد، برای ورود رمز عبوری که برای شما ارسال شده است را وارد کنید')
                ->with('success_register', 1)
                ->withInput($request->all());
    }
}
