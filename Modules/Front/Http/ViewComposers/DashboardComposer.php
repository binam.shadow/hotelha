<?php


namespace Modules\Front\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;

use Auth;

class DashboardComposer {
   
    public function compose(View $view)
    {
        $view->with('user', Auth::user());
    }
}
