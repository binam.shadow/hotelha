<?php

namespace App\Http\Controllers;

use App\Classes\Encryption;
use Illuminate\Http\Request;
use App\Classes\DynamicQuery;

class ApiController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function getData(Request $request) {
        $user = \Auth::user();
        if(!$user->active){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }

        $table = $request->table;

        if(is_array($request->queryParts))
            $parts = json_encode($request->queryParts);
        else
            $parts = $request->queryParts;
        $parts = json_decode($parts);
        $data = DynamicQuery::buildGet($table, $parts);
        //get($table, $select, $join, $where, $whereIn, $groupBy, $distinct);

        return Encryption::encryptor($data, $request->key);
    }
}
