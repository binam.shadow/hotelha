<?php
namespace App\Http\ViewComposers;
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 7/23/2017
 * Time: 9:04 PM
 */

use Illuminate\View\View;
use App\Setting;
// citiesinfo

class LoginComposer
{
    public function __construct()
    {
    }

    /**
     * @param View $view
     */
    public function compose(View $view) {
        $title = Setting::where('key', 'panel_title')->first();

        $view->with(['title' => $title]);
    }

}
