<?php

namespace App\Providers;

use App\Classes\jDateTime;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Validator;

class AppServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        
        Schema::defaultStringLength(191);
		View::composer(
            'adminbsb.login',
            '\App\Http\ViewComposers\LoginComposer'
        );
        View::composer(
            'partials._xcloner',
            '\App\Http\ViewComposers\XClonerComposer'
        );
        Validator::extend('jalali', function($attribute, $value, $parameters, $validator){
            return jDateTime::isValidJalaliDate($value,'-');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
