<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class City
 * @package App
 * @version September 6, 2017, 8:28 am UTC
 *
 * @property \App\Province province
 * @property \Illuminate\Database\Eloquent\Collection carts
 * @property \Illuminate\Database\Eloquent\Collection orderItemsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsProductCategories
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property string name
 * @property integer province_id
 */
class City extends Model {
	use LogsActivity;

    public $table = 'cities';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'name',
        'province_id'
    ];

	protected static $logFillable = true;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'province_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function province()
    {
        return $this->belongsTo(\App\Province::class);
    }

	public function getDescriptionForEvent(string $eventName): string {
		switch ($eventName) {
			case 'created':
				return "ثبت شهر جدید";
				break;

			case 'updated':
				return "ویرایش شهر";
				break;

			case 'deleted':
				return "حذف شهر";
				break;

			default:
				return '';
				break;
		}
    }
}
