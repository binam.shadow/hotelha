<?php
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 9/27/2017
 * Time: 3:49 PM
 */

namespace App\Classes;


class PersianTools {
    public static function numberConvertor($data){
        $persian = array("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹");
        $latin = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $text = str_replace($persian, $latin, $data);
        return $text;
    }

    public static function arabicToPersian($data){
        $arabic = array("ي", "ك", "٤", "٥", "٦");
        $persian = array("ی", "ک", "۴", "۵", "۶");
        $text = str_replace($arabic, $persian, $data);
        return $text;
    }
}