<?php
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 10/14/2017
 * Time: 10:06 AM
 */

namespace App\Classes;

use GuzzleHttp\Client;

class Smsir
{
    public static function sendVerification($code, $number) {
        $body = ['Code' => $code, 'MobileNumber' => $number];
        $url = 'http://restfulsms.com/api/VerificationCode';
        $token = self::getToken();
        if ($token != false) {
            $smsResult = self::execute($url, $body, $token);
            $object = json_decode($smsResult);
            if (is_object($object)) {
                $array = get_object_vars($object);
                if (is_array($array)) {
                    $result = $array['Message'];
                } else {
                    $result = false;
                }
            } else {
                $result = false;
            }

        } else {
            $result = false;
        }
        return $result;
    }

    public static function sendMessage($Messages, $MobileNumbers) {
        $LineNumber = env('SMSIR-LINE-NUMBER');
        date_default_timezone_set("Asia/Tehran");
        $SendDateTime = date("Y-m-d") . "T" . date("H:i:s");

        $token = self::getToken();
        if ($token != false) {
            $postData = array(
                'Messages' => $Messages,
                'MobileNumbers' => $MobileNumbers,
                'LineNumber' => $LineNumber,
                'SendDateTime' => $SendDateTime,
                'CanContinueInCaseOfError' => 'false'
            );

            $url = "http://RestfulSms.com/api/MessageSend";
            $SendMessage = self::execute($url, $postData, $token);
            $object = json_decode($SendMessage);
            if (is_object($object)) {
                $array = get_object_vars($object);
                if (is_array($array)) {
                    $result = $array['Message'];
                } else {
                    $result = false;
                }
            } else {
                $result = false;
            }

        } else {
            $result = false;
        }
        return $result;
    }

    public static function getToken() {
        $APIKey = env('SMSIR-API-KEY');
        $SecretKey = env('SMSIR-SECRET-KEY');

        $postData = array(
            'UserApiKey' => $APIKey,
            'SecretKey' => $SecretKey,
            'System' => 'php_rest_v_1_1'
        );
        $postString = json_encode($postData);

        $tokenUrl = "http://RestfulSms.com/api/Token";
        $ch = curl_init($tokenUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, count($postString));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);

        $result = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($result);

        if (is_object($response)) {
            $resultVars = get_object_vars($response);
            if (is_array($resultVars)) {
                @$IsSuccessful = $resultVars['IsSuccessful'];
                if ($IsSuccessful == true) {
                    @$TokenKey = $resultVars['TokenKey'];
                    $resp = $TokenKey;
                } else {
                    $resp = false;
                }
            }
        }

        return $resp;
    }


    public static function execute($url, $postData, $token) {

        $postString = json_encode($postData);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-sms-ir-secure-token: ' . $token
        ));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, count($postString));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public static function credit()
    {
        $client     = new Client();
        $result     = $client->get('http://restfulsms.com/api/credit',['headers'=>['x-sms-ir-secure-token'=>self::getToken()],'connect_timeout'=>30]);
        return json_decode($result->getBody(),true)['Credit'];
    }
}
