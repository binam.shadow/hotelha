<?php
namespace App\Classes;

class Travel {
    

    public static function requester($method) {
        $base_url = "http://api.alaedin.travel/";
        $guid = "E7B71401-AEFE-4E05-8738-8F3328E4A71C";

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Accept'    => 'application/json, text/json',
            ]
        ]);
        
        return json_decode($client->request('GET', $base_url . "$method/" . $guid)->getBody()->getContents());
    }

    public static function getProvinces()  {
        return self::requester('Province');
    }
}


