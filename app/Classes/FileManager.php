<?php
namespace App\Classes;
use Illuminate\Support\Facades\File;

/**
 * A Helper Class
 */
class FileManager
{

    function __construct() {

    }

    public static function uploader($path, $file) {
        // Upload file to path
        $name = str_random(10) . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('uploads/' . $path), $name);

        // Return image with path like "images/categories/1234567.jpg"
        return $path . '/' . $name;
    }

    public static function delete($path) {

        $original_path = public_path('uploads/' . $path);
        if (file_exists($original_path)) {
            if (File::delete($original_path))
                return true;
            else
                return false;
        }
    }

}
