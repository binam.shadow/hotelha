<?php
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 9/27/2017
 * Time: 3:49 PM
 */

namespace App\Classes;


class Encryption {

    /**
     * @param $data
     * @param $key
     * @return string
     */
    public static function encryptor($data, $key){
        $config = config('app');
        $data = json_encode($data);
        $encryptedData = (new \Illuminate\Encryption\Encrypter($key, $config['cipher']))->encrypt($data, false);
        return $encryptedData;
    }
}