<?php

namespace App\Classes;
use DB;

class DynamicQuery {

	public static function buildGet($table, $data) {
		$query = [];
		$query['table']  = $table;
		$query['select'] = $query['limit'] = $query['offset'] = $query['orderBy'] = $query['join'] = $query['where'] = $query['whereIn'] = $query['groupBy'] = $query['distinct'] = null;

		if(isset($data->select))
			$query['select'] = $data->select;

		if(isset($data->limit))
			$query['limit'] = $data->limit;

		if(isset($data->offset))
			$query['offset'] = $data->offset;

		if(isset($data->orderBy))
			$query['orderBy'] = $data->orderBy;

		if(isset($data->join))
			$query['join'] = $data->join;

		if(isset($data->where))
			$query['where'] = $data->where;

		if(isset($data->whereIn))
			$query['whereIn'] = $data->whereIn;

		if(isset($data->groupBy))
			$query['groupBy'] = $data->groupBy;

		if(isset($data->distinct))
			$query['distinct'] = $data->distinct;

		return self::get(
			$query['table'], $query['select'], $query['limit'],
			$query['offset'], $query['orderBy'], $query['join'],
			$query['where'], $query['whereIn'], $query['groupBy'],
			$query['distinct']
		)->get();
	}

	/***
	 *
	 * Insert Data
	 * @param (strint) table, (array) data
	 * @return 0 or 1
	 *
	 **/
	public function insert($table, $data){
		if (!count($data)) {
			return 1;
		}

		$saved = DB::table($table)->insert((array)$data);

		if(!$saved)
			return 0;

		return DB::table($table)->orderBy('id', 'desc')->first()->id;
	}

	/***
	 *
	 * Update Data
	 * @param (string) table, (string) id, (array) data
	 * @return 0 or 1
	 *
	 **/
	public function update($table, $id, $data){
		if (!count($data)) {
			return 0;
		}

		$updated = DB::table($table)->where('id', $id)->update($data);

		if(!$updated)
			return 0;

		return 1;
	}

	/***
	 *
	 * Delete Data
	 * @param (string) table, (array) where
	 * @return 0 or 1
	 *
	 */
	public function delete($table, $where){
		$deleted = DB::table($table)->where($where)->delete();

		if(!$deleted)
			return 0;

		return 1;
	}

	/***
	 *
	 * Get Data
	 * @param (string) table, (array) select, (Array) orderBy, (array) join, (array) where, (array) whereIn,
	 * (string) groupBy, (any) distinct
	 * @return $query
	 *
	 */
	public static function get(
		$table, $select = null, $limit = null, $offset = null, $orderBy = null, $join = null, $where = null,
		$whereIn = null, $groupBy = null, $distinct = null){

		$query = DB::table($table);

		if($limit != null)
			$query->limit($limit);

		if($offset != null)
			$query->offset($offset);

		if($where != null)
			$query->where((array)$where);

		if($whereIn != null)
			$query->whereIn($whereIn[0], $whereIn[1]);

		if($groupBy != null)
			$query->groupBy($groupBy);

		if($orderBy != null)
			$query->orderBy($orderBy[0], $orderBy[1]);

		if($distinct != null)
			$query->distinct();

		if($select != null)
			$query->select($select);

		if($join != null){
			foreach ((array)$join as $row) {
				$query->join($row->table , $row->rule[0], $row->rule[1], $row->rule[2]);
			}
		}

		return $query;
	}
}
