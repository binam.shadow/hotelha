<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'name', 'display_name', 'description', 'parent_id', 'parent_parent_id', 'url', 'icon'
    ];
    
    public function sub_menus() {
        return $this->hasMany(self::class, 'parent_id');
    }
}
