<?php

namespace App;

//use App\Library\Favorite;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Image;
use Illuminate\Http\Request;

class Medium extends Model
{
    //use Favorite;
    protected $guarded = ['id'];

    public $upload_lg_folder = 'image/lg';
    public $upload_md_folder = 'image/md';
    public $upload_sm_folder = 'image/sm';
    public $upload_xs_folder = 'image/xs';
    private $file_object;

    public function getRouteKeyName() {
        return 'file_name';
    }

    public function getFullNameAttribute() {
        return $this->file_name. '.' .$this->extension;
    }

    public function getFullPathAttribute() {
        if($this->is_image()){
            return $this->full_path_lg;
        }
        return storage_path()."/app/public/".explode("/",$this->mime)[0]."/".$this->full_name;
    }

    public function getFullPathLgAttribute(){
        return storage_path()."/app/public/".$this->upload_lg_folder."/".$this->full_name;
    }

    public function getFullPathMdAttribute(){
        return storage_path()."/app/public/".$this->upload_md_folder."/".$this->full_name;
    }

    public function getFullPathSmAttribute(){
        return storage_path()."/app/public/".$this->upload_sm_folder."/".$this->full_name;
    }

    public function getFullPathXsAttribute(){
        return storage_path()."/app/public/".$this->upload_xs_folder."/".$this->full_name;
    }

    public function mediumable(){
        return $this->morphTo();
    }

    public function file_type(){
        return explode('/',$this->mime)[0];
    }

    public function is_image(){
        return preg_match('/image\/*/',$this->mime);
    }

    public function is_video() {
        return preg_match('/video\/*/',$this->mime);
    }

    public function is_application() {
        return preg_match('/application\/*/',$this->mime);
    }

    public function is_audio() {
        return preg_match('/audio\/*/',$this->mime);
    }

    public function is_font() {
        return preg_match('/font\/*/',$this->mime);
    }

    public function is_text() {
        return preg_match('/text\/*/',$this->mime);
    }

    public function attach_file($file){
        if($this->id){ // if the file already stored
            if($this->is_image()){
                unlink($this->full_path_lg);
                unlink($this->full_path_md);
                unlink($this->full_path_sm);
                unlink($this->full_path_xs);
            }else{
                unlink($this->full_path);
            }
        }
        $this->file_object = $file;
        $this->file_name = str_random(16);
        $this->extension = $file->getClientOriginalExtension();
        $this->size      = $file->getSize();
        $this->mime 	 = $file->getMimeType();
        $this->created_at= strftime("%Y-%m-%d %H:%M:%S",time());
        $this->updated_at= strftime("%Y-%m-%d %H:%M:%S",time());
    }

    public static function make($file, $caption, array $morph_attributes=[], $manner = null)
    {
        $medium = new Medium();
        $medium->attach_file($file);
        $medium->caption = $caption;
        $medium->mediumable_position = isset($morph_attributes['position']) ? $morph_attributes['position']:0;
        $medium->mediumable_type = isset($morph_attributes['model']) ? $morph_attributes['model'] : null;
        $medium->mediumable_id = isset($morph_attributes['model_id']) ? $morph_attributes['model_id'] : null;
        $medium->description = isset($morph_attributes['description']) ? $morph_attributes['description']:null;
        $medium->manner = $manner;
        return $medium;
    }

    public function store() {
        if($this->is_image()){\
            Storage::disk($this->upload_lg_folder)->put($this->full_name, File::get($this->file_object));

            Image::make($this->file_object)
                ->widen(540)
                ->save($this->full_path_md);

            Image::make($this->file_object)
                ->fit(270)
                ->save($this->full_path_sm);

            Image::make($this->file_object)
                ->fit(135)
                ->save($this->full_path_xs);
        }elseif($this->is_video()){
            Storage::disk('video')->put($this->full_name, File::get($this->file_object));
        }
        return $this->save();
    }

    public function show($size="lg"){

        if($this->is_image()){
            $path = storage_path("app/public/image/$size/".$this->full_name);
        }else{
            $path = $this->full_path;
        }

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    public function url($size='lg'){
        if($this->is_image()){
            return route('show_img', [
                'filename' => $this->file_name,
                'size'     => $size
            ]);
        } else {
            return route('show_img', [
                'filename' => $this->file_name
            ]);
        }
    }

    public function get_image_info(array $sizes = ['lg','md','sm','xs'])
    {
        if($this->is_image()){
            $info = [];
            foreach ($sizes as $size){
                $path = 'full_path_'.$size;
                list($width, $height) = @getimagesize($this->$path);
                $info[$size] = ['src' => $this->url($size),
                           'width' => $width,
                           'height' => $height
                ];
            }
            return $info;
        }
        return null;
    }

    public function remove(){
        if($this->delete()){
            if($this->is_image()){
                unlink($this->full_path_lg);
                unlink($this->full_path_md);
                unlink($this->full_path_sm);
                unlink($this->full_path_xs);
            }else{
                unlink($this->full_path);
            }
            return true;
        }else{
            return false;
        }
    }

    public function fileInputInit()
    {
        $initialPreveiew[] = "<img src='".$this->full_path_sm."'>";
        $initialPreveiewConfig[] = ['caption' => $this->caption,
                                    'width' => '160px',
                                    'url' => 'delete/image/'.$this->id,
                                    'key' => $this->id];
        return ['initialPreview' => $initialPreveiew, 'initialPreviewConfig' => $initialPreveiewConfig];
    }

    public function scopeByType($query, $type)
    {
        return $query->where('mime', 'LIKE', $type.'/%' );
    }

    public function scopeImages($query)
    {
        return $query->where('mime', 'LIKE', 'image/%');
    }

    public function scopeVideos($query)
    {
        return $query->where('mime', 'LIKE', 'video/%');
    }

    public function scopeOf($query, $value){
        return $query->where('mediumable_type', $value);
    }

}
