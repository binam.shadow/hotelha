<!DOCTYPE html>
<html>

@include('adminbsb.layouts.head')

<body class="theme-{{$theme_color}}" data-theme="{{$theme_color}}">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red" style="direction: ltr;">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>لطفا صبر کنید...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    @include('adminbsb.layouts.navbar')
    <section>
        @include('adminbsb.layouts.left_side')

        @include('adminbsb.layouts.right_side')
    </section>

    <section class="content">
        <div class="container-fluid">
            @yield('content')
        </div>
    </section>

	@include('adminbsb.layouts.js_files')
    <script type="text/javascript">
        @if(session('msg'))
            showNotification('{{ session('msg_color') }}', '{{ session('msg') }}', 'bottom', 'left');
        @endif
    </script>
</body>

</html>
