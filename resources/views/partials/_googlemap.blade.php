@php
if(!isset($latitude)) $latitude = "";
if(!isset($longitude)) $longitude = "";
@endphp
<div class="row">
    <div class="col-md-6">
        <div class="form-group form-float">                
            <label class="form-label">@lang('setting::default.lat')</label>
            <div class="form-line">
                <input class="form-control" name="latitude" id="lat" value="{{ $latitude }}" required="" aria-required="true" type="text">
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="form-group form-float">                
            <label class="form-label">@lang('setting::default.lng')</label>
            <div class="form-line">
                <input class="form-control" name="longitude" id="lng" value="{{ $longitude }}" required="" aria-required="true" type="text">
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div id="mp" class="col-md-12 form-group form-float">
        <div class="form-group">
            <div class="col-md-12">
                <section id="place-on-map">
                    <label for="address-map" style="padding-top:10px"> پس از جستجوی محل
                        پوینتر را کشیده و بر روی مکان مورد نظر رها کنید.</label>
                    <div class="form-line input-group">
                        <input class="form-control" style="width:88%" id="geocomplete" type="text"
                               placeholder="آدرس را وارد کنید" value="تهران"/>
                        <span class="input-group-btn add-field">
                            <button id="find" type="button" class="btn btn-info btn-md waves-effect waves-circle waves-float btn-map-search"><i class="material-icons">search</i></button>
                        </span>
                    </div>
                    <div class="map_canvas" style=" height: 350px; width: 100%;"></div>
                    <br/>
                    <div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

@pushonce('stack_style')
<style>
    .btn-map-search{
        padding: 2px 5px;
        width: 65px;
        border-radius: 3px;
    }
    .btn-map-search i{
        font-size: 23px !important;
    }
</style>
@endpushonce

@push('stack_scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-IgCJVNvcbIOEaC7oQduowDaG3aJzlnM&libraries=places&v=3.exp"></script>
<script src="{{url('backend/plugins/jquery-geocomplete/jquery.geocomplete.js')}}"></script>

<script type="text/javascript">
$(function () {
    
    
    $("#geocomplete").geocomplete({
        map: ".map_canvas",
        mapOptions: {
            zoom: 17
        },
        markerOptions: {
            draggable: true
        }
    });

    $("#geocomplete").bind("geocode:dragged", function (event, latLng) {

        $("input#lat").val(latLng.lat());
        $("input#lng").val(latLng.lng());

    });
    $("#find").click(function () {
        $("#geocomplete").trigger("geocode");

    }).click();
    
    @if($latitude != '' && $longitude != '')
    var lat_and_long = "{{$latitude}}, {{$longitude}}";
    $("#geocomplete").geocomplete("find", lat_and_long);
    @endif
});
</script>
@endpush