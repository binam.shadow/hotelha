@php $field_name = isset($field_name) ? $field_name : 'gallery' @endphp
@php $field_type = isset($field_type) ? $field_type : 'image' @endphp

@pushonce('stack_style')
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput447/css/fileinput.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput447/css/fileinput-rtl.min.css') }}">
@endpushonce

@pushonce('stack_scripts')
    <script src="{{ asset('backend/plugins/bootstrap-fileinput447/js/fileinput.js') }}"></script>
    <script src="{{ asset('backend/plugins/bootstrap-fileinput447/js/locales/fa.js') }}"></script>
@endpushonce


@push('stack_scripts')
<script type="text/javascript">
$("#{{$field_name}}").fileinput({
    language: 'fa',
    rtl: true,
    uploadAsync: false,
    uploadUrl: '/',
    allowedFileTypes: ['{{$field_type}}'],
    overwriteInitial: false,
    maxFileSize: 10240,
    maxFileCount: 5,
    showPreview: true,
    showCaption: false,
    showRemove: true,
    showUpload: false,
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    layoutTemplates: {actions: '<div class="file-actions">\n' +
        '    <div class="file-footer-buttons">\n' +
        '        {delete} {zoom}' +
        '    </div>\n' +
        '    <div class="clearfix"></div>\n' +
        '</div>'},
    @if(isset($gallery))
    initialPreview: [
        @foreach($gallery as $media)
            "{{$media->url('sm')}}",
        @endforeach
    ],
    initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
    initialPreviewFileType: '{{$field_type}}',
    initialPreviewConfig: [
        @foreach($gallery as $media)
        {
            filetype: "{{$field_type}}/{{$media->extension}}",
            url: '{{  route('remove_media') }}',
            key: '{{ $media->file_name }}'
        },
        @endforeach
    ],
    @endif
});
</script>
@endpush