<div class="col-md-6 text-center">
	<div class="kv-avatar">
		<!-- <div class="file-loading"> -->
			<input id="avatar" name="{{$field_name}}" type="file">
		<!-- </div> -->
	</div>
	<div class="kv-avatar-hint"><small>Select file < 1500 KB</small></div>
</div>

@push('stack_style')
<link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
<style>
	.file-drop-zone, .file-drop-zone.clickable:hover{
		border: none;
	}
	.file-drag-handle{
		display: none;
	}
</style>
@endpush


@push('stack_scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script type="text/javascript">
$("#avatar").fileinput({
	overwriteInitial: true,
	maxFileSize: 1500,
	showClose: false,
	showCaption: false,
	browseOnZoneClick: true,
	browseLabel: '',
	removeLabel: '',
	browseIcon: '<i class="material-icons">create_new_folder</i>',
	removeIcon: '<i class="material-icons">delete</i>',
	removeTitle: 'Cancel or reset changes',
	elErrorContainer: '#kv-avatar-errors-1',
	msgErrorClass: 'alert alert-block alert-danger',
	defaultPreviewContent: '<img class="img-responsive" src="{{ asset('backend/plugins/bootstrap-fileinput/img/default.png') }}">',
	layoutTemplates: {main2: '{preview} {remove}'},
	allowedFileExtensions: ["jpg", "png", "gif"]
});
</script>
@endpush
