@php $id = isset($id) ? $id : 'contacts' @endphp
@php $mode = isset($mode) ? $mode : 'create' @endphp

@pushonce('stack_style')
<style>
    .xcloner {
        -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, .16), 0 2px 10px rgba(0, 0, 0, .12);
        -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, .16), 0 2px 10px rgba(0, 0, 0, .12);
        box-shadow: 0 2px 5px rgba(0, 0, 0, .16), 0 2px 10px rgba(0, 0, 0, .12);
    }

    .xcloner .btn:not(.btn-link):not(.btn-circle) {
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
    }
    
    .xcloner .dropdown-menu{
        height: 180px;
        overflow-y: scroll;
    }
</style>
@endpushonce
<div class="input-group xcloner" id="{{$id}}">
    <div class="input-group-btn">
        <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown"
                class="btn btn-default dropdown-toggle" type="button">
            <span class="selected-choice"></span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            @foreach($contact_types as $type)
            <li><a data-value="{{$type->name}}" href="#">{{$type->label}}</a></li>
            @endforeach
        </ul>
    </div>
    <input type="text" class="form-control text-center main-input" value=""
           name="field_"/>
    <span class="input-group-btn remove-field hidden">
                    <button type="button" class="btn btn-danger">-</button>
                </span>
    <span class="input-group-btn add-field">
                    <button type="button" class="btn btn-success">+</button>
                </span>
</div>
@pushonce('stack_scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/xcloner/js/xcloner.js') }}"></script>
<script>
    @if(isset($fields) && !is_null($fields))
        @foreach($fields as $field)
            $('#{{$id}}').xcloner({chooseText: 'انتخاب کنید'}).add({"{{$field->type}}":"{{$field->value}}"});
        @endforeach
        $('#{{$id}}').xcloner({
            chooseText: 'انتخاب کنید'
        });
    @else
        $('#{{$id}}').xcloner({
            chooseText: 'انتخاب کنید'
        });
    @endif

</script>
@endpushonce